#ifndef QHTML5INPUT_H
#define QHTML5INPUT_H

#include <QScopedPointer>

class QWindow;
class QWebGLScreen;

class QHTML5Input
{
public:
    QHTML5Input();

    void addWindow(QWebGLScreen * s, QWindow *win);

    static QHTML5Input &get()
    {
        static QHTML5Input instance;

        return instance;
    }

    QWindow * _qwindow; //fixme
   QWebGLScreen *screen;
protected:
   // virtual void run() Q_DECL_OVERRIDE;

private:
    //void handleEvents();

    bool m_shouldStop;

};

#endif // QHTML5INPUT_H
