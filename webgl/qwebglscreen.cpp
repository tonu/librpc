/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the plugins of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl-3.0.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or (at your option) the GNU General
** Public license version 3 or any later version approved by the KDE Free
** Qt Foundation. The licenses are as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL2 and LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-2.0.html and
** https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include <webglstreampp/client.hpp>
#include <webglstreampp/server.hpp>
#include <webglstreampp/wsserver.hpp>
#include <unistd.h>

#include "qwebglscreen.h"
#include "qwebglwindow.h"
#include <dlfcn.h>
#include <iostream>
#include <QDebug>

//#include <QtPlatformSupport/private/qeglconvenience_p.h>
//#include <QtPlatformSupport/private/qeglplatformcontext_p.h>

#ifdef Q_OPENKODE
#include <KD/kd.h>
#include <KD/NV_initialize.h>
#endif //Q_OPENKODE


#include <qpa/qwindowsysteminterface.h>
#include "qhtml5input.h"

//to debug:export QSG_RENDERER_DEBUG=render, dump,build, roots
QT_BEGIN_NAMESPACE
void output_gl_diagnostics()
{
    std::cout << "GL version:" << glGetString(GL_VERSION) << std::endl;
    std::cout << "GL renderer:" << glGetString(GL_RENDERER) << std::endl;
    std::cout << "GL vendor:" << glGetString(GL_VENDOR) << std::endl;
    std::cout << "GLSL version:" << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
    std::cout << "GL extensions:" << glGetString(GL_EXTENSIONS) << std::endl;
}

class MyPlatformContext : public QPlatformOpenGLContext
{


public:
    virtual QSurfaceFormat format() const
    {
        QSurfaceFormat fmt;

        fmt.setMajorVersion(2);
        fmt.setMinorVersion(0);
        fmt.setProfile(QSurfaceFormat::NoProfile); // this will write correct shaders for WebGL

        fmt.setRedBufferSize(8);
        fmt.setGreenBufferSize(8);
        fmt.setBlueBufferSize(8);
        fmt.setAlphaBufferSize(8);
        fmt.setStencilBufferSize(8);
        fmt.setDepthBufferSize(16);

        fmt.setRenderableType(QSurfaceFormat::OpenGLES);

        return fmt;
    }

    virtual void swapBuffers(QPlatformSurface *surface)
    {
        glFlush();
        usleep(30000);// fixme.. kuidagi muudmoodi teha fps madalamaks
                glGetError();
    }


    virtual bool makeCurrent(QPlatformSurface *surface)
    {
        return true;
    }

    virtual void doneCurrent()
    {

    }
    virtual void initialize()
    {
        qDebug() <<"init\n";
    }

    virtual QFunctionPointer getProcAddress(const char *procName)
    {
qDebug() << "Proc:" << procName << "=" << (QFunctionPointer)Client::get()->getProcAddress(procName);
        return (QFunctionPointer)Client::get()->getProcAddress(procName);
    }

};

//class QMinimalEglContext : public QEGLPlatformContext
//{
//public:
//    QMinimalEglContext(const QSurfaceFormat &format, QPlatformOpenGLContext *share, EGLDisplay display)
//        : QEGLPlatformContext(format, share, display)
//    {
//    }

//    EGLSurface eglSurfaceForPlatformSurface(QPlatformSurface *surface) Q_DECL_OVERRIDE
//    {
//        QMinimalEglWindow *window = static_cast<QMinimalEglWindow *>(surface);
//        QMinimalEglScreen *screen = static_cast<QMinimalEglScreen *>(window->screen());
//        return screen->surface();
//    }
//};

 void setMouse(int16_t x, int16_t y, uint8_t buttons)
 {
     std::cerr << "setMouse" << x << ";" << y << " " << buttons << "\n";

    struct timeval tv;  // removed comma

    gettimeofday (&tv, NULL);
    ulong ts = (tv.tv_sec*1000) + (tv.tv_usec/1000);

    
    QWindowSystemInterface::handleMouseEvent(QHTML5Input::get()._qwindow, ts, QPointF(x,y), QPointF(x,y), (Qt::MouseButtons)buttons);
 }
void setNewSize(int16_t width, int16_t height)
{
    std::cerr << "setNewSize" << width << "x" << height << "\n";
  QRect rect(0, 0, width, height);


QScreen *screen = QHTML5Input::get()._qwindow->screen();

auto s = QHTML5Input::get().screen;
    if (s)
        s->setGeometry(rect);
    QHTML5Input::get()._qwindow->setGeometry(rect);

    // these commands seem useless :D
    QWindowSystemInterface::handleScreenGeometryChange(screen, rect, rect);

}


#include <QCoreApplication>
QWebGLScreen::QWebGLScreen()
    : m_depth(24)
    , m_format(QImage::Format_Invalid)
    , m_platformContext(0)
    , fifo_cli_to_srv(new FIFO)
    , fifo_srv_to_cli(new FIFO)
    , server(new Server(fifo_srv_to_cli, fifo_cli_to_srv))
    , ws(new WSServer(12345, fifo_cli_to_srv, fifo_srv_to_cli))
{
    Client::get()->init(fifo_cli_to_srv, fifo_srv_to_cli);

    server->setCallback("setNewSize", (void *) setNewSize);
    server->setCallback("setMouse", (void *) setMouse);

    m_platformContext = new MyPlatformContext;
    qWarning("Loading WebGL plugin...\n");


    run = true;
    serverThread = std::thread([&]
    {
        while(run)
        {
            server->step();
            usleep(100);
        }
        qDebug() << "serverThread quit";
    });//fixme

    ws->onClose = [&](){

        // creating thread for this as we cant stop/join WS from itself
        closer = std::thread([this]{

            //stop client accepting commands otherwise qt gets stuck waiting for returnval for glGetString or similar
            Client::get()->stop();

            //stop server
            run = false;
            serverThread.join();

            //stop websockets
            ws->stop();

            // tell app to quit
            qApp->quit();
        });
        //t.join();

        qDebug() << "all quitting..";
    };
    qWarning("QMinimalEglIntegration::createPlatformWindow");


    //    EGLint major, minor;

    //    if (Q_UNLIKELY(!eglBindAPI(EGL_OPENGL_ES_API))) {
    //        qWarning("Could not bind GL_ES API\n");
    //        qFatal("EGL error");
    //    }

    //    m_dpy = eglGetDisplay(display);
    //    if (Q_UNLIKELY(m_dpy == EGL_NO_DISPLAY)) {
    //        qWarning("Could not open egl display\n");
    //        qFatal("EGL error");
    //    }
    //    qWarning("Opened display %p\n", m_dpy);

    //    if (Q_UNLIKELY(!eglInitialize(m_dpy, &major, &minor))) {
    //        qWarning("Could not initialize egl display\n");
    //        qFatal("EGL error");
    //    }

    //    qWarning("Initialized display %d %d\n", major, minor);
}

QWebGLScreen::~QWebGLScreen()
{
    //    if (m_surface)
    //        eglDestroySurface(m_dpy, m_surface);

    closer.join();
    //    eglTerminate(m_dpy);
}

void QWebGLScreen::createAndSetPlatformContext() const {
    const_cast<QWebGLScreen *>(this)->createAndSetPlatformContext();
}

void QWebGLScreen::start()
{
    ws->start();

    // this will block until someone connects
    output_gl_diagnostics();
}

void QWebGLScreen::createAndSetPlatformContext()
{

    m_depth = 24;
    m_format = QImage::Format_ARGB32;



    //    EGLConfig config = q_configFromGLFormat(m_dpy, platformFormat);

    //    EGLNativeWindowType eglWindow = 0;
#ifdef Q_OPENKODE
    if (Q_UNLIKELY(kdInitializeNV() == KD_ENOTINITIALIZED))
        qFatal("Did not manage to initialize openkode");

    KDWindow *window = kdCreateWindow(m_dpy,config,0);

    kdRealizeWindow(window,&eglWindow);
#endif

#ifdef QEGL_EXTRA_DEBUG
    //    q_printEglConfig(m_dpy, config);
#endif

    //    m_surface = eglCreateWindowSurface(m_dpy, config, eglWindow, NULL);
    //    if (Q_UNLIKELY(m_surface == EGL_NO_SURFACE)) {
    //        qWarning("Could not create the egl surface: error = 0x%x\n", eglGetError());
    //        eglTerminate(m_dpy);
    //        qFatal("EGL error");
    //    }
    //    //    qWarning("Created surface %dx%d\n", w, h);

    //    QEGLPlatformContext *platformContext = new QMinimalEglContext(platformFormat, 0, m_dpy);

    //    EGLint w,h;                    // screen size detection
    //    eglQuerySurface(m_dpy, m_surface, EGL_WIDTH, &w);
    //    eglQuerySurface(m_dpy, m_surface, EGL_HEIGHT, &h);

    m_geometry = QRect(0,0,1000,1000);

}

QRect QWebGLScreen::geometry() const
{
    if (m_geometry.isNull()) {
        createAndSetPlatformContext();
    }
    qDebug() << "Geometry" << m_geometry;

    return m_geometry;
}

int QWebGLScreen::depth() const
{
    return m_depth;
}

QImage::Format QWebGLScreen::format() const
{
    if (m_format == QImage::Format_Invalid)
        createAndSetPlatformContext();
    return m_format;
}
QPlatformOpenGLContext *QWebGLScreen::platformContext() const
{
    if (!m_platformContext) {
        QWebGLScreen *that = const_cast<QWebGLScreen *>(this);
        that->createAndSetPlatformContext();
    }
    return m_platformContext;
}

QT_END_NAMESPACE
