#include "qhtml5input.h"

#include <qpa/qwindowsysteminterface.h>
#include <QWindow>
#include <iostream>
#include <sys/time.h>

#include <QDebug>
#include "qwebglscreen.h"


void QHTML5Input::addWindow(QWebGLScreen * s, QWindow *win)
{
    screen = s;
    _qwindow = win;
}

QHTML5Input::QHTML5Input()
    :
      _qwindow(NULL),
      m_shouldStop(false)
{

}

#if 0

extern "C"
void setMouse(int16_t x, int16_t y, uint8_t buttons)
{
    if (!screen)
        return;

    struct timeval tv;  // removed comma

    gettimeofday (&tv, NULL);
    ulong ts = (tv.tv_sec*1000) + (tv.tv_usec/1000);

    //            static void handleMouseEvent(QWindow *w, ulong timestamp, const QPointF & local, const QPointF & global, Qt::MouseButtons b,
    //                                     Qt::KeyboardModifiers mods = Qt::NoModifier,
    //                                     Qt::MouseEventSource source = Qt::MouseEventNotSynthesized);

    QWindowSystemInterface::handleMouseEvent(QHTML5Input::get()._qwindow, ts, QPointF(x,y), QPointF(x,y), (Qt::MouseButtons)buttons);

//    qDebug() << "ts:" << ts << " pt" <<  QPointF(x,y) << " " << buttons;
}

extern "C"
void setNewSize(int16_t width, int16_t height)
{

    if (!screen)
        return;

    QRect rect(0, 0, width, height);
    qDebug() << "New screen size: " << rect;

    if (screen)
        screen->setGeometry(rect);
    QHTML5Input::get()._qwindow->setGeometry(rect);

    // these commands seem useless :D
    QWindowSystemInterface::handleScreenGeometryChange(QHTML5Input::get()._qwindow->screen(), rect, rect);
    QWindowSystemInterface::handleGeometryChange(QHTML5Input::get()._qwindow, rect);
}
#endif





#if 0
void QHTML5Input::handleMouseEvents(const DFBEvent &event)
{
    QPoint p(event.window.x, event.window.y);
    QPoint globalPos(event.window.cx, event.window.cy);
    //    Qt::MouseButtons buttons = QDirectFbConvenience::mouseButtons(event.window.buttons);

    //    QDirectFBPointer<IDirectFBDisplayLayer> layer(QDirectFbConvenience::dfbDisplayLayer());
    //    QDirectFBPointer<IDirectFBWindow> window;
    //    layer->GetWindow(layer.data(), event.window.window_id, window.outPtr());

    //    long timestamp = (event.window.timestamp.tv_sec*1000) + (event.window.timestamp.tv_usec/1000);

    QWindowSystemInterface::handleMouseEvent(_qwindow, timestamp, p, globalPos, buttons);
}


void QHTML5Input::handleKeyEvents(const DFBEvent &event)
{
    //    QEvent::Type type = QDirectFbConvenience::eventType(event.window.type);
    //    Qt::Key key = QDirectFbConvenience::keyMap()->value(event.window.key_symbol);
    //    Qt::KeyboardModifiers modifiers = QDirectFbConvenience::keyboardModifiers(event.window.modifiers);

    //    long timestamp = (event.window.timestamp.tv_sec*1000) + (event.window.timestamp.tv_usec/1000);

    //    QChar character;
    //    if (DFB_KEY_TYPE(event.window.key_symbol) == DIKT_UNICODE)
    //        character = QChar(event.window.key_symbol);

    QWindowSystemInterface::handleKeyEvent(_qwindow, timestamp, type, key, modifiers, character);
}


void QHTML5Input::handleGeometryEvent(const DFBEvent &event)
{

    //    QRect rect(event.window.x, event.window.y, event.window.w, event.window.h);
    QWindowSystemInterface::handleGeometryChange(_qwindow, rect);
}




void QHTML5Input::handleEvents()
{
    //    DFBResult hasEvent = m_eventBuffer->HasEvent(m_eventBuffer.data());
    //    while(hasEvent == DFB_OK){
    //        DFBEvent event;
    //        DFBResult ok = m_eventBuffer->GetEvent(m_eventBuffer.data(), &event);
    //        if (ok != DFB_OK)
    //            DirectFBError("Failed to get event",ok);
    //        if (event.clazz == DFEC_WINDOW) {
    //            switch (event.window.type) {
    //            case DWET_BUTTONDOWN:
    //            case DWET_BUTTONUP:
    //            case DWET_MOTION:
    //                handleMouseEvents(event);
    //                break;
    //            case DWET_WHEEL:
    //                handleWheelEvent(event);
    //                break;
    //            case DWET_KEYDOWN:
    //            case DWET_KEYUP:
    //                handleKeyEvents(event);
    //                break;
    //            case DWET_ENTER:
    //            case DWET_LEAVE:
    //                handleEnterLeaveEvents(event);
    //                break;
    //            case DWET_GOTFOCUS:
    //                handleGotFocusEvent(event);
    //                break;
    //            case DWET_CLOSE:
    //                handleCloseEvent(event);
    //                break;
    //            case DWET_POSITION_SIZE:
    //                handleGeometryEvent(event);
    //                break;
    //            default:
    //                break;
    //            }

    //        }

    //        hasEvent = m_eventBuffer->HasEvent(m_eventBuffer.data());
    //    }
}


void QHTML5Input::run()
{
    while (!m_shouldStop) {
        //        if (m_eventBuffer->WaitForEvent(m_eventBuffer.data()) == DFB_OK)
        handleEvents();
    }
}

void QDirectFbInput::stopInputEventLoop()
{
    m_shouldStop = true;
}
#endif
