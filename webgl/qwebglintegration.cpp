/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the plugins of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl-3.0.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or (at your option) the GNU General
** Public license version 3 or any later version approved by the KDE Free
** Qt Foundation. The licenses are as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL2 and LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-2.0.html and
** https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qwebglintegration.h"

#include "qwebglwindow.h"
#include "qwebglbackingstore.h"


#if defined(Q_OS_MAC)
#include <qpa/qplatformfontdatabase.h>
#else
#include <QtPlatformSupport/private/qgenericunixfontdatabase_p.h>
#endif

#if defined(Q_OS_UNIX)
#  include <QtPlatformSupport/private/qgenericunixeventdispatcher_p.h>
#elif defined(Q_OS_WINRT)
#  include <QtCore/private/qeventdispatcher_winrt_p.h>
#  include <QtGui/qpa/qwindowsysteminterface.h>
#elif defined(Q_OS_WIN)
#  include <QtPlatformSupport/private/qwindowsguieventdispatcher_p.h>
#endif
//#include <QPlatformOpenGLContext>
#include <qpa/qplatformnativeinterface.h>
#include <qpa/qplatformwindow.h>
#include <QtGui/QSurfaceFormat>
#include <QtGui/QOpenGLContext>
#include <QtGui/QScreen>
//#include <EGL/egl.h>
#include <qpa/qplatforminputcontextfactory_p.h>
#include "qhtml5input.h"

QT_BEGIN_NAMESPACE


QWebGLIntegration::QWebGLIntegration()
    : /*mFontDb(new QGenericUnixFontDatabase()), */mScreen(new QWebGLScreen())
{
#if defined(Q_OS_MAC)
    m_fontDatabase = new QPlatformFontDatabase();
#else
    m_fontDatabase = new QGenericUnixFontDatabase();
#endif
    m_inputContext = QPlatformInputContextFactory::create();

    screenAdded(mScreen, true);
    mScreen->start();

#ifdef QEGL_EXTRA_DEBUG
    qWarning("QMinimalEglIntegration\n");
#endif
}

QWebGLIntegration::~QWebGLIntegration()
{
    destroyScreen(mScreen);
}

bool QWebGLIntegration::hasCapability(QPlatformIntegration::Capability cap) const
{
    switch (cap) {
    case OpenGL: return true;
    default: return QPlatformIntegration::hasCapability(cap);
    }
}

QPlatformWindow *QWebGLIntegration::createPlatformWindow(QWindow *window) const
{
#ifdef QEGL_EXTRA_DEBUG
    qWarning("QMinimalEglIntegration::createPlatformWindow %p\n",window);
#endif
    QPlatformWindow *w = new QWebGLWindow(mScreen, window, &QHTML5Input::get());
    window->setGeometry(w->geometry());
    w->requestActivateWindow();

    return w;
}


QPlatformBackingStore *QWebGLIntegration::createPlatformBackingStore(QWindow *window) const
{
#ifdef QEGL_EXTRA_DEBUG
    qWarning("QMinimalEglIntegration::createWindowSurface %p\n", window);
#endif
    return new QWebGLBackingStore(window);
}

QPlatformOpenGLContext *QWebGLIntegration::createPlatformOpenGLContext(QOpenGLContext *context) const
{
#ifdef QEGL_EXTRA_DEBUG
    qWarning("QMinimalEglIntegration::createPlatformOpenGLContext %p\n", context);
#endif

    return static_cast<QWebGLScreen *>(context->screen()->handle())->platformContext();
}

//QPlatformFontDatabase *QMinimalEglIntegration::fontDatabase() const
//{
//    return mFontDb;
//}

QAbstractEventDispatcher *QWebGLIntegration::createEventDispatcher() const
{
#if defined(Q_OS_UNIX)
    return createUnixEventDispatcher();
#elif defined(Q_OS_WINRT)
    return new QWinRTEventDispatcher;
#elif defined(Q_OS_WIN)
    return new QWindowsGuiEventDispatcher;
#else
    return Q_NULLPTR;
#endif
}

QVariant QWebGLIntegration::styleHint(QPlatformIntegration::StyleHint hint) const
{
    if (hint == QPlatformIntegration::ShowIsFullScreen)
        return true;

    return QPlatformIntegration::styleHint(hint);
}

QPlatformFontDatabase *QWebGLIntegration::fontDatabase() const
{
    return m_fontDatabase;
}



QT_END_NAMESPACE
