PLUGIN_TYPE = platforms
PLUGIN_CLASS_NAME = QWebGLIntegrationPlugin


LIBS += -L/usr/local/lib -lboost_system -lboost_regex -lssl -lcrypto

!equals(TARGET, $$QT_DEFAULT_QPA_PLUGIN): PLUGIN_EXTENDS = -
load(qt_plugin)

LIBS += -lQt5PlatformSupport

QMAKE_CXXFLAGS += -fPIC
QMAKE_LDFLAGS += -fPIC

INCLUDEPATH += $$PWD/../  $$PWD/../webglstreampp/3rdparty
LIBS += -L$$OUT_PWD/build/build-webglstreampp/webglstreampp/
TARGET = qwebgl

DEFINES += QEGL_EXTRA_DEBUG
DEFINES += QT_OPENGL_ES_2

QT += core-private gui-private platformsupport-private

QMAKE_EXTRA_TARGETS += webgl

#Avoid X11 header collision
DEFINES += MESA_EGL_NO_X11_HEADERS

SOURCES =   main.cpp \
    qwebglbackingstore.cpp \
    qwebglwindow.cpp \
    qwebglintegration.cpp \
    qwebglscreen.cpp \
    qhtml5input.cpp


HEADERS =   \
    qwebglbackingstore.h \
    qwebglintegration.h \
    qwebglscreen.h \
    qwebglwindow.h \
    qhtml5input.h

CONFIG +=  qpa/genericunixfontdatabase exceptions

OTHER_FILES += \
    webgl.json


