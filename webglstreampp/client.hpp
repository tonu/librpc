#ifndef CLIENT_H
#define CLIENT_H

#include <map>

#include "functions/Commands.hpp"
#include "fifo.hpp"
#include "functions/gles_includes.hpp"



class Client
{
public:
    void init(std::shared_ptr<FIFO> fifoOut, std::shared_ptr<FIFO> fifoIn);

    void * getProcAddress(const char * name) const;

    template<typename Z, typename T, typename N>
    auto exec(T client_out, N client_in, GLCommand_t cmd) -> Z;

    static Client * get();

    void stop() {
         std::cerr << "stop called\n";
	 _run = false;
          sleep(1);
     } // fixme need to wait atleast until exec done. use waitcond

     Client(Client const&) = delete;             // Copy construct
     Client(Client&&) = delete;                  // Move construct
     Client& operator=(Client const&) = delete;  // Copy assign
     Client& operator=(Client &&) = delete;      // Move assign

     Client();
private:

    std::map<std::string, void *> _procs;

    std::shared_ptr<FIFO> _fifoOut;
    std::shared_ptr<FIFO> _fifoIn;


    bool _run;
};


extern Client staticInstance;


template<typename Z, typename T, typename N>
auto Client::exec(T client_out, N client_in, GLCommand_t cmd) -> Z
{
    assert(_run == true);

    try
    {
        //        usleep(1000000);

        *_fifoOut << arg<uint16_t>(cmd);

        // send cmd to server.
        bool needToWait = returns(cmd);

        DBG_TRAFFIC("\ncmd: " << gles_names[cmd]);

        client_out(*_fifoOut);
        _fifoOut->pushBuf(false);

        // wait for reply if need to return something
        if (needToWait)
        {
            while (_fifoIn->popBuf(false) == false)
                usleep(100);

            arg<uint16_t> retCmd;
            *_fifoIn >> retCmd;

            assert(retCmd.val > 0);
            ASSERT((retCmd.val == cmd), "ERROR: cmd:" << gles_names[cmd] << ", but reply was about:" << gles_names[retCmd.val].c_str());

            DBG_TRAFFIC(" reply.. ");

            return client_in(*_fifoIn);
        }

        DBG_TRAFFIC("\n");
    }
    catch (std::string & e)
    {
        std::cerr << "ERROR catched: " << e << "\n";

        assert(0);
    }

    // recheck: needToWait was false -> ret type must be void
    assert((std::is_same<Z, void>::value == true));


    // this is some trick to return with "return" from void returning func
    return static_cast<Z>(NULL);
}



#endif // CLIENT_H
