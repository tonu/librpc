#ifndef SERVER_H
#define SERVER_H

#include "functions/gles_enums.hpp"
#include "fifo.hpp"
#include <map>

class Server
{
public:

    typedef std::map<int, std::string, std::greater<int> > TopMap;

    Server(std::shared_ptr<FIFO> fifoIn, std::shared_ptr<FIFO> fifoOut);

    TopMap callsIn()  { return getSortedStats(_callsIn); }
    TopMap callsOut() { return getSortedStats(_callsOut); }
    TopMap bytesIn()  { return getSortedStats(_bytesIn); }
    TopMap bytesOut() { return getSortedStats(_bytesOut); }

    void setCallback(const std::string & name, void *ptr)
    {
        _callbacks.insert(std::pair<std::string, void*>(name, ptr));
    }

    void * callback(const std::string &name)
    {
        auto it = _callbacks.find(name);
        if (it != _callbacks.end())
            return it->second;

        return NULL;
    }

//    void run();
//    void stop() { doRun = false; _fifoIn->cancelRead();  }
    int step();

private:
    void updateStatsIn(GLCommand_t cmd, int size);
    void updateStatsOut(GLCommand_t cmd, int size);

    TopMap getSortedStats(const std::vector<int> &vec, int nFirst = 10);

    std::shared_ptr<FIFO> _fifoIn;
    std::shared_ptr<FIFO> _fifoOut;

    bool doRun;

    std::vector<int> _bytesIn;
    std::vector<int> _bytesOut;
    std::vector<int> _callsIn;
    std::vector<int> _callsOut;

    std::map<std::string, void*> _callbacks;
};

#endif // SERVER_H
