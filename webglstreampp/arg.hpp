#ifndef ARG_HPP
#define ARG_HPP

#include "fifo.hpp"
#include "buffer.hpp"

template<typename T>
class arg;

template< typename T>
inline FIFO& operator << (FIFO & s, const arg<T> & a); // intentionally not implemented
template< typename T>
inline FIFO& operator >> (FIFO & s, arg<T> & a);


template< typename T>
class arg
{
public:
    // init from copy of val
    arg(T v)
        :val(v)
    {

    }

    // uninitialized
    arg()
        :val()
    {

    }


    friend FIFO& operator << <> (FIFO & s, const arg & a);
    friend FIFO& operator >> <> (FIFO & s, arg & a);

    T val;
};



//float
template<>
inline FIFO& operator << (FIFO & s, const arg<float> & a)
{
    s.write(&a.val, 4);
    return s;
}
template<>
inline FIFO& operator >> (FIFO & s, arg<float> & a)
{
    s.read(&a.val, 4);
    return s;
}

//uint64
template<>
inline FIFO& operator << (FIFO & s, const arg<uint64_t> & a)
{
    s.write(&a.val, 8);
    return s;
}
template<>
inline FIFO& operator >> (FIFO & s, arg<uint64_t> & a)
{
    s.read(&a.val, 8);
    return s;
}


//uint32
template<>
inline FIFO& operator << (FIFO & s, const arg<uint32_t> & a)
{
    s.write(&a.val, 4);
    return s;
}
template<>
inline FIFO& operator >> (FIFO & s, arg<uint32_t> & a)
{
    s.read(&a.val, 4);
    return s;
}


//int32
template<>
inline FIFO& operator << (FIFO & s, const arg<int32_t> & a)
{
    s.write(&a.val, 4);
    return s;
}
template<>
inline FIFO& operator >> (FIFO & s, arg<int32_t> & a)
{
    s.read(&a.val, 4);
    return s;
}

//long
template<>//FIXME remove this
inline FIFO& operator << (FIFO & s, const arg<long> & a)
{
    s.write(&a.val, 4);
    return s;
}
template<>
inline FIFO& operator >> (FIFO & s, arg<long> & a)
{
    s.read(&a.val, 4);
    return s;
}


//uint16
template<>
inline FIFO& operator << (FIFO & s, const arg<uint16_t> & a)
{
    s.write(&a.val, 2);
    return s;
}
template<>
inline FIFO& operator >> (FIFO & s, arg<uint16_t> & a)
{
    s.read(&a.val, 2);
    return s;
}


//int16
template<>
inline FIFO& operator << (FIFO & s, const arg<int16_t> & a)
{
    s.write(&a.val, 2);
    return s;
}
template<>
inline FIFO& operator >> (FIFO & s, arg<int16_t> & a)
{
    s.read(&a.val, 2);
    return s;
}


//uint8
template<>
inline FIFO& operator << (FIFO & s, const arg<uint8_t> & a)
{
    s.write(&a.val, 1);
    return s;
}
template<>
inline FIFO& operator >> (FIFO & s, arg<uint8_t> & a)
{
    s.read(&a.val, 1);
    return s;
}


//int8
template<>
inline FIFO& operator << (FIFO & s, const arg<int8_t> & a)
{
    s.write(&a.val, 1);
    return s;
}
template<>
inline FIFO& operator >> (FIFO & s, arg<int8_t> & a)
{
    s.read(&a.val, 1);
    return s;
}

//buffer
template<typename N>
inline FIFO& operator << (FIFO & s, const arg<buffer<N>> & a)
{
    s << a.val;
    return s;
}
template<typename N>
inline FIFO& operator >> (FIFO & s, arg<buffer<N>> & a)
{
    s >> a.val;
    return s;
}


#endif // ARG_HPP
