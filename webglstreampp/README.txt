
Most important!
https://www.khronos.org/webgl/wiki/WebGL_and_OpenGL_Differences


// fixme .. move comment: get some help here  - https://src.chromium.org/blink/trunk/Source/modules/webgl/WebGLFramebuffer.cpp
// http://www.webchimera.org
// https://github.com/RSATom/Qt
// https://open.gl/framebuffers
// https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/Constants

// glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, 800, 600);
//#define GL_DEPTH_STENCIL_ATTACHMENT       0x821A
//funcs.glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, stencil_buffer);

// if using depth and stencil, then you must combine them into GL_DEPTH_STENCIL_ATTACHMENT

Emscripten
https://github.com/juj/emsdk

Retina:
https://www.khronos.org/webgl/wiki/HandlingHighDPI


http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-14-render-to-texture/

http://www.songho.ca/opengl/gl_fbo.html


Webgl API reference:
https://developer.mozilla.org/en-US/docs/Web/API/WebGLRenderingContext


Rendering depth to texture:
https://www.khronos.org/registry/webgl/extensions/WEBGL_depth_texture/


WebGL: drawElements: texture bound to texture unit 0 is not renderable.
 It maybe non-power-of-2 and have incompatible texture filtering or is not 'texture complete',
 or it is a float/half-float type with linear filtering and without the relevant float/half-float linear extension enabled.


 http://stackoverflow.com/questions/2795269/does-html5-canvas-support-double-buffering?rq=1


open -a "Google Chrome" --args --allow-file-access-from-files


 QSG:
 http://doc.qt.io/qt-5/qtquick-visualcanvas-scenegraph-renderer.html


see this error:
 WebGL: INVALID_FRAMEBUFFER_OPERATION: drawElements: conflicting DEPTH/STENCIL/DEPTH_STENCIL attachments
check this: https://www.khronos.org/registry/webgl/extensions/WEBGL_depth_texture/


http://www.html5rocks.com/en/tutorials/canvas/hidpi/



https://en.wikibooks.org/wiki/OpenGL_Programming/Scientific_OpenGL_Tutorial_05

https://www.researchgate.net/publication/221611809_Real-Time_Network_Streaming_of_Dynamic_3D_Content_with_In-frame_and_Inter-frame_Compression

https://github.com/shodruky-rhyammer/gl-streaming


env to debug render
QSG_RENDERER_DEBUG =
DECLARE_DEBUG_VAR(render)
DECLARE_DEBUG_VAR(build)
DECLARE_DEBUG_VAR(change)
DECLARE_DEBUG_VAR(upload)
DECLARE_DEBUG_VAR(roots)
DECLARE_DEBUG_VAR(dump)
DECLARE_DEBUG_VAR(noalpha)
DECLARE_DEBUG_VAR(noopaque)
DECLARE_DEBUG_VAR(noclip)

https://bost.ocks.org/mike/path/

http://www.arnorehn.de/blog/2014/12/quickplot-a-collection-of-native-qtquick-plot-items/


Qt build :
sudo apt-get install libssl-dev

mkdir build
cd build
~/qt-everywhere-opensource-src-5.7.0/configure -release -opensource -confirm-license -c++std c++11 -D QSG_SEPARATE_INDEX_BUFFER=1 -qt-freetype -qt-xcb -nomake tests -nomake examples -prefix ~/QtHack
make module-qtbase
make module-qtquickcontrols
make module-qtdeclarative

cd qtbase
make install
cd ..

cd qtquickcontrols
make install
cd ..

cd qtdeclarative
make install
cd ..





install.packages(c('Rcpp', 'RInside'))
ei toimi - compiler error

CFLAGS & CXXFLAGS.. -DNORET="" > ~/.R/Makevars
https://github.com/RcppCore/Rcpp/issues/519
http://stackoverflow.com/questions/38566225/not-able-to-compile-rcpp-0-12-6-under-r
http://stackoverflow.com/questions/24921628/failed-compiling-of-rinside-examples-in-linux

R CMD INSTALL RInside_0.2.11.tar.gz
R CMD INSTALL Rcpp_0.12.6.tar.gz


tonu@devbox:~/Package RProEkspert$ sudo R CMD INSTALL ./RProEkspert_0.0.1.tar.gz
* installing to library ‘/home/tonu/R/x86_64-pc-linux-gnu-library/3.0’
ERROR: this R is version 3.0.2, package 'RProEkspert' requires R >=  3.3.1

Depends: R (>= 3.3.1)


ERROR: dependencies ‘AnomalyDetection’, ‘BreakoutDetection’, ‘ggplot2’, ‘gtools’, ‘RODBC’ are not available for package ‘RProEkspert’


install.packages(c('ggplot2'))

package ‘ggplot2’ is not available (for R version 3.0.2)



$ sudo apt-get -y install r-base

$ sudo R
> install.packages('ggplot2')
> install.packages('gtools')
> install.packages('RODBC')
> install.packages('devtools')
> devtools::install_github("twitter/BreakoutDetection")
> devtools::install_github("twitter/AnomalyDetection")
ctrl-d and exit

$ sudo R CMD INSTALL ./RProEkspert_0.0.1.tar.gz



http://www.ibm.com/developerworks/aix/library/au-aix-symbol-visibility/