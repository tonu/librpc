#ifndef WSSERVER_H
#define WSSERVER_H

#include <functional>

#include "3rdparty/server_ws.hpp"
#include "fifo.hpp"

typedef SimpleWeb::SocketServer<SimpleWeb::WS> WsServer;
typedef SimpleWeb::SocketServer<SimpleWeb::WS>::Endpoint WsEndpoint;
typedef SimpleWeb::SocketServer<SimpleWeb::WS>::Connection WsConnection;

class WSServer
{
public:
    WSServer(uint16_t portnum, std::shared_ptr<FIFO> fifoOut, std::shared_ptr<FIFO> fifoIn);

    void start();
    void stop();

    std::function<void(void)> onConnect;
    std::function<void(void)> onClose;


private:
    std::shared_ptr<FIFO> _fifoIn;
    std::shared_ptr<FIFO> _fifoOut;

    WsServer _server;
    WsEndpoint & _endpoint;
    std::shared_ptr<WsConnection> _currentConnection;

    std::shared_ptr<std::thread> _thread1;
    std::shared_ptr<std::thread> _thread2;

    bool _run;
};

#endif // WSSERVER_H
