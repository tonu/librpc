#ifndef BUFFER_HPP
#define BUFFER_HPP

#include <string.h>

template<typename T>
class buffer
{
public:
    buffer(const void *buf, size_t sz) // sz is in elements of type T not bytes
        :_sz(sz)
    {

        if (_sz > 0 && buf != NULL)
        {
            _ptr = std::shared_ptr<T>( new T[sz] );
            memcpy(_ptr.get(), buf, _sz * sizeof (T));
        }
        else
        {
            _sz = 0;
            _ptr = std::shared_ptr<T>( NULL );
        }
    }

    buffer(size_t sz) // sz is in elements of type T not bytes
            :_sz(sz)
    {
        if (_sz > 0)
            _ptr = std::shared_ptr<T>( new T[sz] );
        else
            _ptr = std::shared_ptr<T>( NULL );
    }


    void resize(size_t sz) // sz is in elements of type T not bytes
    {
        if (sz == _sz)
            return;

        if (_sz <= 0)
        {
            _ptr = std::shared_ptr<T>(NULL);
            _sz = 0;

            return;
        }

        // need to realloc
        if (sz > _sz)
        {
            std::shared_ptr<T> newBuf = std::shared_ptr<T>(new T[sz]);
            memcpy(newBuf.get(), _ptr.get(), _sz * sizeof(T));
	
            _ptr = newBuf;
            _sz = sz;
            return;
        }

        //dont realloc
        if (sz < _sz)
        {
            _sz = sz;
            return;
        }
    }

    buffer()
        :_sz(0)
    {}

    friend FIFO& operator >>(FIFO & s, buffer<T> & arg)
    {
        // get size
        s.read(&arg._sz, sizeof(arg._sz));

        //if not empty buffer
        if (arg._sz > 0)
        {
            // (re)alloc
            arg._ptr = std::shared_ptr<T>( new T[arg._sz] );

            // read buf contents
            s.read(arg._ptr.get(), arg._sz * sizeof(T));
        }
        else
        {
            arg._ptr = std::shared_ptr<T>( NULL );

        }

        return s;
    }

    friend FIFO& operator << (FIFO & s, const buffer<T> & arg)
    {
        s.write(&arg._sz, sizeof(arg._sz));

        if (arg._sz > 0)
            s.write(arg._ptr.get(), arg._sz * sizeof(T));

        return s;
    }

    std::shared_ptr<T> _ptr;
    uint32_t _sz;

};


#endif // BUFFER_HPP
