#include "fifo.hpp"

#include <boost/date_time/posix_time/posix_time.hpp>
#include "functions/cmd_includes.hpp"
#include "Util.hpp"

#define BITS_TO_BYTES(n)  ((n)/8 + (((n)%8) > 0))

void FIFO::reset()
{
    _bufFrame->clear();
    _bufRead->clear();
    _bufWrite->clear();
    _bail = true;
}

/// [two functions for wiritng data frames to queue] ///////////////////////////////////////////////////////////////////////////

void FIFO::write(const void *ptr, size_t sz)
{
    assert(ptr);
    assert(sz > 0);

    _bufWrite->insert(_bufWrite->end(), (uint8_t *) ptr, (uint8_t *) ptr + sz);
}

void FIFO::pushBuf(bool flag)
{
    uint32_t szInBits;
    hf.encode(_bufWrite, szInBits);

    // encode flag into size
    szInBits <<= 1;
    szInBits |= flag;

    // encode size in a bit more efficient way. each byte contains 0-6 bits size-data and 7th carry bit
    std::vector<uint8_t> sizeBuf;
    while (szInBits)
    {
        uint8_t v = (szInBits & 0x7f);
        szInBits >>= 7;

        // check if need to carry and push
        sizeBuf.push_back((szInBits ? (v | 0x80) : v));
    }

    // special case.. if size 0 then push 0
    if (sizeBuf.size() == 0)
        sizeBuf.push_back(0);

    // insert size to the beginning
    _bufWrite->insert(_bufWrite->begin(), sizeBuf.begin(), sizeBuf.end());

    bool ret = _queue.enqueue(_bufWrite);
    if (!ret)
    {
        std::cerr <<  "FIXME.. handle somehow. throw or something\n";
        assert(0);
    }

    // replace buffer
    _bufWrite = std::make_shared<std::vector<uint8_t>>();
}

/// [two functions for reading data frames from queue] ///////////////////////////////////////////////////////////////////////////
bool FIFO::popBuf(bool notReturnFlag)
{
    if (_bail)
    {
        _bail = false;
        throw ResetException();
    }

    auto frame = popFrame();

    if (!frame)
    {
        return false;
    }

    // find first byte with no carry flag
    auto dataAt = frame->begin();
    uint32_t bits = 0;
    uint8_t nCarry = 0;
    for (; dataAt < frame->end(); dataAt++)
    {
        bits |= (*dataAt & 0x7f) << (7* nCarry);
        nCarry++;

        if ((*dataAt & 0x80) == false)
            break;
    }
    dataAt++;

    if (bool(bits & 0x01) == notReturnFlag)
    {
        _queue.enqueue(frame); // push it back to queue.. we dont want it
        return false;
    }


    //remove size
    frame->erase(frame->begin(), dataAt);
    hf.decode(frame, bits >> 1);

    _readLock.lock();
    _bufRead = frame;

    return true;
}

size_t FIFO::read(void *ptr, size_t sz)
{
    size_t i = std::min(_bufRead->size(), sz);

    if (i == 0)
    {
        return 0;
    }

    memcpy(ptr, &_bufRead->front(), i);
    _bufRead->erase(_bufRead->begin(), _bufRead->begin() + i);

    if (_bufRead->size() == 0)
    {
        _readLock.unlock();
    }
    return i;
}

/// [ two functions for reading from queue/sending out frame AND receiving and writing to queue ] ////

// SINGLE THREAD read outgoing data
std::shared_ptr<std::vector<uint8_t>> FIFO::popFrame()
{
    if (_bail)
    {
        _bail = false;
        throw ResetException();
    }

    std::shared_ptr<std::vector<uint8_t>> ret;
    if (_queue.try_dequeue(ret))
    {
        return ret;
    }

    return std::shared_ptr<std::vector<uint8_t>>(NULL);
}


// bits 0-6 data, if bit 7 == true, repeat same with next byte and next until 7. bit is false. LSB

// SIGNLE THREAD. write incoming data.
void FIFO::writeFrames(const void *p, uint32_t sz)
{
    const uint8_t *ptr = (const uint8_t *) p;

    // try to read n frames
    while (sz > 0)
    {
        //reading frame size
        if (!_haveFrameSize)
        {
            while (true)
            {
                bool carry = ((*ptr & 0x80) > 0);

                _frameBitSizeBuf |= (*ptr & 0x7f) << (7 * _nCarry);

                _bufFrame->push_back(*ptr);

                _nCarry++;
                ptr++;
                sz--;

                // no carry bit, all size read.
                if (!carry)
                    break;

                if (sz == 0)
                    return;
            }

            // ok - frame size is read into _frameSizeBuf
            _haveFrameSize = true;
        }

        // remember that 1st bit is return flag
        uint32_t frameSize = BITS_TO_BYTES(_frameBitSizeBuf >> 1);

        // reset
        _haveFrameSize = false;
        _frameBitSizeBuf  = 0;
        _sizeBytesRead = 0;
        _nCarry = 0;

        // size of frame data == 0; no data - DONE
        if (frameSize == 0)
            continue;

        // calc needed size
        int i = std::min(frameSize - _sizeBytesRead, sz);

        if (i)
        {
            _bufFrame->insert(_bufFrame->end(), (uint8_t *) ptr, (uint8_t *) ptr + i);
            ptr += i;
            sz -= i;
            _sizeBytesRead += i;


            // not full packet. wait for some more
            if (_sizeBytesRead < frameSize)
            {
                return;
            }

            // reset
            _haveFrameSize = false;
            _frameBitSizeBuf  = 0;
            _sizeBytesRead = 0;
            _nCarry        = 0;

            bool ret = _queue.enqueue(_bufFrame);
            assert (ret);

            _bufFrame = std::make_shared<std::vector<uint8_t>>();
        }

    }
}


