#include "wsserver.hpp"
#include "Util.hpp"

WSServer::WSServer(uint16_t portnum, std::shared_ptr<FIFO> fifoOut, std::shared_ptr<FIFO> fifoIn)
    : _fifoIn(fifoIn),
      _fifoOut(fifoOut),
      _server(portnum, 1),
      _endpoint(_server.endpoint["^/?$"]),
      _run(true)
{
//    _server.config.address = "127.0.0.1";

    _endpoint.onmessage = [this](std::shared_ptr<WsServer::Connection> connection,
            std::shared_ptr<WsServer::Message> message)
    {
        if (_currentConnection == connection)
        {
            auto sz = message->size();

            char *p = new char[sz];
            message->read(p, sz);

#ifdef DEBUG_ALL_IO
            {
                std::lock_guard<std::mutex> g(gPm);
                std::cerr << "IN:  [";
                for (int j = 0; j < sz; j++)
                    std::cerr << std::setw(2) << std::setfill('0') << std::hex << uint16_t(uint8_t(p[j])) << " ";
                std::cerr << " ]\n" << std::setw(0) << std::dec;
            }
#endif
            _fifoIn->writeFrames(p, sz);

        }
    };

    _endpoint.onopen = [this](std::shared_ptr<WsServer::Connection> connection)
    {
        if (!_currentConnection)
        {
            _currentConnection = connection;

            if (onConnect)
            {
                this->onConnect();
            }
        }

        std::cerr << "Server: Opened connection " << (size_t) connection.get() << std::endl;

    };

    //See RFC 6455 7.4.1. for status codes
    _endpoint.onclose = [this](std::shared_ptr<WsServer::Connection> connection, int status,
            const std::string & /*reason*/)
    {
        if (_currentConnection == connection)
        {
            if (onClose)
                this->onClose();

            _currentConnection.reset();
        }

        std::cerr << "Server: Closed connection " << (size_t) connection.get() << " with status code " << status
                  << std::endl;
    };

    //See http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html, Error Codes for error code meanings
    _endpoint.onerror = [this](std::shared_ptr<WsServer::Connection> connection, const boost::system::error_code &ec)
    {
        if (_currentConnection == connection)
        {
            if (onClose)
                this->onClose();

            _currentConnection.reset();
        }

        std::cerr << "Server: Error in connection " << (size_t) connection.get() << ". " << "Error: " << ec
                  << ", error message: " << ec.message() << std::endl;
    };


    _thread1 = std::make_shared<std::thread>(
                [this]
    {
        while (_run)
        {
            try
            {
                //first check if we have a connection .. if not, wait for one and until then dont pop any frames!
                if (_currentConnection)
                {
                    std::vector<uint8_t> buf; // collect more packets if possible

                    std::shared_ptr<std::vector<uint8_t>> frame;
                    while ((frame = _fifoOut->popFrame()))
                    {
#ifdef DEBUG_ALL_IO
                        {
                            std::lock_guard<std::mutex> g(gPm);
                            std::cerr << "OUT: [";
                            for (int j = 0; j < frame->size(); j++)
                                std::cerr << std::setw(2) << std::setfill('0') << std::hex
                                          << uint16_t(*(((const uint8_t *) frame->data()) + j)) << " ";
                            std::cerr << "]\n" << std::setw(0) << std::dec;
                        }
#endif

                        buf.insert(buf.end(), frame->begin(), frame->end());
                    }


                    // we have something to send!
                    if (buf.size() > 0)
                    {
                        auto send_stream = std::make_shared<WsServer::SendStream>();
                        send_stream->write((char*)&buf.front(), buf.size());

                        //server.send is an asynchronous function
                        _server.send(_currentConnection, send_stream,
                                     [](const boost::system::error_code &ec)
                        {
                            if (ec)
                            {
                                std::cerr
                                        << "Server: Error sending message. "
                                        <<
                                           //See http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html, Error Codes for error code meanings
                                           "Error: " << ec
                                        << ", error message: "
                                        << ec.message() << std::endl;
                            }
                        });
                    }
                }
            }
            catch (const char *err)
            {
                std::cerr << "err:" << err << "\n";
            }


            usleep(150);
        }
        //fixme wait cond when stopping. block stop until thread finishes.
    });
}


void WSServer::start()
{
    _thread2 = std::make_shared<std::thread>([this]
        {
            std::cerr <<"WSServerStart\n";
            _server.start();
            std::cerr <<"WSServerStop\n";
        });
}

void WSServer::stop()
{
//    _fifoIn->reset();
//    _fifoOut->reset();

    _run = false;
    _thread1->join();

    _server.stop();
    _thread2->join();
}
