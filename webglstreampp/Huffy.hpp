//
// Created by Tonu Jaansoo on 29/08/16.
//

#ifndef BUILD_ALL_HUFFY_HPP
#define BUILD_ALL_HUFFY_HPP

#include <vector>
#include <memory>

class Node;

class Huffy
{
public:
    Huffy();

    void encode(std::shared_ptr<std::vector<uint8_t> > &vec, uint32_t & bits);
    void decode(std::shared_ptr<std::vector<uint8_t>> &vec, uint32_t bits);


private:
    Node *root;
    std::unique_ptr<std::vector<std::pair< uint8_t , std::vector<bool>>>> codes;
};


#endif //BUILD_ALL_HUFFY_HPP
