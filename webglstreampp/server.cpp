#include "server.hpp"
#include "arg.hpp"

#include <iterator>
#include "functions/gles_access.hpp"

#ifdef BUILD_SERVER
//fixme .. missing in emscrpiten?
 GL_APICALL  GLboolean GL_APIENTRY glIsVertexArrayOES(GLuint array)
{
    return true;
}
#endif


Server::Server(std::shared_ptr<FIFO> fifoIn, std::shared_ptr<FIFO> fifoOut)
        : _fifoIn(fifoIn),
          _fifoOut(fifoOut),
          doRun(true),
          _bytesIn(GLCommand_MAX, 0),
          _bytesOut(GLCommand_MAX, 0),
          _callsIn(GLCommand_MAX, 0),
          _callsOut(GLCommand_MAX, 0)
{

}
//
//void Server::run()
//{
//    while (doRun)
//    {
//        step();
//    }
//}

int Server::step()
{
    arg<uint16_t> cmd;

    try
    {
        while (_fifoIn->popBuf(true)) // wait cmd (not cmd reply// accept if val & 0x8000 == false
        {
            *_fifoIn >> cmd;
            GLCommand_t c = (GLCommand_t) cmd.val;

            DBG_TRAFFIC("servcmd:" << gles_names[c]);

            // do statistics.. we have read in (incl sizeof( GLCommand_t ) )
            updateStatsIn(c, _fifoIn->readBufSize() + sizeof(GLCommand_t));

            //exec and if returns true, push data to client
            if (returns(c))
            {
                *_fifoOut << arg<uint16_t>(cmd.val); // return packet
                serv_in(this, c, *_fifoIn.get(), *_fifoOut.get());

                // update stats
                updateStatsOut(c, _fifoOut->writeBufSize());

                _fifoOut->pushBuf(true);
            }
            else
            {
                serv_in(this, c, *_fifoIn.get(), *_fifoOut.get());
            }

#ifdef EMSCRIPTEN
//            if (c != glGetError_enum)
//                GL_ERR_CHECK(c);
#endif
            if (c == glFlush_enum)
            {
                return 1;
            }
        }

    } catch (std::string & e)
    {
        std::cerr << "err: " << e << "\n";
        assert(0);
    }

    return 0;
}

void Server::updateStatsIn(GLCommand_t cmd, int size)
{
    _callsIn[cmd]++;
    _bytesIn[cmd] += size;
}

void Server::updateStatsOut(GLCommand_t cmd, int size)
{
    _callsOut[cmd]++;
    _bytesOut[cmd] += size;
}

Server::TopMap Server::getSortedStats(const std::vector<int> &vec, int nFirst)
{
    TopMap map;

    int sum = 0;
    for (int i = 0; i < vec.size(); i++)
    {
        map.insert(std::pair<int, std::string>(vec[i], gles_names[i]));
        sum += vec[i];
    }

    map.insert(std::pair<int, std::string>(sum, "TOTAL"));

    auto it = map.begin();
    for (int i=0; i<nFirst && it != map.end();i++)
        it++;

    if (it != map.end())
        map.erase(it, map.end());

    return map;
}

