#include <webglstreampp/server.hpp>
#include <webglstreampp/client.hpp>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <thread>
#include <emscripten/emscripten.h>
#include <fcntl.h>
#include <sstream>      // std::stringstream
#include <iomanip>
#include <emscripten/bind.h>
#include "../functions/WebCtx.hpp"

#include <arpa/inet.h>
//fixme
void setMouse(int16_t x, int16_t y, uint8_t buttons);
void setNewSize(int16_t width, int16_t height);

using namespace emscripten;


//#include "SDLGraphics.hpp"
#include <SDL/SDL.h>

SDL_Surface *screen;

#ifndef EMSCRIPTEN
#error Need to compile this with em++!
#endif

#ifndef __EMSCRIPTEN_PTHREADS__
#warning No threads!
#endif

#include <emscripten/html5.h>
#include <GLES2/gl2.h>

static std::string _currentStats;

std::string getStats()
{
    return _currentStats;
}

EMSCRIPTEN_BINDINGS(librpc) {
        function("getStats", &getStats);
}

void output_gl_diagnostics()
{
    auto gl_version    = reinterpret_cast< char const * >(glGetString(GL_VERSION));
    auto gl_renderer   = reinterpret_cast< char const * >(glGetString(GL_RENDERER));
    auto gl_vendor     = reinterpret_cast< char const * >(glGetString(GL_VENDOR));
    auto glsl_version  = reinterpret_cast< char const * >(glGetString(GL_SHADING_LANGUAGE_VERSION));
    auto gl_extensions = reinterpret_cast< char const * >(glGetString(GL_EXTENSIONS));

    if (!(gl_version && gl_renderer && gl_vendor && glsl_version))
    {
        std::cerr << "Could not obtain complete GL version info... GL context might not exist?!?\n";
    }

    std::cout << "GL version:" << (gl_version ? gl_version : "(null)") << std::endl;
    std::cout << "GL renderer:" << (gl_renderer ? gl_renderer : "(null)") << std::endl;
    std::cout << "GL vendor:" << (gl_vendor ? gl_vendor : "(null)") << std::endl;
    std::cout << "GLSL version:" << (glsl_version ? glsl_version : "(null)") << std::endl;
    std::cout << "GL extensions:" << (gl_extensions ? gl_extensions : "(null)") << std::endl;
}

void printStats(std::ostream &s, const Server::TopMap &map)
{
    for (auto it = map.begin(); it != map.end(); it++)
        s << std::setw(23) << std::left << it->second << std::setw(8) << std::right << it->first << "\n";

    s << std::setw(0);
}

void printStatsKB(std::ostream &s, const Server::TopMap &map)
{
    for (auto it = map.begin(); it != map.end(); it++)
        s << std::setw(23) << std::left << it->second << std::setw(8) << std::right << (it->first) / 1024 << "kB\n";

    s << std::setw(0);
}

static char buf[1000000];
static int         sockfd;

std::shared_ptr<FIFO> fifo_cli_to_srv(new FIFO);
std::shared_ptr<FIFO> fifo_srv_to_cli(new FIFO);

void close_sock()
{
    std::cerr << "closing socket\n";
    close(sockfd);
    sockfd = 0;
}

int open_sock(int portno, const char *addr)
{
    struct sockaddr_in dest;

    sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sockfd == -1)
    {
        perror("ERROR: Creating socket");
        sockfd = 0;
        return 0;
    }

//    std::cerr << "Created socket:" << sockfd << "\n";

    int fl;
    if ((fl = fcntl(sockfd, F_GETFL, 0)) >= 0)
    {
        if (fcntl(sockfd, F_SETFL, fl | O_NONBLOCK) < 0)
        {
            perror("ERROR: Creating socket");
            close_sock();
            return 0;
        }

    }

    memset(&dest, 0, sizeof(dest));                /* zero the struct */
    dest.sin_family      = AF_INET;
//    dest.sin_addr.s_addr = htonl(INADDR_LOOPBACK); /* set destination IP number - localhost, 127.0.0.1*/
    inet_aton(addr, &dest.sin_addr);
    dest.sin_port        = htons(portno);                /* set destination port number */

    int res              = connect(sockfd, (struct sockaddr *) &dest, sizeof(struct sockaddr));
    if (res < 0 && errno != EINPROGRESS)
    {
        close_sock();
        perror("ERROR: connect socket");
        return 0;
    }

    int       result;
    socklen_t result_len = sizeof(result);

    if (getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &result, &result_len) < 0)
    {
        // error, fail somehow, close socket
        close_sock();
        perror("ERROR: SO_ERROR");
        return 0;
    }

    return sockfd;
}


void doRead()
{
    int n;
    do
    {
        n = read(sockfd, buf, sizeof(buf));

        //on error
        if (n == -1 && errno != EAGAIN)
        {
            std::cerr << "err: code: " << n;
            perror("ERROR:read");
            close_sock();
            return;//FIXME: better throw - to disconnect
        }

        if (n > 0)
        {
            fifo_cli_to_srv->writeFrames(buf, n);
        }

        //disconnected
        if (n == 0)
        {
            close_sock();
        }
    } while (n > 0);
    //fbosse joonistada ja swapida
    //iga kord siit l2bi minnes tekstuur renderdada
}

void doWrite()
{
    do
    {
        auto frame = fifo_srv_to_cli->popFrame();

        if (frame)
        {
            int n;

            n = write(sockfd, &frame->front(), frame->size());
            if (n != frame->size())
            {
                std::cerr << "error!!\n";
            }

            //on error
            if (n == -1 && errno != EAGAIN)
            {
                std::cerr << "err: code: " << n;
                perror("ERROR:write");
                close_sock();
                return;
            }
        }
        else
        {
            break;
        }

    } while (1);
}

void doStats(Server *server)
{

    static int i = 0; i++;
    if (i % 100 == 0)
    {
        std::stringstream ss;

        ss << "Bytes in: \n";
        printStatsKB(ss, server->bytesIn());

        ss << "\n\nBytes Out: \n";
        printStatsKB(ss, server->bytesOut());

        ss << "\n\nCalls In: \n";
        printStats(ss, server->callsIn());

        ss << "\n\nCalls Out: \n";
        printStats(ss, server->callsOut());

        ss << "\n";

        _currentStats = ss.str();
    }

}

static inline const char *emscripten_event_type_to_string(int eventType) {
    const char *events[] = { "(invalid)", "(none)", "keypress", "keydown", "keyup", "click", "mousedown", "mouseup", "dblclick", "mousemove", "wheel", "resize",
                             "scroll", "blur", "focus", "focusin", "focusout", "deviceorientation", "devicemotion", "orientationchange", "fullscreenchange", "pointerlockchange",
                             "visibilitychange", "touchstart", "touchend", "touchmove", "touchcancel", "gamepadconnected", "gamepaddisconnected", "beforeunload",
                             "batterychargingchange", "batterylevelchange", "webglcontextlost", "webglcontextrestored", "(invalid)" };
    ++eventType;
    if (eventType < 0) eventType = 0;
    if (eventType >= sizeof(events)/sizeof(events[0])) eventType = sizeof(events)/sizeof(events[0])-1;
    return events[eventType];
}

EM_BOOL key_callback(int eventType, const EmscriptenKeyboardEvent *e, void *userData)
{
    //    printf("%s, key: \"%s\", code: \"%s\", location: %lu,%s%s%s%s repeat: %d, locale: \"%s\", char: \"%s\", charCode: %lu, keyCode: %lu, which: %lu\n",
    //           emscripten_event_type_to_string(eventType), e->key, e->code, e->location,
    //           e->ctrlKey ? " CTRL" : "", e->shiftKey ? " SHIFT" : "", e->altKey ? " ALT" : "", e->metaKey ? " META" : "",
    //           e->repeat, e->locale, e->charValue, e->charCode, e->keyCode, e->which);

    return 0;
}



EM_BOOL mouse_callback(int eventType, const EmscriptenMouseEvent *e, void *userData)
{
//        printf("%s, screen: (%ld,%ld), client: (%ld,%ld),%s%s%s%s button: %hu, buttons: %hu, movement: (%ld,%ld), canvas: (%ld,%ld), target: (%ld, %ld)\n",
//               emscripten_event_type_to_string(eventType), e->screenX, e->screenY, e->clientX, e->clientY,
//               e->ctrlKey ? " CTRL" : "", e->shiftKey ? " SHIFT" : "", e->altKey ? " ALT" : "", e->metaKey ? " META" : "",
//               e->button, e->buttons, e->movementX, e->movementY, e->canvasX, e->canvasY, e->targetX, e->targetY);

    int width;
    int height;
    int fullscreen;

    emscripten_get_canvas_size(&width, &height, &fullscreen);
    double dpi = emscripten_get_device_pixel_ratio();

    static int16_t x = 0;
    static int16_t y = 0;
    static uint8_t ev = 0;
    if (eventType == EMSCRIPTEN_EVENT_MOUSEDOWN)
        ev = 1;
    if (eventType == EMSCRIPTEN_EVENT_MOUSEUP)
        ev = 0;

    if (EMSCRIPTEN_EVENT_MOUSEMOVE)
    {
        x = e->canvasX;
        y = e->canvasY;
    }
    setMouse(x * dpi , y * dpi, ev);

    return 1; // return 1 if accepting
}


EM_BOOL uievent_callback(int eventType, const EmscriptenUiEvent *e, void *userData)
{
    //    printf("%s, detail: %ld, document.body.client size: (%d,%d), window.inner size: (%d,%d), scrollPos: (%d, %d)\n",
    //           emscripten_event_type_to_string(eventType), e->detail, e->documentBodyClientWidth, e->documentBodyClientHeight,
    //           e->windowInnerWidth, e->windowInnerHeight, e->scrollTop, e->scrollLeft);

    //    double DPI = emscripten_get_device_pixel_ratio();
    //    emscripten_set_canvas_size(e->windowInnerWidth * DPI, e->windowInnerHeight * DPI);

    if (eventType == EMSCRIPTEN_EVENT_RESIZE)
    {
        int width;
        int height;
        int fullscreen;

        emscripten_get_canvas_size(&width, &height, &fullscreen);
        setNewSize(width, height);
        WebCtx::get()->resizeFBO(width, height);

    }
    return 0;
}


void doRender(void *)
{
    WebCtx::get()->draw();
}

// main loop
void loop_iteration(void *arg)
{
    Server *server = (Server *) arg;

    if (sockfd > 0)
        doRead();

    if (server->step())
        emscripten_async_call(doRender, NULL, -1);// requestAnimationFrame

    if (sockfd > 0)
        doWrite();

    doStats(server);

    // FIXME need to do resize once each time needed.. this is costly bandwidth wise.
    static int initScreen = 0;
    initScreen++;
    if (initScreen == 200)
    {
        int width;
        int height;
        int fullscreen;
        emscripten_get_canvas_size(&width, &height, &fullscreen);
        setNewSize(width,  height);

        WebCtx::get()->resizeFBO(width, height);
    }
}


int main()
{
    Server server(fifo_cli_to_srv, fifo_srv_to_cli);
    Client::get()->init(fifo_srv_to_cli, fifo_cli_to_srv);

    if ((sockfd = open_sock(12345, "192.168.40.113")) > 0)
//    if ((sockfd = open_sock(12345, "127.0.0.1")) > 0)
    {
        std::cerr << "Connected! \n";
    }
    else
    {
        std::cerr << "Failed to connect! \n";
    }


    std::cerr << "starting ... \n";

    // WebGL
    EmscriptenWebGLContextAttributes attr;
    emscripten_webgl_init_context_attributes(&attr);

    attr.alpha                           = 1;
    attr.depth                           = 1;
    attr.stencil                         = 1;
    attr.antialias                       = 1;
    attr.preserveDrawingBuffer           = 0;
    attr.preferLowPowerToHighPerformance = 1;
    attr.failIfMajorPerformanceCaveat    = 0;
    attr.enableExtensionsByDefault       = 1;
    attr.premultipliedAlpha              = 0;
    attr.majorVersion                    = 1;
    attr.minorVersion                    = 0;

    EMSCRIPTEN_WEBGL_CONTEXT_HANDLE ctx = emscripten_webgl_create_context(0, &attr);
    emscripten_webgl_make_context_current(ctx);

    // mouse
    emscripten_set_click_callback(0, 0, 1, mouse_callback);
    emscripten_set_mousedown_callback(0, 0, 1, mouse_callback);
    emscripten_set_mouseup_callback(0, 0, 1, mouse_callback);
    emscripten_set_dblclick_callback(0, 0, 1, mouse_callback);
    emscripten_set_mousemove_callback(0, 0, 1, mouse_callback);

    // keyboard
    emscripten_set_keypress_callback(0, 0, 1, key_callback);
    emscripten_set_keydown_callback(0, 0, 1, key_callback);
    emscripten_set_keyup_callback(0, 0, 1, key_callback);

    // resize
    emscripten_set_resize_callback(0, 0, 1, uievent_callback);

    // main loop
    emscripten_set_main_loop_arg(loop_iteration, &server, 1000, 1);

    return 0;
}
