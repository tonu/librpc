#ifndef CMD_GLGETINTEGERV_HPP
#define CMD_GLGETINTEGERV_HPP
//typedef void          (*glGetIntegerv_ptr) (GLenum pname, GLint* params);


#include "cmd_includes.hpp"

namespace cmd_glGetIntegerv
{
static void client_out(FIFO &fifo, GLenum pname, GLint* params)
{
    if (!glesHasType(pname))
    {
        DBG_TRAFFIC("(wont do: unknown enum:" << pname << ")");
        return;
    }

    DBG_TRAFFIC("(pname=" << glesType(pname) << ", params=...)");
    if (pname == 0)
    {
        std::cerr << "cmd_glGetIntegerv called with 0\n";
    }
    fifo << arg<GLenum>(pname);
    fifo << arg<buffer<GLint>>( buffer<GLint>(params, 1)); // FIXME ... need to calc buf size ?
}

static void client_in(FIFO &fifo, GLint* params)
{
    arg<buffer<GLint>> p;

    fifo >> p;

    // fixme .. vaja mujal ka checkida, et null pole ... kui kutsutakse glGetIntegerv(..., NULL), sis ptr  on NULL siin
    if (p.val._sz > 0)
        DBG_TRAFFIC(" params[0]=" << p.val._ptr.get()[0]);

    memcpy(params, p.val._ptr.get(), p.val._sz * sizeof(GLint));
}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
{
    arg<GLenum> pname;
    arg<buffer<GLint>> params;

    fifoIn >> pname;
    fifoIn >> params;

#ifdef BUILD_SERVER
    glGetIntegerv(pname.val, params.val._ptr.get());
#endif
    fifoOut << params;

}

static bool returns() { return true; }
}

#endif // CMD_GLGETINTEGERV_HPP
