#ifndef GLSHADERSOURCE_HPP
#define GLSHADERSOURCE_HPP
#include <boost/regex.hpp>

//typedef void          (*glShaderSource_ptr) (GLuint shader, GLsizei count, const GLchar** string, const GLint* length);

#include "cmd_includes.hpp"

namespace cmd_glShaderSource
{
    static bool invalidChar (char c)
    {
        uint8_t i = uint8_t(c);
        return !(i>=0 && i <128);
    }

    static void stripUnicode(std::string & str)
    {
        str.erase( std::remove_if (str.begin(),str.end(), invalidChar), str.end());
    }

    static void client_out(FIFO &fifo, GLuint shader, GLsizei count, const GLchar *const*string, const GLint* length)
    {
        fifo << arg<GLuint>(shader);
        fifo << arg<GLsizei>(count);

        DBG_TRAFFIC("(shader=" << shader << ", count=" << count << ", source=..., length=...) converted GLSL source:---------------\n");
        for (int i=0; i<count; i++)
        {
            int len = (length == NULL ? strlen(string[i]) : length[i]);

            std::string line = std::string(string[i], len);

            line = boost::regex_replace(line, boost::regex("for\\s*\\(i "), "for (int i ");
//            line = boost::regex_replace(line, boost::regex("for \\(int i = 0; i < lightCount"), "for (int i = 0; i < 8");

//            line = boost::regex_replace(line, boost::regex("^in "), "attrib ");
//            line = boost::regex_replace(line, boost::regex("^out "), "attrib ");
//            line = boost::regex_replace(line, boost::regex("^#version [0-9]+"), "#version 100");

            std::vector<char> char_array(line.begin(), line.end());
            char_array.push_back(0);
            fifo << arg<buffer<char>>( buffer<char>(&char_array[0], char_array.size()));


            DBG_TRAFFIC("LINE:'\n"  <<  &char_array[0] << "'\n");

        }
        DBG_TRAFFIC("\n------------------------\n");
    }

    static void client_in(FIFO & fifo)
    {
//nothing
    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLuint> shader;
        arg<GLsizei> count;

        fifoIn >> shader >> count;

        std::vector<char *>  ptrs(count.val);
        std::vector<GLint> lengths(count.val);
        std::vector<arg<buffer<char>>> bufs;

        for (int i=0; i<count.val; i++)
        {
            arg<buffer<char>> buf;

            fifoIn >> buf;

            ptrs[i] = buf.val._ptr.get();
            lengths[i] = buf.val._sz -1; //excl null terminator

            bufs.push_back(buf);
        }



#ifdef BUILD_SERVER
        glShaderSource(shader.val, count.val, (const GLchar** )&ptrs.front(), (const GLint*)&lengths.front());
#endif
    }

    static bool returns() { return false; }

}



#endif // GLSHADERSOURCE_HPP
