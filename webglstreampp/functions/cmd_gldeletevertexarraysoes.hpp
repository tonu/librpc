#ifndef CMD_GLDELETEVERTEXARRAYSOES_HPP
#define CMD_GLDELETEVERTEXARRAYSOES_HPP
//    GL_APICALL void GL_APIENTRY glDeleteVertexArraysOES (GLsizei n, const GLuint *arrays);


#include "cmd_includes.hpp"

namespace cmd_glDeleteVertexArraysOES
{
    static void client_out(FIFO &fifo, GLsizei n, const GLuint *arrays)
    {
        fifo << arg<GLsizei>(n);
        fifo << arg<buffer<GLuint>>(buffer<GLuint>(arrays, n));
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
    {
        arg<GLsizei> n;
        arg<buffer<GLuint>> arrays;

        fifoIn >> n >> arrays;

#ifdef BUILD_SERVER
        glDeleteVertexArraysOES(n.val, arrays.val._ptr.get());
#endif
    }

    static bool returns() { return false; }
}

#endif // CMD_GLDELETEVERTEXARRAYSOES_HPP
