//
// Created by Tonu Jaansoo on 20/08/16.
//

#ifndef BUILD_ALL_CMD_GLGETATTRIBLOCATION_HPP
#define BUILD_ALL_CMD_GLGETATTRIBLOCATION_HPP
//    GL_APICALL GLint GL_APIENTRY glGetAttribLocation (GLuint program, const GLchar *name);

#include "cmd_includes.hpp"

namespace cmd_glGetAttribLocation
{
    static void client_out(FIFO &fifo, GLuint program, const GLchar *name)
    {
        DBG_TRAFFIC("(program=" << program << ", name=" << name << ")");

        fifo << arg<GLuint>(program);
        fifo << arg<buffer<char>>(buffer<char>(name, strlen(name) + 1));
    }

    static GLint client_in(FIFO &fifo)
    {
        arg<GLint> loc;

        fifo >> loc;

        DBG_TRAFFIC(" returned=" << loc.val);

        return loc.val;
    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLuint> program;
        arg<buffer<char>> name;

        fifoIn >> program >> name;

#ifdef BUILD_SERVER
        GLint loc =  glGetAttribLocation(program.val, name.val._ptr.get());
#else
        GLint loc = 0;
#endif

        fifoOut << arg<GLint>(loc);
    }

    static bool returns() { return true; }
}


#endif //BUILD_ALL_CMD_GLGETATTRIBLOCATION_HPP
