#ifndef CMD_GLCOMPILESHADER_H
#define CMD_GLCOMPILESHADER_H
//typedef void          (*glCompileShader_ptr) (GLuint shader);

#include "cmd_includes.hpp"
#include "gles_enums.hpp"

namespace cmd_glCompileShader
{
    static void client_out(FIFO &fifo, GLuint shader)
    {
        fifo << arg<GLuint>(shader);
        DBG_TRAFFIC("(shader="<< shader << ")");
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLuint> shader;
        fifoIn >> shader;

#ifdef BUILD_SERVER
        GLint compiled;

        glCompileShader(shader.val);

#if 0 //extra check
                GL_ERR_CHECK(glCompileShader_enum);

        // Check the compile status
        glGetShaderiv(shader.val, GL_COMPILE_STATUS, &compiled);
                GL_ERR_CHECK(glCompileShader_enum);

        if(!compiled)
        {
            GLint infoLen = 0;
            glGetShaderiv(shader.val, GL_INFO_LOG_LENGTH, &infoLen);
                GL_ERR_CHECK(glCompileShader_enum);

            if(infoLen > 1)
            {
                char* infoLog = static_cast<char*>(malloc(sizeof(char) * infoLen));
                glGetShaderInfoLog(shader.val, infoLen, NULL, infoLog);
                GL_ERR_CHECK(glCompileShader_enum);

                DBG_TRAFFIC("Error compiling shader:-----\n" <<  infoLog << "\n\n");

                free(infoLog);
                throw infoLog;
            }
        glDeleteShader(shader.val);
                GL_ERR_CHECK(glCompileShader_enum);
    }
        DBG_TRAFFIC("glCompileShader OK\n");
#endif
#endif

    }

    static bool returns() { return false; }
}
#endif // CMD_GLCOMPILESHADER_H
