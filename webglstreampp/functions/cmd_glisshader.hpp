//
// Created by Tonu Jaansoo on 23/08/16.
//

#ifndef BUILD_ALL_CMD_GLISSHADER_HPP
#define BUILD_ALL_CMD_GLISSHADER_HPP

//     GL_APICALL GLboolean GL_APIENTRY glIsShader (GLuint shader);


#include "cmd_includes.hpp"

namespace cmd_glIsShader
{
    static void client_out(FIFO &fifo, GLuint shader)
    {
        DBG_TRAFFIC("(shader=" << shader << ")");

        fifo << arg<GLuint>(shader);

    }

    static GLboolean client_in(FIFO &fifo)
    {
        arg<GLboolean> ret;

        fifo >> ret;

        DBG_TRAFFIC(" returned:" << int(ret.val));

        return ret.val;
    }

// what to execute on server
    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLuint> shader;

        fifoIn >> shader;


#ifdef BUILD_SERVER
        GLboolean ret = glIsShader(shader.val);
#else
        GLboolean ret = 0;
#endif

        fifoOut << arg<GLboolean>(ret);
    }

    static bool returns() { return true; }

}

#endif //BUILD_ALL_CMD_GLISSHADER_HPP
