//
// Created by Tonu Jaansoo on 19/08/16.
//

#ifndef BUILD_ALL_CMD_GLFLUSH_HPP
#define BUILD_ALL_CMD_GLFLUSH_HPP

namespace cmd_glFlush
{
    static void client_out(FIFO &)
    {
        DBG_TRAFFIC("()");
    }

    static void client_in(FIFO &)
    {

    }

    static void serv_in(Server *const, FIFO &, FIFO &)
    {
#ifdef BUILD_SERVER
        glFlush();
#endif
    }

    static bool returns() { return false; }
}

#endif //BUILD_ALL_CMD_GLFLUSH_HPP
