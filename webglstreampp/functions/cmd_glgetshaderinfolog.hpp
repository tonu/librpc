#ifndef CMD_GLGETSHADERINFOLOG_HPP
#define CMD_GLGETSHADERINFOLOG_HPP

//typedef void          (*glGetShaderInfoLog_ptr) (GLuint shader, GLsizei bufsize, GLsizei* length, GLchar* infolog);


#include "cmd_includes.hpp"

namespace cmd_glGetShaderInfoLog
{
static void client_out(FIFO & fifo,
                       GLuint shader, GLsizei bufsize, GLsizei* , GLchar* )
{
    fifo << arg<uint32_t>(shader);
    fifo << arg<int32_t>(bufsize); //max size we can take
    fifo << arg<buffer<int32_t>>(buffer<int32_t>(1)); //returns actual
    fifo << arg<buffer<char>>(buffer<char>(bufsize));
}

static void client_in(FIFO & fifo, GLsizei* length, GLchar* infolog)
{
    arg<int32_t> l;
    arg<buffer<char>> i;

    fifo >> l >> i;

    if (length)
    {
        *length = l.val;
    }

    memcpy(infolog, i.val._ptr.get(), l.val + 1);
}

// what to execute on server
static void serv_in(Server *const, FIFO & fifoIn, FIFO & fifoOut)
{
    arg<uint32_t> program;
    arg<int32_t> bufsize;
    arg<int32_t> length;
    arg<buffer<char>> infolog;

    // read params
    fifoIn >> program >> bufsize >> length >> infolog;

#ifdef BUILD_SERVER
    glGetShaderInfoLog(program.val, bufsize.val, &length.val, infolog.val._ptr.get());
#endif

    // send back
    fifoOut << length << infolog;
}

static bool returns() { return true; }
}

#endif // CMD_GLGETSHADERINFOLOG_HPP
