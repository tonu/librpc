#ifndef CMD_GLSTENCILOP_HPP
#define CMD_GLSTENCILOP_HPP
//typedef void          (*glStencilOp_ptr) (GLenum fail, GLenum zfail, GLenum zpass);

#include "cmd_includes.hpp"

namespace cmd_glStencilOp
{
    static void client_out(FIFO &fifo, GLenum fail, GLenum zfail, GLenum zpass)
    {
        DBG_TRAFFIC("(fail=" << glesType(fail) << ", zfail=" << glesType(zfail) << ", zpass=" << glesType(zpass) << ")");

        fifo << arg<GLenum>(fail);
        fifo << arg<GLenum>(zfail);
        fifo << arg<GLenum>(zpass);
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLenum> fail;
        arg<GLenum> zfail;
        arg<GLenum> zpass;

        fifoIn >> fail >> zfail >> zpass;

#ifdef BUILD_SERVER
        glStencilOp(fail.val, zfail.val, zpass.val);
#endif
    }

    static bool returns() { return false; }
}
#endif // CMD_GLSTENCILOP_HPP
