//
// Created by Tonu Jaansoo on 18/08/16.
//

#ifndef BUILD_ALL_CMD_GLISRENDERBUFFER_HPP
#define BUILD_ALL_CMD_GLISRENDERBUFFER_HPP
//    GL_APICALL GLboolean GL_APIENTRY glIsRenderbuffer (GLuint renderbuffer);


#include "cmd_includes.hpp"

namespace cmd_glIsRenderbuffer
{
    static void client_out(FIFO &fifo, GLuint renderbuffer)
{
    DBG_TRAFFIC("(renderbuffer=" << renderbuffer << ")");

    fifo << arg<GLuint>(renderbuffer);
}

static GLboolean client_in(FIFO &fifo)
{
    arg<GLboolean> r;
    fifo >> r;

    DBG_TRAFFIC(" returned:" <<  int(r.val));

    return r.val;
}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
{
    arg<GLuint> renderbuffer;
    fifoIn >> renderbuffer;

#ifdef BUILD_SERVER
    GLboolean ret = glIsRenderbuffer(renderbuffer.val);
#else
    GLboolean ret = 0;
#endif
    fifoOut << arg<GLboolean>(ret);
}

static bool returns() { return true; }
}

#endif //BUILD_ALL_CMD_GLISRENDERBUFFER_HPP
