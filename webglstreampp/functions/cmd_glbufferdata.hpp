#ifndef GLBUFFERDATA_HPP
#define GLBUFFERDATA_HPP
//typedef void          (*glBufferData_ptr) (GLenum target, GLsizeiptr size, const GLvoid* data, GLenum usage);
#include "cmd_includes.hpp"
#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>

namespace cmd_glBufferData
{
static void client_out(FIFO &fifo, GLenum target, GLsizeiptr size, const GLvoid* data, GLenum usage)
{
    DBG_TRAFFIC("(target=" << glesType(target) << ", size=" << size << ", data=..." << ", usage=" << glesType(usage) << ")");

    DBG_TRAFFIC_DATA("  data:[");
    std::ios::fmtflags f(std::cerr.flags() );
    DBG_TRAFFIC_DATA(std::setfill('0') <<  std::setw(2) << std::hex);
    for (int i=0;i<size;i++)
        DBG_TRAFFIC_DATA("0x" <<  int(((uint8_t*)data)[i]) << ", ");
    std::cerr.flags(f);
    DBG_TRAFFIC_DATA("]");

            fifo << arg<GLenum>(target);
    fifo << arg<GLsizeiptr>(size);
    fifo << arg<buffer<char>>(buffer<char>(data, size));
    fifo << arg<GLenum>(usage);
}

static void client_in(FIFO &fifo)
{

}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
{
    arg<GLenum> target;
    arg<GLsizeiptr> size;
    arg<buffer<char>> buffer;
    arg<GLenum> usage;

    //arg<uint32_t> program;
    fifoIn >> target >> size >> buffer >> usage;

#ifdef BUILD_SERVER
    glBufferData(target.val, size.val, buffer.val._ptr.get(), usage.val);
#endif
}

static bool returns() { return false; }
}
#endif // GLBUFFERDATA_HPP
