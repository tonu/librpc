#ifndef GLATTACHSHADER_HPP
#define GLATTACHSHADER_HPP

#include <iostream>
#include "cmd_includes.hpp"

namespace cmd_glAttachShader
{

    static void client_out(FIFO &fifo, GLuint program, GLuint shader)
    {
        DBG_TRAFFIC("(program=" << program << ", shader=" << shader << ")");

        fifo << arg<uint32_t>(program);
        fifo << arg<uint32_t>(shader);
    }

    static void client_in(FIFO &)
    {

    }

// what to execute on server
    static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
    {
        arg<uint32_t> program;
        arg<uint32_t> shader;

        fifoIn >> program >> shader;

#ifdef BUILD_SERVER
        glAttachShader(program.val, shader.val);
#endif
    }

    static bool returns() { return false; }
}

#endif // GLATTACHSHADER_HPP
