#ifndef CMD_GLDELETESHADER_HPP
#define CMD_GLDELETESHADER_HPP

//typedef void          (*glDeleteShader_ptr) (GLuint shader);

#include "cmd_includes.hpp"

namespace cmd_glDeleteShader
{
    static void client_out(FIFO &fifo, GLuint shader)
    {
        fifo << arg<GLuint>(shader);
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLuint> shader;
        fifoIn >> shader;

#ifdef BUILD_SERVER
        glDeleteShader(shader.val);
#endif
    }

    static bool returns() { return false; }
}

#endif // CMD_GLDELETESHADER_HPP
