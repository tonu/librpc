//
// Created by Tonu Jaansoo on 20/08/16.
//

#ifndef BUILD_ALL_CMD_GLPIXELSTOREI_HPP
#define BUILD_ALL_CMD_GLPIXELSTOREI_HPP

//    GL_APICALL void GL_APIENTRY glPixelStorei (GLenum pname, GLint param);

#include "cmd_includes.hpp"

namespace cmd_glPixelStorei
{
    static void client_out(FIFO &fifo, GLenum pname, GLint param)
    {
        if (!glesHasType(pname))
        {
            DBG_TRAFFIC("(wont do: unknown enum:" << pname << ")");
            return;
        }

        DBG_TRAFFIC("(pname=" << glesType(pname) << ", param=" << glesType(param) << ")");

        fifo << arg<GLenum>(pname);
        fifo << arg<GLint>(param);
    }

    static void client_in(FIFO &)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
    {
        arg<GLenum> pname;
        arg<GLint> param;

        fifoIn >> pname >> param;

#ifdef BUILD_SERVER
        glPixelStorei(pname.val, param.val);
#endif
    }

    static bool returns() { return false; }

}

#endif //BUILD_ALL_CMD_GLPIXELSTOREI_HPP
