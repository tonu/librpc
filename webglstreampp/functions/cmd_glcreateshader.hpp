#ifndef GLCREATESHADER_HPP
#define GLCREATESHADER_HPP
//typedef GLuint        (*glCreateShader_ptr) (GLenum type);

#include "cmd_includes.hpp"

namespace cmd_glCreateShader
{
static void client_out(FIFO &fifo, GLenum type)
{
    DBG_TRAFFIC("(type=" << glesType(type) << ")");
    fifo << arg<GLenum>(type);
}

static GLuint client_in(FIFO &fifo)
{
    arg<GLuint> ret;
    fifo >> ret;

    DBG_TRAFFIC(" return=" << ret.val);

    return ret.val;
}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
{
    arg<GLenum> type;
    fifoIn >> type;

#ifdef BUILD_SERVER
    GLuint ret = glCreateShader(type.val);

    if (ret == 0)
    {
        throw "error, glCreateShader ret == NULL";
    }
#else
    GLuint ret = 0;
#endif
    fifoOut << arg<GLuint>(ret);
}

static bool returns() { return true; }
}

#endif // GLCREATESHADER_HPP
