#ifndef CMD_GLGETPROGRAMIV_HPP
#define CMD_GLGETPROGRAMIV_HPP

//typedef void          (*glGetProgramiv_ptr) (GLuint program, GLenum pname, GLint* params);

#include "cmd_includes.hpp"

namespace cmd_glGetProgramiv
{
    static void client_out(FIFO &fifo, GLuint program, GLenum pname, GLint* params)
    {

        if (!glesHasType(pname))
        {
            DBG_TRAFFIC("(wont do: unknown enum:" << pname << ")");
            return;
        }

        DBG_TRAFFIC("(program=" << program << ", pname=" << glesType(pname) << ", params=...)");

        fifo << arg<GLuint>(program) << arg<GLenum>(pname) << arg<buffer<GLint>>(buffer<GLint>(params, 1));
    }

    static void client_in(FIFO &fifo, GLint* params)
    {
        arg<buffer<GLint>> pars;

        fifo >> pars;

        DBG_TRAFFIC(" params[0]=" << pars.val._ptr.get()[0]);

        memcpy(params, pars.val._ptr.get(), pars.val._sz * sizeof(GLint));
    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLuint> program;
        arg<GLenum> pname;
        arg<buffer<GLint>> params;

        fifoIn >> program >> pname >> params;

#ifdef BUILD_SERVER
        glGetProgramiv(program.val, pname.val, params.val._ptr.get());
#endif

        fifoOut << params;
    }

    static bool returns() { return true; }
}

#endif // CMD_GLGETPROGRAMIV_HPP
