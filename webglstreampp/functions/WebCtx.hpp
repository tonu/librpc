//
// Created by Tonu Jaansoo on 29/08/16.
//

#ifndef BUILD_ALL_WEBCTX_HPP
#define BUILD_ALL_WEBCTX_HPP

#define GL_GLEXT_PROTOTYPES
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <memory>
#include <iostream>

// iam lazy - http://gamedev.stackexchange.com/questions/102411/looking-for-specific-textured-quad-opengl-tutorial-in-c
class quad
{
public:
    quad(){
        GLfloat vertices[] = { // format = x, y, z, u, v
                -1.0f, -1.0f, 0.0f,  0.0f, 0.0f,     1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
                1.0f,  1.0f, 0.0f,  1.0f, 1.0f,    -1.0f,  1.0f, 0.0f, 0.0f, 1.0f
        };

        glGenVertexArraysOES(1, &vao); // vao saves state of array buffer, element array, etc
        glGenBuffers(1, &vbo); // vbo stores vertex data

        GLint curr_vao; // original state
        glGetIntegerv(0x8894, &curr_vao);

        glBindVertexArrayOES(vao);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);

        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float)*5, nullptr);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float)*5, (void*)(sizeof(float) * 3));

        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);

        glBindVertexArrayOES(curr_vao);
    }

    ~quad(){
        glDeleteVertexArraysOES(1, &vao);
        glDeleteBuffers(1, &vbo);
    }

    GLuint vao, vbo;
};




class shader_program
{
public:
    shader_program(const char *vertSrc, const char *fragSrc)
            :
            program(glCreateProgram()),
            vert(glCreateShader(GL_VERTEX_SHADER)),
            frag(glCreateShader(GL_FRAGMENT_SHADER))
    {


        glShaderSource(vert, 1, &vertSrc, nullptr);
        glShaderSource(frag, 1, &fragSrc, nullptr);

        glCompileShader(vert);
        check(vert);

        glCompileShader(frag);
        check(frag);
        // check shaders compiled. google glGetShaderInfoLog

        glBindAttribLocation(program, 0, "vertex");
        glBindAttribLocation(program, 1, "uv");

        glAttachShader(program, vert);
        glAttachShader(program, frag);
        glLinkProgram(program);
        // check program linked correctly. google glGetProgramInfoLog
    }

    ~shader_program()
    {
        glDeleteProgram(program);
        glDeleteShader(vert);
        glDeleteShader(frag);
    }

    void check(GLuint shader)
    {
        GLint status;
        glGetShaderiv (shader, GL_COMPILE_STATUS, &status);

        if (status == GL_FALSE)
        {
            GLint infoLogLength;

            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);
            GLchar *infoLog = new GLchar[infoLogLength];
            glGetShaderInfoLog(shader, infoLogLength, NULL, infoLog);
            std::cerr << "could not compile:" <<  infoLog << "\n";

            delete[] infoLog;
        }
    }

    GLuint program, vert, frag;
};


class WebCtx {
public:
    WebCtx();

    void draw();
    void resizeFBO(int width, int height);

    GLuint rttFBO;

    // must do first invocation with GL context current
    static std::shared_ptr<WebCtx> get()
    {
        if (!wCtx)
            wCtx = std::make_shared<WebCtx>();

        return wCtx;
    }

private:
    GLuint rttTexture;
    GLuint renderbuffer;

    quad quad;
    shader_program prog;

    static std::shared_ptr<WebCtx> wCtx;
};

#endif //BUILD_ALL_WEBCTX_HPP
