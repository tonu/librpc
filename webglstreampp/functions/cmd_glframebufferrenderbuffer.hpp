//
// Created by Tonu Jaansoo on 18/08/16.
//

#ifndef BUILD_ALL_CMD_GLFRAMEBUFFERRENDERBUFFER_HPP
#define BUILD_ALL_CMD_GLFRAMEBUFFERRENDERBUFFER_HPP
//    GL_APICALL void GL_APIENTRY glFramebufferRenderbuffer (GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer);

#include "cmd_includes.hpp"

namespace cmd_glFramebufferRenderbuffer
{
    static void client_out(FIFO &fifo, GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer)
{
    DBG_TRAFFIC("(target=" << glesType(target) << ", attachment=" << glesType(attachment) << ", renderbuffertarget=" << glesType(renderbuffertarget) << ", renderbuffer=" << renderbuffer << ")");
    fifo << arg<GLenum>(target);
    fifo << arg<GLenum>(attachment);
    fifo << arg<GLenum>(renderbuffertarget);
    fifo << arg<GLuint>(renderbuffer);
}

static void client_in(FIFO &fifo)
{

}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
{
    arg<GLenum> target;
    arg<GLenum> attachment;
    arg<GLenum> renderbuffertarget;
    arg<GLuint> renderbuffer;

    fifoIn >> target >> attachment >> renderbuffertarget >> renderbuffer;
#ifdef BUILD_SERVER
    glFramebufferRenderbuffer(target.val, attachment.val, renderbuffertarget.val, renderbuffer.val);
#endif
}

static bool returns() { return false; }
}

#endif //BUILD_ALL_CMD_GLFRAMEBUFFERRENDERBUFFER_HPP
