#ifndef CMD_GLUNIFORM3FV_HPP
#define CMD_GLUNIFORM3FV_HPP
//typedef void          (*glUniform3fv_ptr) (GLint location, GLsizei count, const GLfloat* v);


#include "cmd_includes.hpp"

namespace cmd_glUniform3fv
{
    static void client_out(FIFO &fifo, GLint location, GLsizei count, const GLfloat* v)
    {
        DBG_TRAFFIC("(location=" << location << ", count=" << count << ", v=...)");

        fifo << arg<GLint>(location);
        fifo << arg<GLsizei>(count);
        fifo << arg<buffer<GLfloat>>(buffer<GLfloat>(v, 3 * count));
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
    {
        arg<GLint> location;
        arg<GLsizei> count;
        arg<buffer<GLfloat>> v;

        fifoIn >> location >> count >> v;

#ifdef BUILD_SERVER
        glUniform3fv(location.val, count.val, v.val._ptr.get());
#endif
    }

    static bool returns() { return false; }
}
#endif // CMD_GLUNIFORM3FV_HPP
