//
// Created by Tonu Jaansoo on 15/08/16.
//

#ifndef CMD_GLACTIVETEXTURE_HPP
#define CMD_GLACTIVETEXTURE_HPP

#include "cmd_includes.hpp"

namespace cmd_glActiveTexture
{

static void client_out(FIFO &fifo, GLenum texture)
{
    DBG_TRAFFIC("(texture=" << glesType(texture) << ")");
    fifo << arg<GLenum>(texture);
}

static void client_in(FIFO &fifo)
{

}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
{
    arg<GLenum> texture;
    fifoIn >> texture;

#ifdef BUILD_SERVER
    glActiveTexture(texture.val);
#endif
}

static bool returns() { return false; }
}

#endif
