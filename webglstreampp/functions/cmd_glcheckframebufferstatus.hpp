//
// Created by Tonu Jaansoo on 19/08/16.
//

#ifndef BUILD_ALL_CMD_GLCHECKFRAMEBUFFERSTATUS_HPP
#define BUILD_ALL_CMD_GLCHECKFRAMEBUFFERSTATUS_HPP

//    GL_APICALL GLenum GL_APIENTRY glCheckFramebufferStatus (GLenum target);

#include "cmd_includes.hpp"

namespace cmd_glCheckFramebufferStatus
{
    static void client_out(FIFO &fifo, GLenum target)
{
    DBG_TRAFFIC("(target=" << glesType(target) << ")");

    fifo << arg<GLenum>(target);
}

static GLenum client_in(FIFO &fifo)
{
    arg<GLenum> t;

    fifo >> t;

    DBG_TRAFFIC(" returned:" << glesType(t.val));

    if (t.val != GL_FRAMEBUFFER_COMPLETE)
    {
        std::cerr << "...\n";
    }

   // assert(t.val == GL_FRAMEBUFFER_COMPLETE);
    return t.val;
}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
{
    arg<GLenum> target;

    fifoIn >> target;

#ifdef BUILD_SERVER
    GLenum ret = glCheckFramebufferStatus(target.val);
#else
    GLenum ret = GL_FRAMEBUFFER_COMPLETE;
#endif

    fifoOut << arg<GLenum>(ret);
}

static bool returns() { return true; }
}
#endif //BUILD_ALL_CMD_GLCHECKFRAMEBUFFERSTATUS_HPP
