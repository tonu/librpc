#ifndef CMD_GLGENTEXTURES_HPP
#define CMD_GLGENTEXTURES_HPP
//typedef void          (*glGenTextures_ptr) (GLsizei n, GLuint* textures);

#include "cmd_includes.hpp"

namespace cmd_glGenTextures
{
static void client_out(FIFO &fifo, GLsizei n, GLuint* textures)
{
    DBG_TRAFFIC("n=" << n << ", textures=...)");

    fifo << arg<GLsizei>(n);
    fifo << arg<buffer<GLuint>>(buffer<GLuint>(textures, n));
}

static void client_in(FIFO &fifo, GLuint* textures)
{
    arg<buffer<GLuint>> t;

    fifo >> t;

    memcpy(textures, t.val._ptr.get(), t.val._sz * sizeof(GLuint));

    DBG_TRAFFIC(" returned: textures[0]=" << textures[0]);
}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
{
    arg<GLsizei> n;
    arg<buffer<GLuint>> textures;

    fifoIn >> n >> textures;
#ifdef BUILD_SERVER
    glGenTextures(n.val, textures.val._ptr.get());
#endif

    fifoOut << textures;
}

static bool returns() { return true; }
}

#endif // CMD_GLGENTEXTURES_HPP
