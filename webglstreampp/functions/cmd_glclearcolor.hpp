#ifndef CMD_GLCLEARCOLOR_HPP
#define CMD_GLCLEARCOLOR_HPP

#include "cmd_includes.hpp"

namespace cmd_glClearColor
{
static void client_out(FIFO &fifo, GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha)
{
    fifo << arg<float>(red);
    fifo << arg<float>(green);
    fifo << arg<float>(blue);
    fifo << arg<float>(alpha);

    DBG_TRAFFIC("(red=" << red << ", green=" << green << ", blue=" << blue << ", alpha=" << alpha << ")");

}

static void client_in(FIFO &)
{

}

// what to execute on server
static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
{
    arg<float> red;
    arg<float> green;
    arg<float> blue;
    arg<float> alpha;

    fifoIn >> red >> green >> blue >> alpha;

#ifdef BUILD_SERVER

    //fixme hack
    alpha.val = 0;

    glClearColor(red.val, green.val, blue.val, alpha.val);
#endif
}

static bool returns() { return false; }

}


#endif // CMD_GLCLEARCOLOR_HPP
