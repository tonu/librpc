#ifndef CMD_GLBINDATTRIBLOCATION_H
#define CMD_GLBINDATTRIBLOCATION_H

#include "cmd_includes.hpp"

namespace cmd_glBindAttribLocation
{
    static void client_out(FIFO &fifo, GLuint program, GLuint index, const GLchar* name) // name is null term?
    {
        fifo << arg<GLuint>(program) << arg<GLuint>(index) << arg<buffer<char>>(buffer<char>(name, strlen(name) + 1));

        DBG_TRAFFIC("(program=" <<  program << " ,index=" << index << " ,name='" << name  << "')");
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLuint> program;
        arg<GLuint> index;
        arg<buffer<char>> name;

        fifoIn >> program >> index >> name;

#ifdef BUILD_SERVER
        glBindAttribLocation(program.val, index.val, name.val._ptr.get());
#endif
    }

    static bool returns() { return false; }
}

#endif // CMD_GLBINDATTRIBLOCATION_H
