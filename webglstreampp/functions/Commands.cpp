//
// Created by Tonu Jaansoo on 22/08/16.
//

#include "Commands.hpp"


#include "../client.hpp"

#define OUT(name, args...) (std::bind(cmd_##name::client_out, std::placeholders::_1, ##args))
#define IN(name, args...) (std::bind(cmd_##name::client_in,   std::placeholders::_1, ##args))

#define FP(otype, name, out, in)  Client::get()->exec<otype>(out, in, name##_enum); //if (name##_enum != glGetError_enum)  GL_ERR_CHECK( name##_enum);

#define ARRAY_BUF 0
#define ELEMENT_ARRAY_BUF 1

#ifndef EMSCRIPTEN

struct VBO
{
    GLint        size;
    GLenum       type;
    GLboolean    normalized;
    GLsizei      stride;
    const GLvoid *ptr;

    GLuint buffer;
    bool   enabled;
};

struct Ctx
{
    Ctx() {
        memset(this, 0, sizeof(struct Ctx));
    }

    bool bufferBound[2];

    VBO vbos[3];
};

static Ctx ctx = {}; //init

void emulateServerSideArrays(int count, int first)
{
    for (GLuint i = 0; i < 3; i++)
    {
        auto & vbo = ctx.vbos[i];
        if (vbo.enabled && vbo.ptr)
        {
            // one element
            int bufsize = vbo.size * (typeBits(vbo.type) / 8);

            // whole
            count += first;
            bufsize += (count - 1) * (vbo.stride > 0 ? vbo.stride : bufsize);

            if (bufsize > 0)
            {
                if (vbo.buffer == 0)
                {
                    FP(void, glGenBuffers,
                       OUT(glGenBuffers, 1, &vbo.buffer),
                       IN(glGenBuffers, &vbo.buffer));
                }

                FP(void, glBindBuffer,
                   OUT(glBindBuffer, GL_ARRAY_BUFFER, vbo.buffer),
                   IN(glBindBuffer));

                FP(void, glBufferData,
                   OUT(glBufferData,GL_ARRAY_BUFFER, bufsize, vbo.ptr, GL_STATIC_DRAW),
                   IN(glBufferData));

                FP(void, glVertexAttribPointer,
                   OUT(glVertexAttribPointer, i, vbo.size, vbo.type, vbo.normalized, vbo.stride, (const GLvoid *) NULL),
                   IN(glVertexAttribPointer));

                FP(void, glBindBuffer,
                   OUT(glBindBuffer, GL_ARRAY_BUFFER, 0),
                   IN(glBindBuffer));
            }

            // set ptr to NULL. No need to upload buf again until another ptr is set from VertexAtrribPtr
            vbo.ptr = NULL;
        }
    }

}


GL_APICALL void         GL_APIENTRY glActiveTexture(GLenum texture)
{
    FP(void, glActiveTexture,
       OUT(glActiveTexture, texture),
       IN(glActiveTexture));
}

GL_APICALL void         GL_APIENTRY glAttachShader(GLuint program, GLuint shader)
{
    FP(void, glAttachShader,
       OUT(glAttachShader, program, shader),
       IN(glAttachShader));
}

GL_APICALL void         GL_APIENTRY glBindAttribLocation(GLuint program, GLuint index, const GLchar *name)
{
    FP(void, glBindAttribLocation,
       OUT(glBindAttribLocation, program, index, name),
       IN(glBindAttribLocation));
}

GL_APICALL void         GL_APIENTRY glBindBuffer(GLenum target, GLuint buffer)
{
    FP(void, glBindBuffer,
       OUT(glBindBuffer, target, buffer),
       IN(glBindBuffer));

    ctx.bufferBound[(target == GL_ARRAY_BUFFER ? 0 : 1)] = (buffer > 0);
}

GL_APICALL void         GL_APIENTRY glBindFramebuffer(GLenum target, GLuint framebuffer)
{
    FP(void, glBindFramebuffer,
       OUT(glBindFramebuffer, target, framebuffer),
       IN(glBindFramebuffer));
}

GL_APICALL void         GL_APIENTRY glBindRenderbuffer(GLenum target, GLuint renderbuffer)
{
    FP(void, glBindRenderbuffer,
       OUT(glBindRenderbuffer, target, renderbuffer),
       IN(glBindRenderbuffer));
}

GL_APICALL void         GL_APIENTRY glBindTexture(GLenum target, GLuint texture)
{
    FP(void, glBindTexture,
       OUT(glBindTexture, target, texture),
       IN(glBindTexture));
}

GL_APICALL void         GL_APIENTRY glBlendColor(GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha)
{
    FP(void, glBlendColor,
       OUT(glBlendColor, red, green, blue, alpha),
       IN(glBlendColor));
}

GL_APICALL void         GL_APIENTRY glBlendEquation(GLenum mode)
{
    std::cerr << "Not implemented: 'glBlendEquation'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glBlendEquationSeparate(GLenum modeRGB, GLenum modeAlpha)
{
    std::cerr << "Not implemented: 'glBlendEquationSeparate'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glBlendFunc(GLenum sfactor, GLenum dfactor)
{
    FP(void, glBlendFunc,
       OUT(glBlendFunc, sfactor, dfactor),
       IN(glBlendFunc));
}

GL_APICALL void         GL_APIENTRY glBlendFuncSeparate(GLenum srcRGB, GLenum dstRGB, GLenum srcAlpha, GLenum dstAlpha)
{
    std::cerr << "Not implemented: 'glBlendFuncSeparate'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glBufferData(GLenum target, GLsizeiptr size, const GLvoid *data, GLenum usage)
{
    if (size == 0)
        return;

    FP(void, glBufferData,
       OUT(glBufferData, target, size, data, usage),
       IN(glBufferData));
}

GL_APICALL void         GL_APIENTRY glBufferSubData(GLenum target, GLintptr offset, GLsizeiptr size, const GLvoid *data)
{
    std::cerr << "Not implemented: 'glBufferSubData'\n";
    assert(0);
}

GL_APICALL GLenum       GL_APIENTRY glCheckFramebufferStatus(GLenum target)
{
    return FP(GLenum, glCheckFramebufferStatus,
              OUT(glCheckFramebufferStatus, target),
              IN(glCheckFramebufferStatus));
}

GL_APICALL void         GL_APIENTRY glClear(GLbitfield mask)
{
    FP(void, glClear,
       OUT(glClear, mask),
       IN(glClear));
}

GL_APICALL void         GL_APIENTRY glClearColor(GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha)
{
    FP(void, glClearColor,
       OUT(glClearColor, red, green, blue, alpha),
       IN(glClearColor));
}

GL_APICALL void         GL_APIENTRY glClearDepthf(GLclampf depth)
{
    FP(void, glClearDepthf,
       OUT(glClearDepthf, depth),
       IN(glClearDepthf));
}

GL_APICALL void         GL_APIENTRY glClearStencil(GLint s)
{
    FP(void, glClearStencil,
       OUT(glClearStencil, s),
       IN(glClearStencil));
}

GL_APICALL void         GL_APIENTRY glColorMask(GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha)
{
    FP(void, glColorMask,
       OUT(glColorMask, red, green, blue, alpha),
       IN(glColorMask));
}

GL_APICALL void         GL_APIENTRY glCompileShader(GLuint shader)
{
    FP(void, glCompileShader,
       OUT(glCompileShader, shader),
       IN(glCompileShader));
}

GL_APICALL void         GL_APIENTRY
glCompressedTexImage2D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border,
                       GLsizei imageSize, const GLvoid *data)
{
    std::cerr << "Not implemented: 'glCompressedTexImage2D'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY
glCompressedTexSubImage2D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height,
                          GLenum format, GLsizei imageSize, const GLvoid *data)
{
    std::cerr << "Not implemented: 'glCompressedTexSubImage2D'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY
glCopyTexImage2D(GLenum target, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLsizei height,
                 GLint border)
{
    FP(void, glCopyTexImage2D,
       OUT(glCopyTexImage2D, target, level, internalformat, x, y, width, height, border),
       IN(glCopyTexImage2D));
}

GL_APICALL void         GL_APIENTRY
glCopyTexSubImage2D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width,
                    GLsizei height)
{
    FP(void, glCopyTexSubImage2D,
       OUT(glCopyTexSubImage2D, target, level, xoffset, yoffset, x, y, width, height),
       IN(glCopyTexSubImage2D));
}

GL_APICALL GLuint       GL_APIENTRY glCreateProgram(void)
{
    return FP(GLuint, glCreateProgram,
              OUT(glCreateProgram),
              IN(glCreateProgram));
}

GL_APICALL GLuint       GL_APIENTRY glCreateShader(GLenum type)
{
    return FP(GLuint, glCreateShader,
              OUT(glCreateShader, type),
              IN(glCreateShader));
}

GL_APICALL void         GL_APIENTRY glCullFace(GLenum mode)
{
    FP(void, glCullFace,
       OUT(glCullFace, mode),
       IN(glCullFace));
}

GL_APICALL void         GL_APIENTRY glDeleteBuffers(GLsizei n, const GLuint *buffers)
{
    FP(void, glDeleteBuffers,
       OUT(glDeleteBuffers, n, buffers),
       IN(glDeleteBuffers));
}

GL_APICALL void         GL_APIENTRY glDeleteFramebuffers(GLsizei n, const GLuint *framebuffers)
{
    FP(void, glDeleteFramebuffers,
       OUT(glDeleteFramebuffers, n, framebuffers),
       IN(glDeleteFramebuffers));
}

GL_APICALL void         GL_APIENTRY glDeleteProgram(GLuint program)
{
    FP(void, glDeleteProgram,
       OUT(glDeleteProgram, program),
       IN(glDeleteProgram));
}

GL_APICALL void         GL_APIENTRY glDeleteRenderbuffers(GLsizei n, const GLuint *renderbuffers)
{
    FP(void, glDeleteRenderbuffers,
       OUT(glDeleteRenderbuffers, n, renderbuffers),
       IN(glDeleteRenderbuffers));
}

GL_APICALL void         GL_APIENTRY glDeleteShader(GLuint shader)
{
    FP(void, glDeleteShader,
       OUT(glDeleteShader, shader),
       IN(glDeleteShader));
}

GL_APICALL void         GL_APIENTRY glDeleteTextures(GLsizei n, const GLuint *textures)
{
    FP(void, glDeleteTextures,
       OUT(glDeleteTextures, n, textures),
       IN(glDeleteTextures));
}

GL_APICALL void         GL_APIENTRY glDepthFunc(GLenum func)
{
    FP(void, glDepthFunc,
       OUT(glDepthFunc, func),
       IN(glDepthFunc));
}

GL_APICALL void         GL_APIENTRY glDepthMask(GLboolean flag)
{
    FP(void, glDepthMask,
       OUT(glDepthMask, flag),
       IN(glDepthMask));
}

GL_APICALL void         GL_APIENTRY glDepthRangef(GLclampf zNear, GLclampf zFar)
{
    std::cerr << "Not implemented: 'glDepthRangef'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glDetachShader(GLuint program, GLuint shader)
{
    std::cerr << "Not implemented: 'glDetachShader'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glDisable(GLenum cap)
{
    FP(void, glDisable,
       OUT(glDisable, cap),
       IN(glDisable));
}

GL_APICALL void         GL_APIENTRY glDisableVertexAttribArray(GLuint index)
{
    ctx.vbos[index].enabled = false;
    FP(void, glDisableVertexAttribArray,
       OUT(glDisableVertexAttribArray, index),
       IN(glDisableVertexAttribArray));
}

GL_APICALL void         GL_APIENTRY glDrawArrays(GLenum mode, GLint first, GLsizei count)
{
    emulateServerSideArrays(count, first);

    FP(void, glDrawArrays,
       OUT(glDrawArrays, mode, first, count),
       IN(glDrawArrays));
}

GL_APICALL void         GL_APIENTRY glDrawElements(GLenum mode, GLsizei count, GLenum type, const GLvoid *indices)
{
    // attrib array - GL_ARRAY_BUFFER
    emulateServerSideArrays(count, 0);

    // index array - GL_ELEMENT_ARRAY_BUFFER
    if (!ctx.bufferBound[ELEMENT_ARRAY_BUF])
    {
        // do magic
        //...
        assert(0);
        indices = NULL;
    }


    FP(void, glDrawElements,
       OUT(glDrawElements, mode, count, type, indices),
       IN(glDrawElements));

}

GL_APICALL void         GL_APIENTRY glEnable(GLenum cap)
{
    FP(void, glEnable,
       OUT(glEnable, cap),
       IN(glEnable));
}

GL_APICALL void         GL_APIENTRY glEnableVertexAttribArray(GLuint index)
{
    ctx.vbos[index].enabled = true;
    FP(void, glEnableVertexAttribArray,
       OUT(glEnableVertexAttribArray, index),
       IN(glEnableVertexAttribArray));
}

GL_APICALL void         GL_APIENTRY glFinish(void)
{
    std::cerr << "Not implemented: 'glFinish'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glFlush(void)
{
    FP(void, glFlush,
       OUT(glFlush),
       IN(glFlush));
}

GL_APICALL void         GL_APIENTRY
glFramebufferRenderbuffer(GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer)
{
    FP(void, glFramebufferRenderbuffer,
       OUT(glFramebufferRenderbuffer, target, attachment, renderbuffertarget, renderbuffer),
       IN(glFramebufferRenderbuffer));
}

GL_APICALL void         GL_APIENTRY
glFramebufferTexture2D(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level)
{
    FP(void, glFramebufferTexture2D,
       OUT(glFramebufferTexture2D, target, attachment, textarget, texture, level),
       IN(glFramebufferTexture2D));
}

GL_APICALL void         GL_APIENTRY glFrontFace(GLenum mode)
{
    std::cerr << "Not implemented: 'glFrontFace'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glGenBuffers(GLsizei n, GLuint *buffers)
{
    FP(void, glGenBuffers,
       OUT(glGenBuffers, n, buffers),
       IN(glGenBuffers, buffers));
}

GL_APICALL void         GL_APIENTRY glGenerateMipmap(GLenum target)
{
    FP(void, glGenerateMipmap,
       OUT(glGenerateMipmap, target),
       IN(glGenerateMipmap));
}

GL_APICALL void         GL_APIENTRY glGenFramebuffers(GLsizei n, GLuint *framebuffers)
{
    FP(void, glGenFramebuffers,
       OUT(glGenFramebuffers, n, framebuffers),
       IN(glGenFramebuffers, framebuffers));
}

GL_APICALL void         GL_APIENTRY glGenRenderbuffers(GLsizei n, GLuint *renderbuffers)
{
    FP(void, glGenRenderbuffers,
       OUT(glGenRenderbuffers, n, renderbuffers),
       IN(glGenRenderbuffers, renderbuffers));
}

GL_APICALL void         GL_APIENTRY glGenTextures(GLsizei n, GLuint *textures)
{
    FP(void, glGenTextures,
       OUT(glGenTextures, n, textures),
       IN(glGenTextures, textures));
}

GL_APICALL void         GL_APIENTRY
glGetActiveAttrib(GLuint program, GLuint index, GLsizei bufsize, GLsizei *length, GLint *size, GLenum *type,
                  GLchar *name)
{
    FP(void, glGetActiveAttrib,
       OUT(glGetActiveAttrib, program, index, bufsize),
       IN(glGetActiveAttrib, length, size, type, name));
}

GL_APICALL void         GL_APIENTRY
glGetActiveUniform(GLuint program, GLuint index, GLsizei bufsize, GLsizei *length, GLint *size, GLenum *type,
                   GLchar *name)
{
    FP(void, glGetActiveUniform,
       OUT(glGetActiveUniform, program, index, bufsize),
       IN(glGetActiveUniform, length, size, type, name));
}

GL_APICALL void         GL_APIENTRY
glGetAttachedShaders(GLuint program, GLsizei maxcount, GLsizei *count, GLuint *shaders)
{
    std::cerr << "Not implemented: 'glGetAttachedShaders'\n";
    assert(0);
}

GL_APICALL int          GL_APIENTRY glGetAttribLocation(GLuint program, const GLchar *name)
{
    return FP(int, glGetAttribLocation,
              OUT(glGetAttribLocation, program, name),
              IN(glGetAttribLocation));
}

GL_APICALL void         GL_APIENTRY glGetBooleanv(GLenum pname, GLboolean *params)
{
    FP(void, glGetBooleanv,
       OUT(glGetBooleanv, pname),
       IN(glGetBooleanv, params));
}

GL_APICALL void         GL_APIENTRY glGetBufferParameteriv(GLenum target, GLenum pname, GLint *params)
{
    std::cerr << "Not implemented: 'glGetBufferParameteriv'\n";
    assert(0);
}

GL_APICALL GLenum       GL_APIENTRY glGetError(void)
{
    return FP(GLenum, glGetError,
              OUT(glGetError),
              IN(glGetError));
}

GL_APICALL void         GL_APIENTRY glGetFloatv(GLenum pname, GLfloat *params)
{
    std::cerr << "Not implemented: 'glGetFloatv'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY
glGetFramebufferAttachmentParameteriv(GLenum target, GLenum attachment, GLenum pname, GLint *params)
{
    std::cerr << "Not implemented: 'glGetFramebufferAttachmentParameteriv'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glGetIntegerv(GLenum pname, GLint *params)
{
    FP(void, glGetIntegerv,
       OUT(glGetIntegerv, pname, params),
       IN(glGetIntegerv, params));
}

GL_APICALL void         GL_APIENTRY glGetProgramiv(GLuint program, GLenum pname, GLint *params)
{
    FP(void, glGetProgramiv,
       OUT(glGetProgramiv, program, pname, params),
       IN(glGetProgramiv, params));
}

GL_APICALL void         GL_APIENTRY
glGetProgramInfoLog(GLuint program, GLsizei bufsize, GLsizei *length, GLchar *infolog)
{
    FP(void, glGetProgramInfoLog,
       OUT(glGetProgramInfoLog, program, bufsize, length, infolog),
       IN(glGetProgramInfoLog,
          length, infolog))
}

GL_APICALL void         GL_APIENTRY glGetRenderbufferParameteriv(GLenum target, GLenum pname, GLint *params)
{
    std::cerr << "Not implemented: 'glGetRenderbufferParameteriv'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glGetShaderiv(GLuint shader, GLenum pname, GLint *params)
{
    FP(void, glGetShaderiv,
       OUT(glGetShaderiv, shader, pname, params),
       IN(glGetShaderiv, params));
}

GL_APICALL void         GL_APIENTRY
glGetShaderInfoLog(GLuint shader, GLsizei bufsize, GLsizei *length, GLchar *infolog)
{
    FP(void, glGetShaderInfoLog,
       OUT(glGetShaderInfoLog, shader, bufsize, length, infolog),
       IN(glGetShaderInfoLog,
          length, infolog));
}

GL_APICALL void         GL_APIENTRY
glGetShaderPrecisionFormat(GLenum shadertype, GLenum precisiontype, GLint *range, GLint *precision)
{
    std::cerr << "Not implemented: 'glGetShaderPrecisionFormat'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glGetShaderSource(GLuint shader, GLsizei bufsize, GLsizei *length, GLchar *source)
{
    std::cerr << "Not implemented: 'glGetShaderSource'\n";
    assert(0);
}

GL_APICALL const GLubyte *GL_APIENTRY glGetString(GLenum name)
{
    return FP(const GLubyte*, glGetString,
              OUT(glGetString, name),
              IN(glGetString, name));
}

GL_APICALL void         GL_APIENTRY glGetTexParameterfv(GLenum target, GLenum pname, GLfloat *params)
{
    std::cerr << "Not implemented: 'glGetTexParameterfv'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glGetTexParameteriv(GLenum target, GLenum pname, GLint *params)
{
    std::cerr << "Not implemented: 'glGetTexParameteriv'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glGetUniformfv(GLuint program, GLint location, GLfloat *params)
{
    std::cerr << "Not implemented: 'glGetUniformfv'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glGetUniformiv(GLuint program, GLint location, GLint *params)
{
    std::cerr << "Not implemented: 'glGetUniformiv'\n";
    assert(0);
}

GL_APICALL int          GL_APIENTRY glGetUniformLocation(GLuint program, const GLchar *name)
{
    return FP(int, glGetUniformLocation,
              OUT(glGetUniformLocation, program, name),
              IN(glGetUniformLocation));
}

GL_APICALL void         GL_APIENTRY glGetVertexAttribfv(GLuint index, GLenum pname, GLfloat *params)
{
    std::cerr << "Not implemented: 'glGetVertexAttribfv'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glGetVertexAttribiv(GLuint index, GLenum pname, GLint *params)
{
    std::cerr << "Not implemented: 'glGetVertexAttribiv'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glGetVertexAttribPointerv(GLuint index, GLenum pname, GLvoid **pointer)
{
    std::cerr << "Not implemented: 'glGetVertexAttribPointerv'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glHint(GLenum target, GLenum mode)
{
    std::cerr << "Not implemented: 'glHint'\n";
    assert(0);
}

GL_APICALL GLboolean    GL_APIENTRY glIsBuffer(GLuint buffer)
{
    assert(0);
    return 0;
}

GL_APICALL GLboolean    GL_APIENTRY glIsEnabled(GLenum cap)
{
    std::cerr << "Not implemented: 'glIsEnabled'\n";
    assert(0);
    return false;
}

GL_APICALL GLboolean    GL_APIENTRY glIsFramebuffer(GLuint framebuffer)
{
    assert(0);
    return 0;
}

GL_APICALL GLboolean    GL_APIENTRY glIsProgram(GLuint program)
{
    assert(0);
    return 0;
}

GL_APICALL GLboolean    GL_APIENTRY glIsRenderbuffer(GLuint renderbuffer)
{
    return FP(GLboolean, glIsRenderbuffer,
              OUT(glIsRenderbuffer, renderbuffer),
              IN(glIsRenderbuffer));
}

GL_APICALL GLboolean    GL_APIENTRY glIsShader(GLuint shader)
{
    return FP(GLboolean, glIsShader,
              OUT(glIsShader, shader),
              IN(glIsShader));
}

GL_APICALL GLboolean    GL_APIENTRY glIsTexture(GLuint texture)
{
    assert(0);
    return 0;
}

GL_APICALL void         GL_APIENTRY glLineWidth(GLfloat width)
{
    FP(void, glLineWidth,
       OUT(glLineWidth, width),
       IN(glLineWidth));
}

GL_APICALL void         GL_APIENTRY glLinkProgram(GLuint program)
{
    FP(void, glLinkProgram,
       OUT(glLinkProgram, program),
       IN(glLinkProgram));
}

GL_APICALL void         GL_APIENTRY glPixelStorei(GLenum pname, GLint param)
{
    FP(void, glPixelStorei,
       OUT(glPixelStorei, pname, param),
       IN(glPixelStorei));
}

GL_APICALL void         GL_APIENTRY glPolygonOffset(GLfloat factor, GLfloat units)
{
    FP(void, glPolygonOffset,
       OUT(glPolygonOffset, factor, units),
       IN(glPolygonOffset));
}

GL_APICALL void         GL_APIENTRY
glReadPixels(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLvoid *pixels)
{
    std::cerr << "Not implemented: 'glReadPixels'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glReleaseShaderCompiler(void)
{
    std::cerr << "Not implemented: 'glReleaseShaderCompiler'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY
glRenderbufferStorage(GLenum target, GLenum internalformat, GLsizei width, GLsizei height)
{
    FP(void, glRenderbufferStorage,
       OUT(glRenderbufferStorage, target, internalformat, width, height),
       IN(glRenderbufferStorage));
}

GL_APICALL void         GL_APIENTRY glSampleCoverage(GLclampf value, GLboolean invert)
{
    std::cerr << "Not implemented: 'glSampleCoverage'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glScissor(GLint x, GLint y, GLsizei width, GLsizei height)
{
    FP(void, glScissor,
       OUT(glScissor, x, y, width, height),
       IN(glScissor));
}

GL_APICALL void         GL_APIENTRY
glShaderBinary(GLsizei n, const GLuint *shaders, GLenum binaryformat, const GLvoid *binary, GLsizei length)
{
    std::cerr << "Not implemented: 'glShaderBinary'\n";
    assert(0);
}

GL_APICALL void  GL_APIENTRY
glShaderSource(GLuint shader, GLsizei count, const GLchar *const *string, const GLint *length)
{
    FP(void, glShaderSource,
       OUT(glShaderSource, shader, count, string, length),
       IN(glShaderSource));
}

GL_APICALL void         GL_APIENTRY glStencilFunc(GLenum func, GLint ref, GLuint mask)
{
    FP(void, glStencilFunc,
       OUT(glStencilFunc, func, ref, mask),
       IN(glStencilFunc));
}

GL_APICALL void         GL_APIENTRY glStencilFuncSeparate(GLenum face, GLenum func, GLint ref, GLuint mask)
{
    std::cerr << "Not implemented: 'glStencilFuncSeparate'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glStencilMask(GLuint mask)
{
    FP(void, glStencilMask,
       OUT(glStencilMask, mask),
       IN(glStencilMask));
}

GL_APICALL void         GL_APIENTRY glStencilMaskSeparate(GLenum face, GLuint mask)
{
    std::cerr << "Not implemented: 'glStencilMaskSeparate'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glStencilOp(GLenum fail, GLenum zfail, GLenum zpass)
{
    FP(void, glStencilOp,
       OUT(glStencilOp, fail, zfail, zpass),
       IN(glStencilOp));
}

GL_APICALL void         GL_APIENTRY glStencilOpSeparate(GLenum face, GLenum fail, GLenum zfail, GLenum zpass)
{
    FP(void, glStencilOpSeparate,
       OUT(glStencilOpSeparate, face, fail, zfail, zpass),
       IN(glStencilOpSeparate));
}

GL_APICALL void         GL_APIENTRY
glTexImage2D(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border,
             GLenum format, GLenum type, const GLvoid *pixels)
{

    FP(void, glTexImage2D,
       OUT(glTexImage2D, target, level, internalformat, width, height, border, format, type, pixels),
       IN(glTexImage2D));
}

GL_APICALL void         GL_APIENTRY glTexParameterf(GLenum target, GLenum pname, GLfloat param)
{
    std::cerr << "Not implemented: 'glTexParameterf'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glTexParameterfv(GLenum target, GLenum pname, const GLfloat *params)
{
    std::cerr << "Not implemented: 'glTexParameterfv'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glTexParameteri(GLenum target, GLenum pname, GLint param)
{
    FP(void, glTexParameteri,
       OUT(glTexParameteri, target, pname, param),
       IN(glTexParameteri));
}

GL_APICALL void         GL_APIENTRY glTexParameteriv(GLenum target, GLenum pname, const GLint *params)
{
    std::cerr << "Not implemented: 'glTexParameteriv'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY
glTexSubImage2D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format,
                GLenum type, const GLvoid *pixels)
{
    FP(void, glTexSubImage2D,
       OUT(glTexSubImage2D, target, level, xoffset, yoffset, width, height, format, type, pixels),
       IN(glTexSubImage2D));
}

GL_APICALL void         GL_APIENTRY glUniform1f(GLint location, GLfloat x)
{
    FP(void, glUniform1f,
       OUT(glUniform1f, location, x),
       IN(glUniform1f));
}

GL_APICALL void         GL_APIENTRY glUniform1fv(GLint location, GLsizei count, const GLfloat *v)
{
    FP(void, glUniform1fv,
       OUT(glUniform1fv, location, count, v),
       IN(glUniform1fv));
}

GL_APICALL void         GL_APIENTRY glUniform1i(GLint location, GLint x)
{
    FP(void, glUniform1i,
       OUT(glUniform1i, location, x),
       IN(glUniform1i));
}

GL_APICALL void         GL_APIENTRY glUniform1iv(GLint location, GLsizei count, const GLint *v)
{
    FP(void, glUniform1iv,
       OUT(glUniform1iv, location, count, v),
       IN(glUniform1iv));
}

GL_APICALL void         GL_APIENTRY glUniform2f(GLint location, GLfloat x, GLfloat y)
{
    std::cerr << "Not implemented: 'glUniform2f'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glUniform2fv(GLint location, GLsizei count, const GLfloat *v)
{
    FP(void, glUniform2fv,
       OUT(glUniform2fv, location, count, v),
       IN(glUniform2fv));
}

GL_APICALL void         GL_APIENTRY glUniform2i(GLint location, GLint x, GLint y)
{
    std::cerr << "Not implemented: 'glUniform2i'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glUniform2iv(GLint location, GLsizei count, const GLint *v)
{
    std::cerr << "Not implemented: 'glUniform2iv'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glUniform3f(GLint location, GLfloat x, GLfloat y, GLfloat z)
{
    std::cerr << "Not implemented: 'glUniform3f'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glUniform3fv(GLint location, GLsizei count, const GLfloat *v)
{
    FP(void, glUniform3fv,
       OUT(glUniform3fv, location, count, v),
       IN(glUniform3fv));
}

GL_APICALL void         GL_APIENTRY glUniform3i(GLint location, GLint x, GLint y, GLint z)
{
    std::cerr << "Not implemented: 'glUniform3i'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glUniform3iv(GLint location, GLsizei count, const GLint *v)
{
    std::cerr << "Not implemented: 'glUniform3iv'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glUniform4f(GLint location, GLfloat x, GLfloat y, GLfloat z, GLfloat w)
{
    std::cerr << "Not implemented: 'glUniform4f'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glUniform4fv(GLint location, GLsizei count, const GLfloat *v)
{
    FP(void, glUniform4fv,
       OUT(glUniform4fv, location, count, v),
       IN(glUniform4fv));
}

GL_APICALL void         GL_APIENTRY glUniform4i(GLint location, GLint x, GLint y, GLint z, GLint w)
{
    std::cerr << "Not implemented: 'glUniform4i'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glUniform4iv(GLint location, GLsizei count, const GLint *v)
{
    std::cerr << "Not implemented: 'glUniform4iv'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY
glUniformMatrix2fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value)
{
    std::cerr << "Not implemented: 'glUniformMatrix2fv'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY
glUniformMatrix3fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value)
{
    FP(void, glUniformMatrix3fv,
       OUT(glUniformMatrix3fv, location, count, transpose, value),
       IN(glUniformMatrix3fv));
}

GL_APICALL void         GL_APIENTRY
glUniformMatrix4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value)
{
    FP(void, glUniformMatrix4fv,
       OUT(glUniformMatrix4fv, location, count, transpose, value),
       IN(glUniformMatrix4fv));
}

GL_APICALL void         GL_APIENTRY glUseProgram(GLuint program)
{
    FP(void, glUseProgram,
       OUT(glUseProgram, program),
       IN(glUseProgram));
}

GL_APICALL void         GL_APIENTRY glValidateProgram(GLuint program)
{
    std::cerr << "Not implemented: 'glValidateProgram'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glVertexAttrib1f(GLuint indx, GLfloat x)
{
    std::cerr << "Not implemented: 'glVertexAttrib1f'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glVertexAttrib1fv(GLuint indx, const GLfloat *values)
{
    std::cerr << "Not implemented: 'glVertexAttrib1fv'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glVertexAttrib2f(GLuint indx, GLfloat x, GLfloat y)
{
    std::cerr << "Not implemented: 'glVertexAttrib2f'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glVertexAttrib2fv(GLuint indx, const GLfloat *values)
{
    std::cerr << "Not implemented: 'glVertexAttrib2fv'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glVertexAttrib3f(GLuint indx, GLfloat x, GLfloat y, GLfloat z)
{
    std::cerr << "Not implemented: 'glVertexAttrib3f'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glVertexAttrib3fv(GLuint indx, const GLfloat *values)
{
    FP(void, glVertexAttrib3fv,
       OUT(glVertexAttrib3fv, indx, values),
       IN(glVertexAttrib3fv));
}

GL_APICALL void         GL_APIENTRY glVertexAttrib4f(GLuint indx, GLfloat x, GLfloat y, GLfloat z, GLfloat w)
{
    std::cerr << "Not implemented: 'glVertexAttrib4f'\n";
    assert(0);
}

GL_APICALL void         GL_APIENTRY glVertexAttrib4fv(GLuint indx, const GLfloat *values)
{
    std::cerr << "Not implemented: 'glVertexAttrib4fv'\n";
    assert(0);
}

GL_APICALL void
GL_APIENTRY
glVertexAttribPointer(GLuint indx, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *ptr)
{
    if (!ctx.bufferBound[ARRAY_BUF])
    {
        ctx.vbos[indx].size       = size;
        ctx.vbos[indx].type       = type;
        ctx.vbos[indx].normalized = normalized;
        ctx.vbos[indx].stride     = stride;
        ctx.vbos[indx].ptr        = ptr;

        DBG_TRAFFIC("EMULAtING glVertexAttribPointer");
        DBG_TRAFFIC("(index=" << indx << ", size=" << size << ", type=" << glesType(type) << ", normalized="
                    << int(normalized) << ", stride=" << stride << ", ptr=" << uint64_t(ptr) << ")");
        DBG_TRAFFIC("\n");

        return;
    }

    ctx.vbos[indx].ptr = 0;

    FP(void, glVertexAttribPointer,
       OUT(glVertexAttribPointer, indx, size, type, normalized, stride, ptr),
       IN(glVertexAttribPointer));
}

GL_APICALL void         GL_APIENTRY glViewport(GLint x, GLint y, GLsizei width, GLsizei height)
{
    FP(void, glViewport,
       OUT(glViewport, x, y, width, height),
       IN(glViewport));
}



/// extension: GL_OES_vertex_array_object
GL_APICALL void GL_APIENTRY glBindVertexArrayOES(GLuint array)
{
    FP(void, glBindVertexArrayOES,
       OUT(glBindVertexArrayOES, array),
       IN(glBindVertexArrayOES));
}

GL_APICALL void GL_APIENTRY glDeleteVertexArraysOES(GLsizei n, const GLuint *arrays)
{
    FP(void, glDeleteVertexArraysOES,
       OUT(glDeleteVertexArraysOES, n, arrays),
       IN(glDeleteVertexArraysOES));
}

GL_APICALL void GL_APIENTRY glGenVertexArraysOES(GLsizei n, GLuint *arrays)
{
    FP(void, glGenVertexArraysOES,
       OUT(glGenVertexArraysOES, n, arrays),
       IN(glGenVertexArraysOES, arrays));
}

GL_APICALL GLboolean GL_APIENTRY glIsVertexArrayOES(GLuint array)
{
    return FP(GLboolean, glIsVertexArrayOES,
              OUT(glIsVertexArrayOES, array),
              IN(glIsVertexArrayOES));
}


#else


void setMouse(int16_t x, int16_t y, uint8_t buttons)
{
    FP(void, setMouse,
       OUT(setMouse, x, y, buttons),
       IN(setMouse));
}


void setNewSize(int16_t width, int16_t height)
{
    FP(void, setNewSize,
       OUT(setNewSize, width, height),
       IN(setNewSize));
}


#endif
