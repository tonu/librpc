#ifndef CMD_GLUNIFORM1I_HPP
#define CMD_GLUNIFORM1I_HPP
//typedef void          (*glUniform1i_ptr) (GLint location, GLint x);


#include "cmd_includes.hpp"

namespace cmd_glUniform1i
{
static void client_out(FIFO &fifo, GLint location, GLint x)
{
    fifo << arg<GLint>(location);
    fifo << arg<GLint>(x);
}

static void client_in(FIFO &fifo)
{

}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
{
     arg<GLint> location;
     arg<GLint> x;

    fifoIn >> location >> x;

#ifdef BUILD_SERVER
    glUniform1i(location.val, x.val);
#endif
}

static bool returns() { return false; }
}
#endif // CMD_GLUNIFORM1I_HPP
