//
// Created by Tonu Jaansoo on 20/08/16.
//

#ifndef BUILD_ALL_CMD_GLGETACTIVEUNIFORM_HPP
#define BUILD_ALL_CMD_GLGETACTIVEUNIFORM_HPP

//GL_APICALL void GL_APIENTRY glGetActiveUniform (GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name);

#include "cmd_includes.hpp"


namespace cmd_glGetActiveUniform
{
    static void
    client_out(FIFO &fifo, GLuint program, GLuint index, GLsizei bufSize)
    {
        DBG_TRAFFIC("(program=" << program << ", index=" << index << ", bufsize=" << bufSize << ", length=..., size=..., type=..., name=...)");

        fifo << arg<GLuint>(program);
        fifo << arg<GLuint>(index);
        fifo << arg<GLsizei>(bufSize);
    }

    static void client_in(FIFO &fifo, GLsizei *length, GLint *size, GLenum *type, GLchar *name)
    {
        arg<GLsizei>        ln;
        arg<GLint>          sz;
        arg<GLenum>         ty;
        arg<buffer<char>>    na;

        fifo >> ln >> sz >> ty >> na;

        *length = ln.val;
        *size = sz.val;
        *type = ty.val;

        memcpy(name, na.val._ptr.get(), na.val._sz * sizeof (char));

        DBG_TRAFFIC(" returned: length=" << ln.val << ", size=" << sz.val << ", type=" << glesType(ty.val) << ", name='" << na.val._ptr.get() << "')");
    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        // in
        arg<GLuint>  program;
        arg<GLuint>  index;
        arg<GLsizei> bufSize;

        fifoIn >> program;
        fifoIn >> index;
        fifoIn >> bufSize;

        // out
        arg<GLsizei> length;
        arg<GLint>   size;
        arg<GLenum>  type;
        arg<buffer<char>>    name(bufSize.val + 1);// reserve for \0

#ifdef BUILD_SERVER
        glGetActiveUniform(program.val, index.val, bufSize.val, &length.val, &size.val, &type.val, name.val._ptr.get());
#endif

        //dont send all data specified in bufsize.. only strlen
        name.val.resize(length.val + 1);
        name.val._ptr.get()[length.val] = 0;

        fifoOut << length << size << type << name;
    }

    static bool returns() { return true; }
}

#endif //BUILD_ALL_CMD_GLGETACTIVEUNIFORM_HPP
