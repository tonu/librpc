//
// Created by Tonu Jaansoo on 19/08/16.
//

#ifndef BUILD_ALL_CMD_GLBLENDCOLOR_HPP
#define BUILD_ALL_CMD_GLBLENDCOLOR_HPP
//    GL_APICALL void GL_APIENTRY glBlendColor (GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha);



#include "cmd_includes.hpp"

namespace cmd_glBlendColor
{
    static void client_out(FIFO &fifo, GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha)
    {
        DBG_TRAFFIC("(red=" << red << ", green=" << green << ", blue=" << blue << ", alpha=" << alpha << ")");

        fifo << arg<GLfloat>(red);
        fifo << arg<GLfloat>(green);
        fifo << arg<GLfloat>(blue);
        fifo << arg<GLfloat>(alpha);
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
    {

        arg<GLfloat> red;
        arg<GLfloat> green;
        arg<GLfloat> blue;
        arg<GLfloat> alpha;

        fifoIn >> red >> green >> blue >> alpha;

#ifdef BUILD_SERVER
        glBlendColor(red.val, green.val, blue.val, alpha.val);
#endif
    }

    static bool returns() { return false; }
}



#endif //BUILD_ALL_CMD_GLBLENDCOLOR_HPP
