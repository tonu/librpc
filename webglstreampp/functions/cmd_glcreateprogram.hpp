#ifndef CMD_GLCREATEPROGRAM_H
#define CMD_GLCREATEPROGRAM_H

//typedef GLuint        (*glCreateProgram_ptr) (void);

#include "cmd_includes.hpp"

namespace cmd_glCreateProgram
{
    static void client_out(FIFO &)
    {
        DBG_TRAFFIC("()");
    }

    static GLuint client_in(FIFO &fifo)
    {
        arg<uint32_t> b;
        fifo >> b;

        DBG_TRAFFIC(" return=" << b.val);

        return b.val;
    }

// what to execute on server
    static void serv_in(Server *const, FIFO &, FIFO &fifoOut)
    {

#ifdef BUILD_SERVER
        GLuint p = glCreateProgram();
#else
        GLuint p = 7654;
#endif
        fifoOut << arg<uint32_t>(p);
    }

    static bool returns() { return true; }

}


#endif // CMD_GLCREATEPROGRAM_H
