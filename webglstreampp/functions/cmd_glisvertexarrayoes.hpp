#ifndef CMD_GLISVERTEXARRAYOES_HPP
#define CMD_GLISVERTEXARRAYOES_HPP
//    GL_APICALL GLboolean GL_APIENTRY glIsVertexArrayOES (GLuint array);

#include "cmd_includes.hpp"

namespace cmd_glIsVertexArrayOES
{
static void client_out(FIFO &fifo, GLuint array)
{
    fifo << arg<GLuint>(array);
}

static GLboolean client_in(FIFO &fifo)
{
    arg<GLboolean> p;

    fifo >> p;

    return p.val;
}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
{
    arg<GLuint> array;

    fifoIn >> array;

#ifdef BUILD_SERVER
    GLboolean ret =  glIsVertexArrayOES(array.val);
#else
    GLboolean ret = 0;
#endif
    fifoOut << arg<GLboolean>(ret);
}

static bool returns() { return true; }
}

#endif // CMD_GLISVERTEXARRAYOES_HPP
