//
// Created by Tonu Jaansoo on 17/08/16.
//

#ifndef BUILD_ALL_GLES_ENUMS_HPP
#define BUILD_ALL_GLES_ENUMS_HPP

#include "gles_defs.hpp"

#undef CMD
#define CMD(n) n##_enum,

typedef enum
{
    GLCommand_Begin,
    DEFS

    GLCommand_MAX
} GLCommand_t;


#endif //BUILD_ALL_GLES_ENUMS_HPP
