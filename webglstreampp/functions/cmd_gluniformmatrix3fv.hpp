#ifndef CMD_GLUNIFORMMATRIX3FV_HPP
#define CMD_GLUNIFORMMATRIX3FV_HPP
// typedef void          (*glUniformMatrix3fv_ptr) (GLint location, GLsizei count, GLboolean transpose, const GLfloat* value);


#include "cmd_includes.hpp"

namespace cmd_glUniformMatrix3fv
{
    static void client_out(FIFO &fifo, GLint location, GLsizei count, GLboolean transpose, const GLfloat* value)
    {
        DBG_TRAFFIC("(location=" << location << ", count=" << count << ", transpose=" << int(transpose) << ", v=...)");

        fifo << arg<GLint>(location);
        fifo << arg<GLsizei>(count);
        fifo << arg<GLboolean>(transpose);
        fifo << arg<buffer<float>>( buffer<float>(value, count * 9)); // count * 3x3
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLint> location;
        arg<GLsizei> count;
        arg<GLboolean> transpose;
        arg<buffer<float>> value;

#ifdef BUILD_SERVER
        glUniformMatrix3fv(location.val, count.val, transpose.val, value.val._ptr.get());
#endif
    }

    static bool returns() { return false; }
}

#endif // CMD_GLUNIFORMMATRIX3FV_HPP
