#ifndef CMD_GLGETSTRING_HPP
#define CMD_GLGETSTRING_HPP

#define MAX_SIZE_GLGETSTRING 1024

#include "cmd_includes.hpp"

// for this command we need static
static char returnVal[MAX_SIZE_GLGETSTRING] = {0};

namespace cmd_glGetString
{
    static void client_out(FIFO &fifo, GLenum name)
    {
        DBG_TRAFFIC("(name="  << glesType(name) << ")");

        fifo << arg<uint32_t>(name);
    }

    static const GLubyte *client_in(FIFO &fifo, GLenum name)
    {
        arg<buffer<char>> b;
        fifo >> b;

        std::string tmp(b.val._ptr.get(), b.val._sz - 1); //remove NULL

        if (name == GL_RENDERER)
            tmp.append(std::string(" LibWebGL++"));

        memcpy(&returnVal[0], tmp.c_str(), tmp.size()+1);
        returnVal[tmp.size()] = 0;

        DBG_TRAFFIC(" return='" << returnVal << "'");

        return (const GLubyte *) &returnVal[0];// point buf to returnval
    }

// what to execute on server
    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<uint32_t> name;
        fifoIn >> name;

#ifdef BUILD_SERVER
        const GLubyte* p = glGetString(name.val);
#else
        char p[] = "";
#endif

        //test
        fifoOut << arg<buffer<char>>(buffer<char>((void *) p, strlen((const char *) p) + 1));
    }

    static bool returns() { return true; }
}

#endif // CMD_GLGETSTRING_HPP
