#ifndef CMD_GLVERTEXATTRIBPOINTER_HPP
#define CMD_GLVERTEXATTRIBPOINTER_HPP
//typedef void          (*glVertexAttribPointer_ptr) (GLuint indx, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* ptr);

#include "cmd_includes.hpp"

namespace cmd_glVertexAttribPointer
{
static void client_out(FIFO &fifo, GLuint indx, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* ptr)
{
    DBG_TRAFFIC("(index=" << indx << ", size=" << size << ", type=" << glesType(type) << ", normalized=" << int(normalized)  << ", stride=" << stride << ", ptr=" << uint64_t(ptr) << ")");

    fifo << arg<GLuint>(indx);
    fifo << arg<GLint>(size);
    fifo << arg<GLenum>(type);
    fifo << arg<uint8_t>(normalized);
    fifo << arg<GLsizei>(stride);

    bool isGPU = (uint64_t(ptr) < 0x10000);
    assert(isGPU);

    uint16_t offset = (uint16_t)(uint64_t(ptr) & 0xffff);
    fifo << arg<uint16_t>(offset);
}

static void client_in(FIFO &)
{

}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
{
    arg<GLuint>   indx;
    arg<GLint>    size;
    arg<GLenum>   type;
    arg<uint8_t>  normalized;
    arg<GLsizei>  stride;
    arg<uint16_t> offset;

    fifoIn >> indx >> size >> type >> normalized >> stride >> offset;

#ifdef BUILD_SERVER
    glVertexAttribPointer(indx.val, size.val, type.val, normalized.val, stride.val, (const void *)(GLsizeiptr)(offset.val));
#endif
}

static bool returns() { return false; }
}
#endif // CMD_GLVERTEXATTRIBPOINTER_HPP
