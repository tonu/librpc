#ifndef CMD_INCLUDES_H
#define CMD_INCLUDES_H

#include <stdint.h>


#define GL_GLEXT_PROTOTYPES
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#undef GL_GLEXT_PROTOTYPES


#include "../arg.hpp"
#include "../fifo.hpp"
#include "gles_names.hpp"
#include <webglstreampp/Util.hpp>

class Server;

#endif // CMD_INCLUDES_H
