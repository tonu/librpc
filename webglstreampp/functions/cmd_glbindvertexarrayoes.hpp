#ifndef CMD_GLBINDVERTEXARRAYOES_HPP
#define CMD_GLBINDVERTEXARRAYOES_HPP
//    GL_APICALL void GL_APIENTRY glBindVertexArrayOES (GLuint array);


#include "cmd_includes.hpp"

namespace cmd_glBindVertexArrayOES
{
    static void client_out(FIFO &fifo, GLuint array)
    {
        fifo << arg<GLuint>(array);
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
    {
        arg<GLuint> array;

        fifoIn >> array;

#ifdef BUILD_SERVER
        glBindVertexArrayOES(array.val);
#endif
    }

    static bool returns() { return false; }
}

#endif // CMD_GLBINDVERTEXARRAYOES_HPP
