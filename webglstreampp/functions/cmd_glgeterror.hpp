#ifndef CMD_GLGETERROR_HPP
#define CMD_GLGETERROR_HPP
//typedef GLenum        (*glGetError_ptr) (void);

#include "cmd_includes.hpp"

namespace cmd_glGetError
{
static void client_out(FIFO &fifo)
{

}

static GLenum client_in(FIFO &fifo)
{
    arg<GLenum> ret;
    fifo >> ret;

    return ret.val;
}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
{
#ifdef BUILD_SERVER
    GLenum ret = glGetError();
#else
    GLenum ret = 0;
#endif

    fifoOut << arg<GLenum>(ret);
}

static bool returns() { return true; }
}

#endif // CMD_GLGETERROR_HPP
