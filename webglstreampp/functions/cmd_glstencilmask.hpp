#ifndef CMD_GLSTENCILMASK_HPP
#define CMD_GLSTENCILMASK_HPP
//typedef void          (*glStencilMask_ptr) (GLuint mask);

#include "cmd_includes.hpp"

namespace cmd_glStencilMask
{
    static void client_out(FIFO &fifo, GLuint mask)
    {
        fifo << arg<GLuint>(mask);
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLuint> mask;

        fifoIn >> mask;

#ifdef BUILD_SERVER
        glStencilMask(mask.val);
#endif
    }

    static bool returns() { return false; }
}

#endif // CMD_GLSTENCILMASK_HPP
