#ifndef CMD_GLGETSHADERIV_HPP
#define CMD_GLGETSHADERIV_HPP

//typedef void          (*glGetShaderiv_ptr) (GLuint shader, GLenum pname, GLint* params);

#include "cmd_includes.hpp"

namespace cmd_glGetShaderiv
{
    static void client_out(FIFO &fifo, GLuint shader, GLenum pname, GLint* params)
    {

        if (!glesHasType(pname))
        {
            DBG_TRAFFIC("(wont do: unknown enum:" << pname << ")");
            return;
        }

        DBG_TRAFFIC("(shader=" << shader << ", pname=" << glesType(pname) << ", params=...)");

        fifo << arg<GLuint>(shader);
        fifo << arg<GLenum>(pname);
    }

    static void client_in(FIFO &fifo, GLint* params)
    {
        arg<buffer<GLint>> pars;

        fifo >> pars;

        DBG_TRAFFIC(" params[0]=" << pars.val._ptr.get()[0]);

        memcpy(params, pars.val._ptr.get(), pars.val._sz * sizeof(GLint));
    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLuint> shader;
        arg<GLenum> pname;

        fifoIn >> shader >> pname;

        arg<buffer<GLint>> params(1);
#ifdef BUILD_SERVER
        glGetShaderiv(shader.val, pname.val, params.val._ptr.get());
#endif

        fifoOut << params;
    }

    static bool returns() { return true; }
}

#endif // CMD_GLGETSHADERIV_HPP
