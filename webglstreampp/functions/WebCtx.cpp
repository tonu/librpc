//
// Created by Tonu Jaansoo on 29/08/16.
//

#include "WebCtx.hpp"

std::shared_ptr<WebCtx> WebCtx::wCtx  = NULL;

static const GLfloat squareVertices[] = {
        -1.0f, -1.0f,
        1.0f, -1.0f,
        -1.0f, 1.0f,
        1.0f, 1.0f,
};

static const GLfloat textureVertices[] = {
        1.0f, 1.0f,
        1.0f, 0.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,
};


const char vtxShader[] =
"attribute vec3 vertex;\n"
"attribute vec2 uv;\n"
"varying vec2 uv_frag;\n"
"void main(){\n"
"    uv_frag = uv;\n"
"    gl_Position = vec4(vertex, 1.0);\n"
"}\n";

const char fragShader[] =
"precision mediump float;\n"
"varying vec2 uv_frag;\n"
"uniform sampler2D tex;\n"
"void main(){\n"
"    gl_FragColor = texture2D(tex, uv_frag);\n"
"}\n";


WebCtx::WebCtx()
        :
        rttTexture(0),
        renderbuffer(0),
        quad(),
        prog(vtxShader, fragShader)
{
    int width  = 1;
    int height = 1;

    glGenFramebuffers(1, &rttFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, rttFBO);

    glGenTextures(1, &rttTexture);
    glBindTexture(GL_TEXTURE_2D, rttTexture);

    // note: https://www.khronos.org/webgl/wiki/WebGL_and_OpenGL_Differences#Non-Power_of_Two_Texture_Support
    // fixme use mipmaps and POT textures
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//    glGenerateMipmap(GL_TEXTURE_2D);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);

    glGenRenderbuffers(1, &renderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, renderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_STENCIL_OES, width, height);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, rttTexture, 0);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, renderbuffer);

    glBindTexture(GL_TEXTURE_2D, 0);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void WebCtx::resizeFBO(int width, int height)
{
    GLint prevFBO;
    GLint prevTexBound;
    GLint prevRenderbuffer;

    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &prevFBO);
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &prevTexBound);
    glGetIntegerv(GL_RENDERBUFFER_BINDING, &prevRenderbuffer);

    {
        glBindFramebuffer(GL_FRAMEBUFFER, rttFBO);
        glBindTexture(GL_TEXTURE_2D, rttTexture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);

        glBindRenderbuffer(GL_RENDERBUFFER, renderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_STENCIL_OES, width, height);
    }

    glBindFramebuffer(GL_FRAMEBUFFER, prevFBO);
    glBindTexture(GL_TEXTURE_2D, prevTexBound);
    glBindRenderbuffer(GL_RENDERBUFFER, prevRenderbuffer);
}

void WebCtx::draw()
{
    GLint prevVAO;
    GLint prevActiveTexture;
    GLint prevTexBound;
    GLint prevProgram;
    GLint prevFBO;

    // store current state
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &prevFBO);
    glGetIntegerv(0x8894, &prevVAO);
    glGetIntegerv(GL_ACTIVE_TEXTURE, &prevActiveTexture);
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &prevTexBound);
    glGetIntegerv(GL_CURRENT_PROGRAM, &prevProgram);

    if (prevFBO)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    // draw stuff
    {
        glBindVertexArrayOES(quad.vao);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, rttTexture);

        glUseProgram(prog.program);
        glUniform1i(glGetUniformLocation(prog.program, "tex"), 0);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    }

    //restore previous state
    glUseProgram(0);
    glBindVertexArrayOES(prevVAO);
    glBindTexture(GL_TEXTURE_2D, 0);
    glActiveTexture(prevActiveTexture);
    glBindFramebuffer(GL_FRAMEBUFFER, prevFBO);
}