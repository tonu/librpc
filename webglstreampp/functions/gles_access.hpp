//
// Created by Tonu Jaansoo on 17/08/16.
//

#ifndef BUILD_ALL_GLES_ACCESS_HPP
#define BUILD_ALL_GLES_ACCESS_HPP

#include "../fifo.hpp"
#include "gles_enums.hpp"
#include "gles_includes.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////////////

#undef CMD
#define CMD(n)   case n##_enum: cmd_##n::serv_in(s,f,o); break;

static void serv_in(Server *const s, GLCommand_t cmd, FIFO &f, FIFO &o)
{
    switch (cmd)
    {
        DEFS;
    default:
        std::stringstream ss;
        ss << "serv in : no such cmd: " << cmd;
        throw ss.str();
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
#undef CMD
#define CMD(n)   case n##_enum: return cmd_##n::returns();

static bool returns(GLCommand_t cmd)
{
    switch (cmd)
    {
        DEFS;
    default:

        std::stringstream ss;
        ss << "returns : no such cmd: " << cmd;
        throw ss.str();
    }

    return false;
}




#endif //BUILD_ALL_GLES_ACCESS_HPP
