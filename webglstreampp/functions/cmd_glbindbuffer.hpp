#ifndef CMD_GLBINDBUFFER_HPP
#define CMD_GLBINDBUFFER_HPP
//typedef void          (*glBindBuffer_ptr) (GLenum target, GLuint buffer);

#include "cmd_includes.hpp"

//vim quick/scenegraph/scenegraph.pri
//uncomment:
//DEFINES += QSG_SEPARATE_INDEX_BUFFER
//https://git.merproject.org/mer-core/qtdeclarative/commit/662046fe17c76d4fe505f0a04a66aa3477c14b32
//../qt-everywhere-opensource-src-5.7.0/configure -nomake tests -nomake examples -DQSG_SEPARATE_INDEX_BUFFER -prefix /home/chain/Qt
namespace cmd_glBindBuffer
{
static void client_out(FIFO &fifo, GLenum target, GLuint buffer)
{
    fifo << arg<GLenum>(target) << arg<GLuint>(buffer);

    DBG_TRAFFIC("(target=" << glesType(target) << ", buffer=" << buffer << ")");
}

static void client_in(FIFO &fifo)
{

}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
{
    arg<GLenum> target;
    arg<GLuint> buffer;

    fifoIn >> target >> buffer;

#ifdef BUILD_SERVER
    glBindBuffer(target.val, buffer.val);
#endif
}

static bool returns() { return false; }
}

#endif // CMD_GLBINDBUFFER_HPP
