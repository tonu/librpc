#ifndef CMD_GLDRAWARRAYS_HPP
#define CMD_GLDRAWARRAYS_HPP

//typedef void          (*glDrawArrays_ptr) (GLenum mode, GLint first, GLsizei count);

#include "cmd_includes.hpp"

namespace cmd_glDrawArrays
{
static void client_out(FIFO &fifo, GLenum mode, GLint first, GLsizei count)
{
    DBG_TRAFFIC("(mode=" << glesType(mode) << ", first=" << first << ", count=" << count << ")");

    fifo << arg<GLenum>(mode) << arg<GLint>(first) << arg<GLsizei>(count);

//    if (count > 1000)
//    {
//        std::cerr  <<" (warning.. count too big)\n";
//    }
}

static void client_in(FIFO &fifo)
{

}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
{
    arg<GLenum> mode;
    arg<GLint> first;
    arg<GLsizei> count;

    fifoIn >> mode >> first >> count;

#ifdef BUILD_SERVER
    //        glClearColor(0,count.val %2,0,0);
    //        glClear(GL_COLOR_BUFFER_BIT);
    //        glDisable(GL_DEPTH_TEST);
    //        glDisable(GL_STENCIL_TEST);

    //glDisable(GL_BLEND);
    //glDisable(GL_DEPTH_TEST);
    //glDepthMask(GL_TRUE);
    //glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
    //glDepthFunc(GL_ALWAYS);
    // glDisable(GL_STENCIL_TEST);


    glDrawArrays(mode.val, first.val, count.val);
#endif
}

static bool returns() { return false; }
}
#endif // CMD_GLDRAWARRAYS_HPP
