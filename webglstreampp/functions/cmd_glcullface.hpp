//
// Created by Tonu Jaansoo on 20/08/16.
//

#ifndef BUILD_ALL_CMD_GLCULLFACE_HPP
#define BUILD_ALL_CMD_GLCULLFACE_HPP
//     GL_APICALL void GL_APIENTRY glCullFace (GLenum mode);
#include "cmd_includes.hpp"

namespace cmd_glCullFace
{
    static void client_out(FIFO &fifo, GLenum mode)
    {
        DBG_TRAFFIC("(mode=" << glesType(mode) << ")");

        fifo << arg<GLenum>(mode);
    }

    static void client_in(FIFO &)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
    {
        arg<GLenum> mode;

        fifoIn >> mode;

#ifdef BUILD_SERVER
        glCullFace(mode.val);
#endif
    }

    static bool returns() { return false; }

}

#endif //BUILD_ALL_CMD_GLCULLFACE_HPP
