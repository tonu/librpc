#ifndef CMD_GLSCISSOR_HPP
#define CMD_GLSCISSOR_HPP
//typedef void          (*glScissor_ptr) (GLint x, GLint y, GLsizei width, GLsizei height);

#include "cmd_includes.hpp"

namespace cmd_glScissor
{
    static void client_out(FIFO &fifo, GLint x, GLint y, GLsizei width, GLsizei height)
    {
fifo << arg<GLint>(x);
fifo << arg<GLint>(y);
fifo << arg<GLsizei>(width);
fifo << arg<GLsizei>(height);
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
    {
        arg<GLint> x;
        arg<GLint> y;
        arg<GLsizei> width;
        arg<GLsizei> height;

        fifoIn >> x >> y >> width >> height;

#ifdef BUILD_SERVER
        glScissor(x.val, y.val, width.val, height.val);
#endif
    }

    static bool returns() { return false; }
}

#endif // CMD_GLSCISSOR_HPP
