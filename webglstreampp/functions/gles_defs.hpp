//
// Created by Tonu Jaansoo on 17/08/16.
//

#ifndef BUILD_ALL_GLES_DEFS_HPP_HPP
#define BUILD_ALL_GLES_DEFS_HPP_HPP

#define  DEFS   \
/* 1 */        CMD(glActiveTexture) \
        CMD(glAttachShader) \
        CMD(glBindAttribLocation) \
        CMD(glBindBuffer) \
        CMD(glBufferData) \
/* 6 */        CMD(glClear) \
        CMD(glClearColor) \
        CMD(glCompileShader) \
        CMD(glCreateProgram) \
        CMD(glCreateShader) \
/* 11 */        CMD(glDeleteProgram) \
        CMD(glDeleteShader) \
        CMD(glDrawArrays) \
        CMD(glEnableVertexAttribArray) \
        CMD(glGenBuffers) \
/* 16 */        CMD(glGetError) \
        CMD(glGetProgramiv) \
        CMD(glGetProgramInfoLog) \
        CMD(glGetShaderiv) \
        CMD(glGetShaderInfoLog) \
/* 21 */       CMD(glGetString) \
        CMD(glLinkProgram) \
        CMD(glShaderSource) \
        CMD(glUseProgram) \
        CMD(glVertexAttribPointer) \
        CMD(glViewport) \
        CMD(glBindFramebuffer) \
        CMD(glBindRenderbuffer) \
        CMD(glGenFramebuffers) \
        CMD(glGenRenderbuffers) \
        CMD(glBindTexture) \
        CMD(glBlendFunc) \
        CMD(glClearDepthf) \
        CMD(glClearStencil) \
        CMD(glColorMask) \
        CMD(glCopyTexImage2D) \
        CMD(glCopyTexSubImage2D) \
        CMD(glDisable) \
        CMD(glDisableVertexAttribArray) \
        CMD(glStencilFunc) \
        CMD(glVertexAttrib3fv) \
        CMD(glDepthFunc) \
        CMD(glDepthMask) \
        CMD(glEnable) \
        CMD(glUniform4fv) \
        CMD(glUniform3fv) \
        CMD(glUniform2fv) \
        CMD(glTexParameteri) \
        CMD(glGenTextures) \
        CMD(glTexImage2D) \
        CMD(glStencilMask) \
        CMD(glStencilOp) \
        CMD(glScissor) \
        CMD(glGetIntegerv) \
        CMD(glUniform1i) \
        CMD(glUniformMatrix3fv) \
        CMD(glGenVertexArraysOES) \
        CMD(glDeleteVertexArraysOES) \
        CMD(glBindVertexArrayOES) \
        CMD(glIsVertexArrayOES) \
        CMD(glTexSubImage2D) \
        CMD(glDrawElements) \
        CMD(glFramebufferTexture2D) \
        CMD(glIsRenderbuffer) \
        CMD(glRenderbufferStorage)\
        CMD(glFramebufferRenderbuffer) \
        CMD(glUniformMatrix4fv) \
        CMD(glUniform1fv) \
        CMD(glFlush) \
        CMD(glBlendColor) \
        CMD(glGetUniformLocation) \
        CMD(glCheckFramebufferStatus) \
        CMD(glLineWidth) \
        CMD(glDeleteTextures) \
        CMD(glDeleteBuffers) \
        CMD(glDeleteFramebuffers) \
        CMD(glDeleteRenderbuffers) \
        CMD(glCullFace) \
        CMD(glGetActiveUniform) \
        CMD(glGetActiveAttrib) \
        CMD(glGetAttribLocation) \
        CMD(glPixelStorei) \
        CMD(glUniform1iv) \
        CMD(glGetBooleanv) \
        CMD(glGenerateMipmap) \
        CMD(glIsShader) \
        CMD(glPolygonOffset) \
        CMD(glUniform1f) \
        CMD(glStencilOpSeparate) \
\
\
        CMD(setMouse) \
        CMD(setNewSize) \

#endif //BUILD_ALL_GLES_DEFS_HPP_HPP
