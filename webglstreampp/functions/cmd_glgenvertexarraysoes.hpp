#ifndef GLGENVERTEXARRAYSOES_HPP
#define GLGENVERTEXARRAYSOES_HPP

//    GL_APICALL void GL_APIENTRY glGenVertexArraysOES (GLsizei n, GLuint *arrays);

#include "cmd_includes.hpp"

namespace cmd_glGenVertexArraysOES
{
    static void client_out(FIFO &fifo, GLsizei n, GLuint *arrays)
    {
        fifo << arg<GLsizei>(n);
        fifo << arg<buffer<GLuint>>(buffer<GLuint>(arrays, n));
    }

    static void client_in(FIFO &fifo, GLuint *arrays)
    {
        arg<buffer<GLuint>> a;

        fifo >> a;

        memcpy(arrays, a.val._ptr.get(), a.val._sz * sizeof(GLuint));
    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLsizei> n;
        arg<buffer<GLuint>> arrays;

        fifoIn >> n >> arrays;

#ifdef BUILD_SERVER
        glGenVertexArraysOES(n.val, arrays.val._ptr.get());
#endif

        fifoOut << arrays;
    }

    static bool returns() { return true; }
}

#endif // GLGENVERTEXARRAYSOES_HPP
