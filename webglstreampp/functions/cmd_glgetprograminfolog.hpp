#ifndef GLGETPROGRAMINFOLOG_HPP
#define GLGETPROGRAMINFOLOG_HPP

//typedef void          (*glGetProgramInfoLog_ptr) (GLuint program, GLsizei bufsize, GLsizei* length, GLchar* infolog);

#include "cmd_includes.hpp"

namespace cmd_glGetProgramInfoLog
{
static void client_out(FIFO & fifo,
                       GLuint program, GLsizei bufsize, GLsizei* , GLchar* )
{
    fifo << arg<uint32_t>(program);
    fifo << arg<int32_t>(bufsize); //max size we can take
    fifo << arg<buffer<int32_t>>(buffer<int32_t>(1)); //returns actual
    fifo << arg<buffer<char>>(buffer<char>(bufsize));
}

static void client_in(FIFO & fifo, GLsizei* length, GLchar* infolog)
{
    arg<buffer<int32_t>> l;
    arg<buffer<char>> i;

    fifo >> l >> i;

    *length = *l.val._ptr.get();
    memcpy(infolog, i.val._ptr.get(), *length);


    if (i.val._sz)
        DBG_TRAFFIC(" cmd_glGetProgramInfoLog: \n" << std::string(infolog, *length) << "\n-----\n");// convert to string .. infolog is not null terminated
}

// what to execute on server
static void serv_in(Server *const, FIFO & fifoIn, FIFO & fifoOut)
{
    arg<uint32_t> program;
    arg<int32_t> bufsize;
    arg<buffer<int32_t>> length;
    arg<buffer<char>> infolog;

    // read params
    fifoIn >> program >> bufsize >> length >> infolog;

#ifdef BUILD_SERVER
    glGetProgramInfoLog(program.val, bufsize.val, length.val._ptr.get(), infolog.val._ptr.get());// fixme
#endif

    //        send back
    fifoOut << length << infolog;
}

static bool returns() { return true; }
}

#endif // GLGETPROGRAMINFOLOG_HPP
