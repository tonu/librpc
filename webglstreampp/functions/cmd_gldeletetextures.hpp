//
// Created by Tonu Jaansoo on 19/08/16.
//

#ifndef BUILD_ALL_CMD_GLDELETETEXTURES_HPP
#define BUILD_ALL_CMD_GLDELETETEXTURES_HPP

//GL_APICALL void GL_APIENTRY glDeleteTextures (GLsizei n, const GLuint *textures);

#include "cmd_includes.hpp"

namespace cmd_glDeleteTextures
{
    static void client_out(FIFO &fifo, GLsizei n, const GLuint *textures)
{
    DBG_TRAFFIC("(n=" << n << " textures=...)");

    fifo << arg<GLsizei>(n);
    fifo << arg<buffer<GLuint>>(buffer<GLuint>(textures, n));
}

static void client_in(FIFO &fifo)
{

}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
{
    arg<GLsizei> n;
    arg<buffer<GLuint>> textures;

    fifoIn >> n >> textures;

#ifdef BUILD_SERVER
    glDeleteTextures(n.val, textures.val._ptr.get());
#endif
}

static bool returns() { return false; }
}




#endif //BUILD_ALL_CMD_GLDELETETEXTURES_HPP
