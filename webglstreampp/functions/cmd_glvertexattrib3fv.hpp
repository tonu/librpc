#ifndef CMD_GLVERTEXATTRIB3FV_HPP
#define CMD_GLVERTEXATTRIB3FV_HPP
//typedef void          (*glVertexAttrib3fv_ptr) (GLuint indx, const GLfloat* values);

#include "cmd_includes.hpp"

namespace cmd_glVertexAttrib3fv
{
static void client_out(FIFO &fifo, GLuint indx, const GLfloat* values)
{
    DBG_TRAFFIC("(indx="<< indx << ", values=...)");

    assert(indx < GL_MAX_VERTEX_ATTRIBS);

    fifo << arg<GLuint>(indx);
    fifo << arg<buffer<float>>( buffer<float> (values, 3)); //FIXME... is this correct ??
}

static void client_in(FIFO &fifo)
{

}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
{
    arg<GLuint> indx;
    arg<buffer<float>> values;

    fifoIn >> indx >> values;

#ifdef BUILD_SERVER
    glVertexAttrib3fv(indx.val, values.val._ptr.get());
#endif
}

static bool returns() { return false; }
}
#endif // CMD_GLVERTEXATTRIB3FV_HPP
