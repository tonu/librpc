#ifndef CMD_GLDEPTHMASK_HPP
#define CMD_GLDEPTHMASK_HPP
//typedef void          (*glDepthMask_ptr) (GLboolean flag);

#include "cmd_includes.hpp"

namespace cmd_glDepthMask
{
static void client_out(FIFO &fifo, GLboolean flag)
{
    DBG_TRAFFIC("(flag=" << int(flag) << ")");

    fifo << arg<GLboolean>(flag);
}

static void client_in(FIFO &fifo)
{

}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
{
    arg<GLboolean> flag;
    fifoIn >> flag;

#ifdef BUILD_SERVER
    glDepthMask(flag.val);
#endif
}

static bool returns() { return false; }
}

#endif // CMD_GLDEPTHMASK_HPP
