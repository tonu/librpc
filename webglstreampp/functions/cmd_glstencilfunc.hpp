#ifndef CMD_GLSTENCILFUNC_HPP
#define CMD_GLSTENCILFUNC_HPP
//typedef void          (*glStencilFunc_ptr) (GLenum func, GLint ref, GLuint mask);

#include "cmd_includes.hpp"

namespace cmd_glStencilFunc
{
static void client_out(FIFO &fifo, GLenum func, GLint ref, GLuint mask)
{
    DBG_TRAFFIC("(func=" << glesType(func) << ", ref=" << ref << ", mask=" << mask << ")");

    fifo << arg<GLenum>(func);
    fifo << arg<GLint>(ref);
    fifo << arg<GLuint>(mask);
}

static void client_in(FIFO &fifo)
{

}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
{
    arg<GLenum> func;
    arg<GLint> ref;
    arg<GLuint> mask;

    fifoIn >> func >> ref >> mask;

#ifdef BUILD_SERVER
    glStencilFunc(func.val, ref.val, mask.val);
#endif
}

static bool returns() { return false; }
}
#endif // CMD_GLSTENCILFUNC_HPP
