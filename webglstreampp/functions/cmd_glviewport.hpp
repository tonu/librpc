#ifndef CMD_GLVIEWPORT_HPP
#define CMD_GLVIEWPORT_HPP

#include "cmd_includes.hpp"

namespace cmd_glViewport
{
    static void client_out(FIFO &fifo, GLint x, GLint y, GLsizei width, GLsizei height)
    {
        DBG_TRAFFIC("(x=" << x << ", y=" << y << ", width=" << width << ", height=" << height << ")");

        fifo << arg<int32_t>(x);
        fifo << arg<int32_t>(y);
        fifo << arg<int32_t>(width);
        fifo << arg<int32_t>(height);

//        DBG_TRAFFIC("   cmd_glViewport:" << x << " " << y << " " << width << " " << height << "\n");
    }

    static void client_in(FIFO &)
    {

    }

// what to execute on server
    static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
    {
        arg<int32_t> x;
        arg<int32_t> y;
        arg<int32_t> width;
        arg<int32_t> height;

        fifoIn >> x >> y >> width >> height;

#ifdef BUILD_SERVER
        glViewport(x.val, y.val, width.val, height.val);
#endif
    }

    static bool returns() { return false; }

}


#endif // CMD_GLVIEWPORT_HPP
