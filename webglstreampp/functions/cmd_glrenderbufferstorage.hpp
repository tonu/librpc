//
// Created by Tonu Jaansoo on 18/08/16.
//

#ifndef BUILD_ALL_CMD_GLRENDERBUFFERSTORAGE_HPP
#define BUILD_ALL_CMD_GLRENDERBUFFERSTORAGE_HPP
//    GL_APICALL void GL_APIENTRY glRenderbufferStorage (GLenum target, GLenum internalformat, GLsizei width, GLsizei height);


#include "cmd_includes.hpp"

namespace cmd_glRenderbufferStorage
{
    static void client_out(FIFO &fifo, GLenum target, GLenum internalformat, GLsizei width, GLsizei height)
{
    DBG_TRAFFIC("(target=" << glesType(target) << ", internalformat=" << glesType(internalformat) << ", width=" << width << ", height=" << height << ")");

    fifo << arg<GLenum>(target);
    fifo << arg<GLenum>(internalformat);
    fifo << arg<GLsizei>(width);
    fifo << arg<GLsizei>(height);
}

static void client_in(FIFO &fifo)
{

}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
{
    arg<GLenum> target;
    arg<GLenum> internalformat;
    arg<GLsizei> width;
    arg<GLsizei> height;

    fifoIn >> target >> internalformat >> width >> height;
#ifdef BUILD_SERVER
    glRenderbufferStorage(target.val, internalformat.val, width.val, height.val);
#endif
}

static bool returns() { return false; }
}

#endif //BUILD_ALL_CMD_GLRENDERBUFFERSTORAGE_HPP
