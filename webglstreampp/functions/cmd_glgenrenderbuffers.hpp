//
// Created by Tonu Jaansoo on 18/08/16.
//

#ifndef BUILD_ALL_CMD_GLGENRENDERBUFFERS_HPP
#define BUILD_ALL_CMD_GLGENRENDERBUFFERS_HPP
//    GL_APICALL void GL_APIENTRY glGenRenderbuffers (GLsizei n, GLuint *renderbuffers);

#include "cmd_includes.hpp"

namespace cmd_glGenRenderbuffers
{
    static void client_out(FIFO &fifo, GLsizei n, GLuint* buffers)
    {
        DBG_TRAFFIC("(n=" << n << ", buffers=...)");
        fifo << arg<GLsizei>(n) << arg<buffer<GLuint>>(buffer<GLuint>(buffers, n));
    }

    static void client_in(FIFO &fifo, GLuint* buffers)
    {
        arg<buffer<GLuint>> buf;

        fifo >> buf;

        memcpy(buffers, buf.val._ptr.get(), buf.val._sz * sizeof(GLuint) );

        DBG_TRAFFIC(" returned: buffers[0]=" << buffers[0]);
    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLsizei> n;
        arg<buffer<GLuint>> renderbuffers;

        fifoIn >> n >> renderbuffers;

#ifdef BUILD_SERVER
        glGenRenderbuffers(n.val, renderbuffers.val._ptr.get());
#endif
        fifoOut << renderbuffers;
    }

    static bool returns() { return true; }
}

#endif //BUILD_ALL_CMD_GLGENRENDERBUFFERS_HPP
