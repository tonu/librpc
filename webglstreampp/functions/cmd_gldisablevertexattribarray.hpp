#ifndef CMD_GLDISABLEVERTEXATTRIBARRAY_HPP
#define CMD_GLDISABLEVERTEXATTRIBARRAY_HPP
//typedef void          (*glDisableVertexAttribArray_ptr) (GLuint index);


#include "cmd_includes.hpp"

namespace cmd_glDisableVertexAttribArray
{
    static void client_out(FIFO &fifo, GLuint index)
    {
        DBG_TRAFFIC("(index=" << index << ")");

        fifo << arg<GLuint>(index);
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLuint> index;
        fifoIn >> index;

#ifdef BUILD_SERVER
        glDisableVertexAttribArray(index.val);
#endif
    }

    static bool returns() { return false; }
}
#endif // CMD_GLDISABLEVERTEXATTRIBARRAY_HPP
