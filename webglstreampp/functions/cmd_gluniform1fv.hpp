//
// Created by Tonu Jaansoo on 18/08/16.
//

#ifndef BUILD_ALL_CMD_GLUNIFORM1FV_HPP
#define BUILD_ALL_CMD_GLUNIFORM1FV_HPP
//    GL_APICALL void GL_APIENTRY glUniform1fv (GLint location, GLsizei count, const GLfloat *value);


#include "cmd_includes.hpp"

namespace cmd_glUniform1fv
{
    static void client_out(FIFO &fifo, GLint location, GLsizei count, const GLfloat* v)
    {
        DBG_TRAFFIC("(location=" << location << ", count=" << count << ", v=...) v[0] =" << v[0]);

        fifo << arg<GLint>(location);
        fifo << arg<GLsizei>(count);
        fifo << arg<buffer<GLfloat>>(buffer<GLfloat>(v, 1 * count));
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
    {
        arg<GLint> location;
        arg<GLsizei> count;
        arg<buffer<GLfloat>> v;

        fifoIn >> location >> count >> v;

#ifdef BUILD_SERVER
        glUniform1fv(location.val, count.val, v.val._ptr.get());
#endif
    }

    static bool returns() { return false; }
}

#endif //BUILD_ALL_CMD_GLUNIFORM1FV_HPP
