#ifndef CMD_GLGENBUFFERS_HPP
#define CMD_GLGENBUFFERS_HPP
//typedef void          (*glGenBuffers_ptr) (GLsizei n, GLuint* buffers);

#include "cmd_includes.hpp"

namespace cmd_glGenBuffers
{
    static void client_out(FIFO &fifo, GLsizei n, GLuint* buffers)
    {
        DBG_TRAFFIC("(n=" << n << ", buffers=...)");

        fifo << arg<GLsizei>(n) << arg<buffer<GLuint>>(buffer<GLuint>(buffers, n));
    }

    static void client_in(FIFO &fifo, GLuint* buffers)
    {
        arg<buffer<GLuint>> buf;

        fifo >> buf;
        DBG_TRAFFIC(" buffers[0]=" << buf.val._ptr.get()[0]);

        memcpy(buffers, buf.val._ptr.get(), buf.val._sz * sizeof(GLuint) );
    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLsizei> n;
        arg<buffer<GLuint>> buffers;

        fifoIn >> n >> buffers;

#ifdef BUILD_SERVER
        glGenBuffers(n.val, buffers.val._ptr.get());
#endif
        fifoOut << buffers;
    }

    static bool returns() { return true; }
}

#endif // CMD_GLGENBUFFERS_HPP
