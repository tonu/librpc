#ifndef CMD_GLDRAWELEMENTS_HPP
#define CMD_GLDRAWELEMENTS_HPP

//GL_APICALL void GL_APIENTRY glDrawElements (GLenum mode, GLsizei count, GLenum type, const void *indices);


#include "cmd_includes.hpp"

namespace cmd_glDrawElements
{
    static void client_out(FIFO &fifo, GLenum mode, GLsizei count, GLenum type, const void *indices)
    {
        DBG_TRAFFIC("(mode=" << glesType(mode) << ", count=" << count << ", type=" << glesType(type) << ", indices=" << uint64_t(indices) << ")");


        bool isGPU = (uint64_t(indices) < 0x10000);
        assert(isGPU);

        uint16_t offset = (uint16_t)(uint64_t(indices) & 0xffff);

        fifo << arg<GLenum>(mode);
        fifo << arg<GLsizei>(count);
        fifo << arg<GLenum>(type);
        fifo << arg<uint16_t>(offset);

//        if (count > 1000)
//        {
//            std::cerr  <<" (warning.. count too big)\n";
//        }
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
    {
        arg<GLenum> mode;
        arg<GLsizei> count;
        arg<GLenum> type;
        arg<uint16_t> indices;

        fifoIn >> mode >> count >> type >> indices;

#ifdef BUILD_SERVER

        glDrawElements(mode.val, count.val, type.val,  (const void *)(GLsizeiptr)(indices.val));


//    static GLuint ind[1];
//    static int init = 0;
//    if (init == 0){
//        init = 1;
//        glGenBuffers (1, ind);
//    }
//
//    uint16_t inds[] = {0,1,2,0,2,3};
//
//    glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, ind[0]);
//    glBufferData (GL_ELEMENT_ARRAY_BUFFER, 12, inds, GL_STATIC_DRAW);
//    glDrawElements(mode.val, count.val, type.val,  (const void *)(GLsizeiptr)(indices.val));
//    glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);
//
//    glClearColor(0,1,0,0);
//    glClear(GL_COLOR_BUFFER_BIT);
#endif
    }

    static bool returns() { return false; }
}

#endif // CMD_GLDRAWELEMENTS_HPP
