#ifndef CMD_GLDEPTHFUNC_HPP
#define CMD_GLDEPTHFUNC_HPP

//typedef void          (*glDepthFunc_ptr) (GLenum func);

#include "cmd_includes.hpp"

namespace cmd_glDepthFunc
{
static void client_out(FIFO &fifo, GLenum func)
{
    DBG_TRAFFIC("(func=" << glesType(func) << ")");

    fifo << arg<GLenum>(func);
}

static void client_in(FIFO &fifo)
{

}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
{
    arg<GLenum> func;
    fifoIn >> func;

#ifdef BUILD_SERVER
    glDepthFunc(func.val);
#endif
}

static bool returns() { return false; }
}

#endif // CMD_GLDEPTHFUNC_HPP
