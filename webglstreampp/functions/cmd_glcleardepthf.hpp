#ifndef CMD_GLCLEARDEPTHF_HPP
#define CMD_GLCLEARDEPTHF_HPP

//typedef void          (*glClearDepthf_ptr) (GLclampf depth);

#include "cmd_includes.hpp"

namespace cmd_glClearDepthf
{
static void client_out(FIFO &fifo, GLclampf depth)
{
    DBG_TRAFFIC("(depth=" << depth << ")");

    fifo << arg<GLclampf>(depth);
}

static void client_in(FIFO &fifo)
{

}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
{
    arg<GLclampf> depth;

    fifoIn >> depth;

#ifdef BUILD_SERVER
    glClearDepthf(depth.val);
#endif
}

static bool returns() { return false; }
}

#endif // CMD_GLCLEARDEPTHF_HPP
