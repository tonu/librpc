#ifndef CMD_GLBINDTEXTURE_HPP
#define CMD_GLBINDTEXTURE_HPP

//typedef void          (*glBindTexture_ptr) (GLenum target, GLuint texture);

#include "cmd_includes.hpp"

namespace cmd_glBindTexture
{
    static void client_out(FIFO &fifo, GLenum target, GLuint texture)
    {
        DBG_TRAFFIC("(target=" << glesType(target) << ", texture=" << texture << ")");
        fifo << arg<GLenum>(target) << arg<GLuint>(texture);
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLenum> target;
        arg<GLuint> texture;

        fifoIn >> target >> texture;

#ifdef BUILD_SERVER
        glBindTexture(target.val, texture.val);
#endif
    }

    static bool returns() { return false; }
}
#endif // CMD_GLBINDTEXTURE_HPP
