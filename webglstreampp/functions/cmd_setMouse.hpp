//
// Created by Tonu Jaansoo on 23/08/16.
//

#ifndef BUILD_ALL_CMD_SETMOUSE_HPP
#define BUILD_ALL_CMD_SETMOUSE_HPP

#ifndef BUILD_SERVER
typedef void setMouse_t(int16_t x, int16_t y, uint8_t buttons);
#endif

#include "cmd_includes.hpp"

#include "../server.hpp"

namespace cmd_setMouse
{
    static void client_out(FIFO &fifo, int16_t x, int16_t y, uint8_t buttons)
    {
        DBG_TRAFFIC("(x=" << x << ", y=" << y << ", buttons=" << buttons << ")");

        fifo << arg<int16_t>(x);
        fifo << arg<int16_t>(y);
        fifo << arg<uint8_t>(buttons);

    }

    static void client_in(FIFO &)
    {

    }

    static void serv_in(Server *const server, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<int16_t> x;
        arg<int16_t> y;
        arg<uint8_t> buttons;

        fifoIn >> x >> y >> buttons;

#ifndef BUILD_SERVER
        setMouse_t *s = (setMouse_t*)server->callback("setMouse");

        if (s)
           s(x.val, y.val, buttons.val);
#endif
    }

    static bool returns() { return false; }

}

#endif //BUILD_ALL_CMD_SETMOUSE_HPP
