//
// Created by Tonu Jaansoo on 18/08/16.
//

#ifndef BUILD_ALL_CMD_GLBINDRENDERBUFFER_HPP
#define BUILD_ALL_CMD_GLBINDRENDERBUFFER_HPP
//    GL_APICALL void GL_APIENTRY glBindRenderbuffer (GLenum target, GLuint renderbuffer);

#include "cmd_includes.hpp"

namespace cmd_glBindRenderbuffer
{
    static void client_out(FIFO &fifo, GLenum target, GLuint renderbuffer)
    {
        DBG_TRAFFIC("(target=" << glesType(target) << ", renderbuffer=" << renderbuffer << ")");
        fifo << arg<GLenum>(target) << arg<GLuint>(renderbuffer);
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLenum> target;
        arg<GLuint> renderbuffer;

        fifoIn >> target >> renderbuffer;

#ifdef BUILD_SERVER
        glBindRenderbuffer(target.val, renderbuffer.val);
#endif
    }

    static bool returns() { return false; }
}
#endif //BUILD_ALL_CMD_GLBINDRENDERBUFFER_HPP
