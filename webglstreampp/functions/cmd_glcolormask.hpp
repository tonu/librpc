#ifndef CMD_GLCOLORMASK_HPP
#define CMD_GLCOLORMASK_HPP
//typedef void          (*glColorMask_ptr) (GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha);

#include "cmd_includes.hpp"

namespace cmd_glColorMask
{
    static void client_out(FIFO &fifo, GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha)
    {
        DBG_TRAFFIC("(red=" << int(red) << ", green=" << int(green) << ", blue=" << int(blue) << ", alpha=" << int(alpha) << ")");
        fifo << arg<GLboolean>(red);
        fifo << arg<GLboolean>(green);
        fifo << arg<GLboolean>(blue);
        fifo << arg<GLboolean>(alpha);
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLboolean> red;
        arg<GLboolean> green;
        arg<GLboolean> blue;
        arg<GLboolean> alpha;

        fifoIn >> red >> green >> blue >> alpha;

#ifdef BUILD_SERVER
        glColorMask(red.val, green.val, blue.val, alpha.val);
#endif
    }

    static bool returns() { return false; }
}

#endif // CMD_GLCOLORMASK_HPP
