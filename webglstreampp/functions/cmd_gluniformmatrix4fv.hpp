//
// Created by Tonu Jaansoo on 18/08/16.
//

#ifndef BUILD_ALL_CMD_GLUNIFORMMATRIX4FV_HPP
#define BUILD_ALL_CMD_GLUNIFORMMATRIX4FV_HPP
//GL_APICALL void GL_APIENTRY glUniformMatrix4fv (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);

#include "cmd_includes.hpp"

namespace cmd_glUniformMatrix4fv
{
    static void client_out(FIFO &fifo, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value)
    {
        DBG_TRAFFIC("(location=" << location << ", count=" << count << ", transpose=" << int(transpose) << ", v=...)  v[0]=[");
        for (int i=0;i<16; i++)
            DBG_TRAFFIC(value[i] << ",");
        DBG_TRAFFIC("]");

        fifo << arg<GLint>(location);
        fifo << arg<GLsizei>(count);
        fifo << arg<GLboolean>(transpose);
        fifo << arg<buffer<GLfloat>>( buffer<GLfloat>(value, count * 16)); // count * 4x4
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLint> location;
        arg<GLsizei> count;
        arg<GLboolean> transpose;
        arg<buffer<GLfloat>> value;

        fifoIn >> location >> count >> transpose >> value;
#ifdef BUILD_SERVER
        glUniformMatrix4fv(location.val, count.val, transpose.val, value.val._ptr.get());
#endif
    }

    static bool returns() { return false; }
}

#endif //BUILD_ALL_CMD_GLUNIFORMMATRIX4FV_HPP
