#ifndef CMD_GLCOPYTEXSUBIMAGE2D_HPP
#define CMD_GLCOPYTEXSUBIMAGE2D_HPP
//typedef void          (*glCopyTexSubImage2D_ptr) (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height);

#include "cmd_includes.hpp"

namespace cmd_glCopyTexSubImage2D
{
static void client_out(FIFO &fifo, GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height)
{
    DBG_TRAFFIC("(target=" << glesType(target) << ", level=" << level << ", xoffset=" << xoffset << ", yoffset=" << yoffset << ", x=" << x << ", y=" << y << ", width=" << width << ", height=" << height << ")" );

    fifo << arg<GLenum>(target);
    fifo << arg<GLint>(level);
    fifo << arg<GLint>(xoffset);
    fifo << arg<GLint>(yoffset);
    fifo << arg<GLint>(x);
    fifo << arg<GLint>(y);
    fifo << arg<GLsizei>(width);
    fifo << arg<GLsizei>(height);
}

static void client_in(FIFO &)
{

}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
{
    arg<GLenum> target;
    arg<GLint> level;
    arg<GLint> xoffset;
    arg<GLint> yoffset;
    arg<GLint> x;
    arg<GLint> y;
    arg<GLsizei> width;
    arg<GLsizei> height;

    fifoIn >> target >> level >> xoffset >> yoffset >> x >> y >> width >> height;

#ifdef BUILD_SERVER
    glCopyTexSubImage2D(target.val, level.val, xoffset.val, yoffset.val, x.val, y.val, width.val, height.val);
#endif
}

static bool returns() { return false; }
}

#endif // CMD_GLCOPYTEXSUBIMAGE2D_HPP
