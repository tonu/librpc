//
// Created by Tonu Jaansoo on 17/08/16.
//

#ifndef BUILD_ALL_GLES_INCLUDES_HPP
#define BUILD_ALL_GLES_INCLUDES_HPP
#include <map>
#include <string>

#include "gles_defs.hpp"
#include <iomanip>
#include <sstream>
#include <cassert>

#undef CMD
#define CMD(n) #n,

static std::string gles_names[] = { "ERROR", DEFS };


// fixme .. teha l2bi geti ja throwida kui ei leia
static std::map<int, std::string> gles_types =  {

        //GL ES 2
        std::make_pair(0x00000100, "GL_DEPTH_BUFFER_BIT"),
        std::make_pair(0x00000400, "GL_STENCIL_BUFFER_BIT"),
        std::make_pair(0x00004000, "GL_COLOR_BUFFER_BIT"),
        std::make_pair(0, "GL_FALSE"),
        std::make_pair(1, "GL_TRUE"),
        std::make_pair(0x0000, "GL_POINTS"),
        std::make_pair(0x0001, "GL_LINES"),
        std::make_pair(0x0002, "GL_LINE_LOOP"),
        std::make_pair(0x0003, "GL_LINE_STRIP"),
        std::make_pair(0x0004, "GL_TRIANGLES"),
        std::make_pair(0x0005, "GL_TRIANGLE_STRIP"),
        std::make_pair(0x0006, "GL_TRIANGLE_FAN"),
        std::make_pair(0, "GL_ZERO"),
        std::make_pair(1, "GL_ONE"),
        std::make_pair(0x0300, "GL_SRC_COLOR"),
        std::make_pair(0x0301, "GL_ONE_MINUS_SRC_COLOR"),
        std::make_pair(0x0302, "GL_SRC_ALPHA"),
        std::make_pair(0x0303, "GL_ONE_MINUS_SRC_ALPHA"),
        std::make_pair(0x0304, "GL_DST_ALPHA"),
        std::make_pair(0x0305, "GL_ONE_MINUS_DST_ALPHA"),
        std::make_pair(0x0306, "GL_DST_COLOR"),
        std::make_pair(0x0307, "GL_ONE_MINUS_DST_COLOR"),
        std::make_pair(0x0308, "GL_SRC_ALPHA_SATURATE"),
        std::make_pair(0x8006, "GL_FUNC_ADD"),
        std::make_pair(0x8009, "GL_BLEND_EQUATION"),
        std::make_pair(0x8009, "GL_BLEND_EQUATION_RGB"),
        std::make_pair(0x883D, "GL_BLEND_EQUATION_ALPHA"),
        std::make_pair(0x800A, "GL_FUNC_SUBTRACT"),
        std::make_pair(0x800B, "GL_FUNC_REVERSE_SUBTRACT"),
        std::make_pair(0x80C8, "GL_BLEND_DST_RGB"),
        std::make_pair(0x80C9, "GL_BLEND_SRC_RGB"),
        std::make_pair(0x80CA, "GL_BLEND_DST_ALPHA"),
        std::make_pair(0x80CB, "GL_BLEND_SRC_ALPHA"),
        std::make_pair(0x8001, "GL_CONSTANT_COLOR"),
        std::make_pair(0x8002, "GL_ONE_MINUS_CONSTANT_COLOR"),
        std::make_pair(0x8003, "GL_CONSTANT_ALPHA"),
        std::make_pair(0x8004, "GL_ONE_MINUS_CONSTANT_ALPHA"),
        std::make_pair(0x8005, "GL_BLEND_COLOR"),
        std::make_pair(0x8892, "GL_ARRAY_BUFFER"),
        std::make_pair(0x8893, "GL_ELEMENT_ARRAY_BUFFER"),
        std::make_pair(0x8894, "GL_ARRAY_BUFFER_BINDING"),
        std::make_pair(0x8895, "GL_ELEMENT_ARRAY_BUFFER_BINDING"),
        std::make_pair(0x88E0, "GL_STREAM_DRAW"),
        std::make_pair(0x88E4, "GL_STATIC_DRAW"),
        std::make_pair(0x88E8, "GL_DYNAMIC_DRAW"),
        std::make_pair(0x8764, "GL_BUFFER_SIZE"),
        std::make_pair(0x8765, "GL_BUFFER_USAGE"),
        std::make_pair(0x8626, "GL_CURRENT_VERTEX_ATTRIB"),
        std::make_pair(0x0404, "GL_FRONT"),
        std::make_pair(0x0405, "GL_BACK"),
        std::make_pair(0x0408, "GL_FRONT_AND_BACK"),
        std::make_pair(0x0DE1, "GL_TEXTURE_2D"),
        std::make_pair(0x0B44, "GL_CULL_FACE"),
        std::make_pair(0x0BE2, "GL_BLEND"),
        std::make_pair(0x0BD0, "GL_DITHER"),
        std::make_pair(0x0B90, "GL_STENCIL_TEST"),
        std::make_pair(0x0B71, "GL_DEPTH_TEST"),
        std::make_pair(0x0C11, "GL_SCISSOR_TEST"),
        std::make_pair(0x8037, "GL_POLYGON_OFFSET_FILL"),
        std::make_pair(0x809E, "GL_SAMPLE_ALPHA_TO_COVERAGE"),
        std::make_pair(0x80A0, "GL_SAMPLE_COVERAGE"),
        std::make_pair(0, "GL_NO_ERROR"),
        std::make_pair(0x0500, "GL_INVALID_ENUM"),
        std::make_pair(0x0501, "GL_INVALID_VALUE"),
        std::make_pair(0x0502, "GL_INVALID_OPERATION"),
        std::make_pair(0x0505, "GL_OUT_OF_MEMORY"),
        std::make_pair(0x0900, "GL_CW"),
        std::make_pair(0x0901, "GL_CCW"),
        std::make_pair(0x0B21, "GL_LINE_WIDTH"),
        std::make_pair(0x846D, "GL_ALIASED_POINT_SIZE_RANGE"),
        std::make_pair(0x846E, "GL_ALIASED_LINE_WIDTH_RANGE"),
        std::make_pair(0x0B45, "GL_CULL_FACE_MODE"),
        std::make_pair(0x0B46, "GL_FRONT_FACE"),
        std::make_pair(0x0B70, "GL_DEPTH_RANGE"),
        std::make_pair(0x0B72, "GL_DEPTH_WRITEMASK"),
        std::make_pair(0x0B73, "GL_DEPTH_CLEAR_VALUE"),
        std::make_pair(0x0B74, "GL_DEPTH_FUNC"),
        std::make_pair(0x0B91, "GL_STENCIL_CLEAR_VALUE"),
        std::make_pair(0x0B92, "GL_STENCIL_FUNC"),
        std::make_pair(0x0B94, "GL_STENCIL_FAIL"),
        std::make_pair(0x0B95, "GL_STENCIL_PASS_DEPTH_FAIL"),
        std::make_pair(0x0B96, "GL_STENCIL_PASS_DEPTH_PASS"),
        std::make_pair(0x0B97, "GL_STENCIL_REF"),
        std::make_pair(0x0B93, "GL_STENCIL_VALUE_MASK"),
        std::make_pair(0x0B98, "GL_STENCIL_WRITEMASK"),
        std::make_pair(0x8800, "GL_STENCIL_BACK_FUNC"),
        std::make_pair(0x8801, "GL_STENCIL_BACK_FAIL"),
        std::make_pair(0x8802, "GL_STENCIL_BACK_PASS_DEPTH_FAIL"),
        std::make_pair(0x8803, "GL_STENCIL_BACK_PASS_DEPTH_PASS"),
        std::make_pair(0x8CA3, "GL_STENCIL_BACK_REF"),
        std::make_pair(0x8CA4, "GL_STENCIL_BACK_VALUE_MASK"),
        std::make_pair(0x8CA5, "GL_STENCIL_BACK_WRITEMASK"),
        std::make_pair(0x0BA2, "GL_VIEWPORT"),
        std::make_pair(0x0C10, "GL_SCISSOR_BOX"),
        std::make_pair(0x0C22, "GL_COLOR_CLEAR_VALUE"),
        std::make_pair(0x0C23, "GL_COLOR_WRITEMASK"),
        std::make_pair(0x0CF5, "GL_UNPACK_ALIGNMENT"),
        std::make_pair(0x0D05, "GL_PACK_ALIGNMENT"),
        std::make_pair(0x0D33, "GL_MAX_TEXTURE_SIZE"),
        std::make_pair(0x0D3A, "GL_MAX_VIEWPORT_DIMS"),
        std::make_pair(0x0D50, "GL_SUBPIXEL_BITS"),
        std::make_pair(0x0D52, "GL_RED_BITS"),
        std::make_pair(0x0D53, "GL_GREEN_BITS"),
        std::make_pair(0x0D54, "GL_BLUE_BITS"),
        std::make_pair(0x0D55, "GL_ALPHA_BITS"),
        std::make_pair(0x0D56, "GL_DEPTH_BITS"),
        std::make_pair(0x0D57, "GL_STENCIL_BITS"),
        std::make_pair(0x2A00, "GL_POLYGON_OFFSET_UNITS"),
        std::make_pair(0x8038, "GL_POLYGON_OFFSET_FACTOR"),
        std::make_pair(0x8069, "GL_TEXTURE_BINDING_2D"),
        std::make_pair(0x80A8, "GL_SAMPLE_BUFFERS"),
        std::make_pair(0x80A9, "GL_SAMPLES"),
        std::make_pair(0x80AA, "GL_SAMPLE_COVERAGE_VALUE"),
        std::make_pair(0x80AB, "GL_SAMPLE_COVERAGE_INVERT"),
        std::make_pair(0x86A2, "GL_NUM_COMPRESSED_TEXTURE_FORMATS"),
        std::make_pair(0x86A3, "GL_COMPRESSED_TEXTURE_FORMATS"),
        std::make_pair(0x1100, "GL_DONT_CARE"),
        std::make_pair(0x1101, "GL_FASTEST"),
        std::make_pair(0x1102, "GL_NICEST"),
        std::make_pair(0x8192, "GL_GENERATE_MIPMAP_HINT"),
        std::make_pair(0x1400, "GL_BYTE"),
        std::make_pair(0x1401, "GL_UNSIGNED_BYTE"),
        std::make_pair(0x1402, "GL_SHORT"),
        std::make_pair(0x1403, "GL_UNSIGNED_SHORT"),
        std::make_pair(0x1404, "GL_INT"),
        std::make_pair(0x1405, "GL_UNSIGNED_INT"),
        std::make_pair(0x1406, "GL_FLOAT"),
        std::make_pair(0x140C, "GL_FIXED"),
        std::make_pair(0x1902, "GL_DEPTH_COMPONENT"),
        std::make_pair(0x1906, "GL_ALPHA"),
        std::make_pair(0x1907, "GL_RGB"),
        std::make_pair(0x1908, "GL_RGBA"),
        std::make_pair(0x1909, "GL_LUMINANCE"),
        std::make_pair(0x190A, "GL_LUMINANCE_ALPHA"),
        std::make_pair(0x8033, "GL_UNSIGNED_SHORT_4_4_4_4"),
        std::make_pair(0x8034, "GL_UNSIGNED_SHORT_5_5_5_1"),
        std::make_pair(0x8363, "GL_UNSIGNED_SHORT_5_6_5"),
        std::make_pair(0x8B30, "GL_FRAGMENT_SHADER"),
        std::make_pair(0x8B31, "GL_VERTEX_SHADER"),
        std::make_pair(0x8869, "GL_MAX_VERTEX_ATTRIBS"),
        std::make_pair(0x8DFB, "GL_MAX_VERTEX_UNIFORM_VECTORS"),
        std::make_pair(0x8DFC, "GL_MAX_VARYING_VECTORS"),
        std::make_pair(0x8B4D, "GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS"),
        std::make_pair(0x8B4C, "GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS"),
        std::make_pair(0x8872, "GL_MAX_TEXTURE_IMAGE_UNITS"),
        std::make_pair(0x8DFD, "GL_MAX_FRAGMENT_UNIFORM_VECTORS"),
        std::make_pair(0x8B4F, "GL_SHADER_TYPE"),
        std::make_pair(0x8B80, "GL_DELETE_STATUS"),
        std::make_pair(0x8B82, "GL_LINK_STATUS"),
        std::make_pair(0x8B83, "GL_VALIDATE_STATUS"),
        std::make_pair(0x8B85, "GL_ATTACHED_SHADERS"),
        std::make_pair(0x8B86, "GL_ACTIVE_UNIFORMS"),
        std::make_pair(0x8B87, "GL_ACTIVE_UNIFORM_MAX_LENGTH"),
        std::make_pair(0x8B89, "GL_ACTIVE_ATTRIBUTES"),
        std::make_pair(0x8B8A, "GL_ACTIVE_ATTRIBUTE_MAX_LENGTH"),
        std::make_pair(0x8B8C, "GL_SHADING_LANGUAGE_VERSION"),
        std::make_pair(0x8B8D, "GL_CURRENT_PROGRAM"),
        std::make_pair(0x0200, "GL_NEVER"),
        std::make_pair(0x0201, "GL_LESS"),
        std::make_pair(0x0202, "GL_EQUAL"),
        std::make_pair(0x0203, "GL_LEQUAL"),
        std::make_pair(0x0204, "GL_GREATER"),
        std::make_pair(0x0205, "GL_NOTEQUAL"),
        std::make_pair(0x0206, "GL_GEQUAL"),
        std::make_pair(0x0207, "GL_ALWAYS"),
        std::make_pair(0x1E00, "GL_KEEP"),
        std::make_pair(0x1E01, "GL_REPLACE"),
        std::make_pair(0x1E02, "GL_INCR"),
        std::make_pair(0x1E03, "GL_DECR"),
        std::make_pair(0x150A, "GL_INVERT"),
        std::make_pair(0x8507, "GL_INCR_WRAP"),
        std::make_pair(0x8508, "GL_DECR_WRAP"),
        std::make_pair(0x1F00, "GL_VENDOR"),
        std::make_pair(0x1F01, "GL_RENDERER"),
        std::make_pair(0x1F02, "GL_VERSION"),
        std::make_pair(0x1F03, "GL_EXTENSIONS"),
        std::make_pair(0x2600, "GL_NEAREST"),
        std::make_pair(0x2601, "GL_LINEAR"),
        std::make_pair(0x2700, "GL_NEAREST_MIPMAP_NEAREST"),
        std::make_pair(0x2701, "GL_LINEAR_MIPMAP_NEAREST"),
        std::make_pair(0x2702, "GL_NEAREST_MIPMAP_LINEAR"),
        std::make_pair(0x2703, "GL_LINEAR_MIPMAP_LINEAR"),
        std::make_pair(0x2800, "GL_TEXTURE_MAG_FILTER"),
        std::make_pair(0x2801, "GL_TEXTURE_MIN_FILTER"),
        std::make_pair(0x2802, "GL_TEXTURE_WRAP_S"),
        std::make_pair(0x2803, "GL_TEXTURE_WRAP_T"),
        std::make_pair(0x1702, "GL_TEXTURE"),
        std::make_pair(0x8513, "GL_TEXTURE_CUBE_MAP"),
        std::make_pair(0x8514, "GL_TEXTURE_BINDING_CUBE_MAP"),
        std::make_pair(0x8515, "GL_TEXTURE_CUBE_MAP_POSITIVE_X"),
        std::make_pair(0x8516, "GL_TEXTURE_CUBE_MAP_NEGATIVE_X"),
        std::make_pair(0x8517, "GL_TEXTURE_CUBE_MAP_POSITIVE_Y"),
        std::make_pair(0x8518, "GL_TEXTURE_CUBE_MAP_NEGATIVE_Y"),
        std::make_pair(0x8519, "GL_TEXTURE_CUBE_MAP_POSITIVE_Z"),
        std::make_pair(0x851A, "GL_TEXTURE_CUBE_MAP_NEGATIVE_Z"),
        std::make_pair(0x851C, "GL_MAX_CUBE_MAP_TEXTURE_SIZE"),
        std::make_pair(0x84C0, "GL_TEXTURE0"),
        std::make_pair(0x84C1, "GL_TEXTURE1"),
        std::make_pair(0x84C2, "GL_TEXTURE2"),
        std::make_pair(0x84C3, "GL_TEXTURE3"),
        std::make_pair(0x84C4, "GL_TEXTURE4"),
        std::make_pair(0x84C5, "GL_TEXTURE5"),
        std::make_pair(0x84C6, "GL_TEXTURE6"),
        std::make_pair(0x84C7, "GL_TEXTURE7"),
        std::make_pair(0x84C8, "GL_TEXTURE8"),
        std::make_pair(0x84C9, "GL_TEXTURE9"),
        std::make_pair(0x84CA, "GL_TEXTURE10"),
        std::make_pair(0x84CB, "GL_TEXTURE11"),
        std::make_pair(0x84CC, "GL_TEXTURE12"),
        std::make_pair(0x84CD, "GL_TEXTURE13"),
        std::make_pair(0x84CE, "GL_TEXTURE14"),
        std::make_pair(0x84CF, "GL_TEXTURE15"),
        std::make_pair(0x84D0, "GL_TEXTURE16"),
        std::make_pair(0x84D1, "GL_TEXTURE17"),
        std::make_pair(0x84D2, "GL_TEXTURE18"),
        std::make_pair(0x84D3, "GL_TEXTURE19"),
        std::make_pair(0x84D4, "GL_TEXTURE20"),
        std::make_pair(0x84D5, "GL_TEXTURE21"),
        std::make_pair(0x84D6, "GL_TEXTURE22"),
        std::make_pair(0x84D7, "GL_TEXTURE23"),
        std::make_pair(0x84D8, "GL_TEXTURE24"),
        std::make_pair(0x84D9, "GL_TEXTURE25"),
        std::make_pair(0x84DA, "GL_TEXTURE26"),
        std::make_pair(0x84DB, "GL_TEXTURE27"),
        std::make_pair(0x84DC, "GL_TEXTURE28"),
        std::make_pair(0x84DD, "GL_TEXTURE29"),
        std::make_pair(0x84DE, "GL_TEXTURE30"),
        std::make_pair(0x84DF, "GL_TEXTURE31"),
        std::make_pair(0x84E0, "GL_ACTIVE_TEXTURE"),
        std::make_pair(0x2901, "GL_REPEAT"),
        std::make_pair(0x812F, "GL_CLAMP_TO_EDGE"),
        std::make_pair(0x8370, "GL_MIRRORED_REPEAT"),
        std::make_pair(0x8B50, "GL_FLOAT_VEC2"),
        std::make_pair(0x8B51, "GL_FLOAT_VEC3"),
        std::make_pair(0x8B52, "GL_FLOAT_VEC4"),
        std::make_pair(0x8B53, "GL_INT_VEC2"),
        std::make_pair(0x8B54, "GL_INT_VEC3"),
        std::make_pair(0x8B55, "GL_INT_VEC4"),
        std::make_pair(0x8B56, "GL_BOOL"),
        std::make_pair(0x8B57, "GL_BOOL_VEC2"),
        std::make_pair(0x8B58, "GL_BOOL_VEC3"),
        std::make_pair(0x8B59, "GL_BOOL_VEC4"),
        std::make_pair(0x8B5A, "GL_FLOAT_MAT2"),
        std::make_pair(0x8B5B, "GL_FLOAT_MAT3"),
        std::make_pair(0x8B5C, "GL_FLOAT_MAT4"),
        std::make_pair(0x8B5E, "GL_SAMPLER_2D"),
        std::make_pair(0x8B60, "GL_SAMPLER_CUBE"),
        std::make_pair(0x8622, "GL_VERTEX_ATTRIB_ARRAY_ENABLED"),
        std::make_pair(0x8623, "GL_VERTEX_ATTRIB_ARRAY_SIZE"),
        std::make_pair(0x8624, "GL_VERTEX_ATTRIB_ARRAY_STRIDE"),
        std::make_pair(0x8625, "GL_VERTEX_ATTRIB_ARRAY_TYPE"),
        std::make_pair(0x886A, "GL_VERTEX_ATTRIB_ARRAY_NORMALIZED"),
        std::make_pair(0x8645, "GL_VERTEX_ATTRIB_ARRAY_POINTER"),
        std::make_pair(0x889F, "GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING"),
        std::make_pair(0x8B9A, "GL_IMPLEMENTATION_COLOR_READ_TYPE"),
        std::make_pair(0x8B9B, "GL_IMPLEMENTATION_COLOR_READ_FORMAT"),
        std::make_pair(0x8B81, "GL_COMPILE_STATUS"),
        std::make_pair(0x8B84, "GL_INFO_LOG_LENGTH"),
        std::make_pair(0x8B88, "GL_SHADER_SOURCE_LENGTH"),
        std::make_pair(0x8DFA, "GL_SHADER_COMPILER"),
        std::make_pair(0x8DF8, "GL_SHADER_BINARY_FORMATS"),
        std::make_pair(0x8DF9, "GL_NUM_SHADER_BINARY_FORMATS"),
        std::make_pair(0x8DF0, "GL_LOW_FLOAT"),
        std::make_pair(0x8DF1, "GL_MEDIUM_FLOAT"),
        std::make_pair(0x8DF2, "GL_HIGH_FLOAT"),
        std::make_pair(0x8DF3, "GL_LOW_INT"),
        std::make_pair(0x8DF4, "GL_MEDIUM_INT"),
        std::make_pair(0x8DF5, "GL_HIGH_INT"),
        std::make_pair(0x8D40, "GL_FRAMEBUFFER"),
        std::make_pair(0x8D41, "GL_RENDERBUFFER"),
        std::make_pair(0x8056, "GL_RGBA4"),
        std::make_pair(0x8057, "GL_RGB5_A1"),
        std::make_pair(0x8D62, "GL_RGB565"),
        std::make_pair(0x81A5, "GL_DEPTH_COMPONENT16"),
        std::make_pair(0x8D48, "GL_STENCIL_INDEX8"),
        std::make_pair(0x8D42, "GL_RENDERBUFFER_WIDTH"),
        std::make_pair(0x8D43, "GL_RENDERBUFFER_HEIGHT"),
        std::make_pair(0x8D44, "GL_RENDERBUFFER_INTERNAL_FORMAT"),
        std::make_pair(0x8D50, "GL_RENDERBUFFER_RED_SIZE"),
        std::make_pair(0x8D51, "GL_RENDERBUFFER_GREEN_SIZE"),
        std::make_pair(0x8D52, "GL_RENDERBUFFER_BLUE_SIZE"),
        std::make_pair(0x8D53, "GL_RENDERBUFFER_ALPHA_SIZE"),
        std::make_pair(0x8D54, "GL_RENDERBUFFER_DEPTH_SIZE"),
        std::make_pair(0x8D55, "GL_RENDERBUFFER_STENCIL_SIZE"),
        std::make_pair(0x8CD0, "GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE"),
        std::make_pair(0x8CD1, "GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME"),
        std::make_pair(0x8CD2, "GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL"),
        std::make_pair(0x8CD3, "GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE"),
        std::make_pair(0x8CE0, "GL_COLOR_ATTACHMENT0"),
        std::make_pair(0x8D00, "GL_DEPTH_ATTACHMENT"),
        std::make_pair(0x8D20, "GL_STENCIL_ATTACHMENT"),
        std::make_pair(0, "GL_NONE"),
        std::make_pair(0x8CD5, "GL_FRAMEBUFFER_COMPLETE"),
        std::make_pair(0x8CD6, "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT"),
        std::make_pair(0x8CD7, "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT"),
        std::make_pair(0x8CD9, "GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS"),
        std::make_pair(0x8CDD, "GL_FRAMEBUFFER_UNSUPPORTED"),
        std::make_pair(0x8CA6, "GL_FRAMEBUFFER_BINDING"),
        std::make_pair(0x8CA7, "GL_RENDERBUFFER_BINDING"),
        std::make_pair(0x84E8, "GL_MAX_RENDERBUFFER_SIZE"),
        std::make_pair(0x0506, "GL_INVALID_FRAMEBUFFER_OPERATION"),

        // these are extra added by me
        std::make_pair(0x84f9, "GL_DEPTH_STENCIL"),
        std::make_pair(0x821A, "GL_DEPTH_STENCIL_ATTACHMENT")
};

static bool glesHasType(int name)
{
    return (gles_types.find( name ) != gles_types.end());
}

static std::string glesType(int name)
{
    auto it = gles_types.find( name );

    if (it != gles_types.end())
        return it->second;

    std::stringstream stream;
    stream << "0x" << std::setfill ('0') << std::setw(4)  <<  std::hex << name << "(whats this?)";

    assert(0);

    return stream.str();
}

#endif //BUILD_ALL_GLES_INCLUDES_HPP
