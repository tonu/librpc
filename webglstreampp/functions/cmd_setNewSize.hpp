//
// Created by Tonu Jaansoo on 26/08/16.
//

#ifndef BUILD_ALL_SETNEWSIZE_HPP
#define BUILD_ALL_SETNEWSIZE_HPP


#ifndef BUILD_SERVER
typedef void setNewSize_t(int16_t width, int16_t height);
#endif

#include "cmd_includes.hpp"
#include "../server.hpp"

namespace cmd_setNewSize
{
    static void client_out(FIFO &fifo, int16_t width, int16_t height)
    {
        DBG_TRAFFIC("(width=" << width << ", height=" << height << ")");

        fifo << arg<int16_t>(width);
        fifo << arg<int16_t>(height);

    }

    static void client_in(FIFO &)
    {

    }

    static void serv_in(Server *const server, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<int16_t> width;
        arg<int16_t> height;

        fifoIn >> width >> height;

#ifndef BUILD_SERVER
        setNewSize_t *s = (setNewSize_t*)server->callback("setNewSize");

        if (s)
            s(width.val, height.val);
#endif
    }

    static bool returns() { return false; }

}

#endif //BUILD_ALL_SETNEWSIZE_HPP
