#ifndef CMD_GLGENFRAMEBUFFERS_HPP
#define CMD_GLGENFRAMEBUFFERS_HPP
//typedef void          (*glGenFramebuffers_ptr) (GLsizei n, GLuint* framebuffers);


#include "cmd_includes.hpp"

namespace cmd_glGenFramebuffers
{
static void client_out(FIFO &fifo, GLsizei n, GLuint* framebuffers)
{
    DBG_TRAFFIC("n=" << n << ", buffers=...)");

    fifo << arg<GLsizei>(n);
    fifo << arg<buffer<GLuint>>(buffer<GLuint>(framebuffers, n));
}

static void client_in(FIFO &fifo, GLuint* framebuffers)
{
    arg<buffer<GLuint>> f;

    fifo >> f;

    memcpy(framebuffers, f.val._ptr.get(), f.val._sz * sizeof(GLuint));

    DBG_TRAFFIC(" returned: buffers[0]=" << framebuffers[0]);
}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
{
    arg<GLsizei> n;
    arg<buffer<GLuint>> framebuffers;

    fifoIn >> n >> framebuffers;
#ifdef BUILD_SERVER
    glGenFramebuffers(n.val, framebuffers.val._ptr.get());
#endif

    fifoOut << framebuffers;
}

static bool returns() { return true; }
}

#endif // CMD_GLGENFRAMEBUFFERS_HPP
