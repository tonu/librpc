//
// Created by Tonu Jaansoo on 19/08/16.
//

#ifndef BUILD_ALL_CMD_GLLINEWIDTH_H
#define BUILD_ALL_CMD_GLLINEWIDTH_H

//GL_APICALL void GL_APIENTRY glLineWidth (GLfloat width);

#include "cmd_includes.hpp"

namespace cmd_glLineWidth
{
    static void client_out(FIFO &fifo, GLfloat width)
{
    DBG_TRAFFIC("(width=" << width << ")");

    fifo << arg<GLfloat>(width);
}

static void client_in(FIFO &fifo)
{

}

static void serv_in(Server * const, FIFO &fifoIn, FIFO &)
{
    arg<GLfloat> width;

    fifoIn >> width;

#ifdef BUILD_SERVER
    glLineWidth(width.val);
#endif
}

static bool returns() { return false; }
}



#endif //BUILD_ALL_CMD_GLLINEWIDTH_H
