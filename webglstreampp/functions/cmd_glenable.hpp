#ifndef CMD_GLENABLE_HPP
#define CMD_GLENABLE_HPP
//typedef void          (*glEnable_ptr) (GLenum cap);

#include "cmd_includes.hpp"

namespace cmd_glEnable
{
    static void client_out(FIFO &fifo, GLenum cap)
    {
        DBG_TRAFFIC("(cap=" << glesType(cap) << ")");
        fifo << arg<GLenum>(cap);
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
    {
        arg<GLenum> cap;

        fifoIn >> cap;

#ifdef BUILD_SERVER
        glEnable(cap.val);
#endif
    }

    static bool returns() { return false; }
}
#endif // CMD_GLENABLE_HPP
