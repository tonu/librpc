//
// Created by Tonu Jaansoo on 18/08/16.
//

#ifndef BUILD_ALL_CMD_GLBINDFRAMEBUFFER_HPP
#define BUILD_ALL_CMD_GLBINDFRAMEBUFFER_HPP
//    GL_APICALL void GL_APIENTRY glBindFramebuffer (GLenum target, GLuint framebuffer);

#ifdef BUILD_SERVER
#include "WebCtx.hpp"
#endif

#include "cmd_includes.hpp"

namespace cmd_glBindFramebuffer
{
    static void client_out(FIFO &fifo, GLenum target, GLuint framebuffer)
    {
        DBG_TRAFFIC("(target=" << glesType(target) << ", framebuffer=" << framebuffer << ")");

        fifo << arg<GLenum>(target) << arg<GLuint>(framebuffer);
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLenum> target;
        arg<GLuint> framebuffer;

        fifoIn >> target >> framebuffer;

#ifdef BUILD_SERVER
        // when unbinding, then bind to RTT FBO instead
        glBindFramebuffer(target.val, (framebuffer.val ? framebuffer.val : WebCtx::get()->rttFBO ));
#endif
    }

    static bool returns() { return false; }
}

#endif //BUILD_ALL_CMD_GLBINDFRAMEBUFFER_HPP
