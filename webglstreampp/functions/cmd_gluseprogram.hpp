#ifndef CMD_GLUSEPROGRAM_HPP
#define CMD_GLUSEPROGRAM_HPP

//typedef void          (*glUseProgram_ptr) (GLuint program);


#include "cmd_includes.hpp"

namespace cmd_glUseProgram
{
    static void client_out(FIFO &fifo, GLuint program)
    {
        DBG_TRAFFIC("(program="<< program << ")");

        fifo << arg<GLuint>(program);
    }

    static void client_in(FIFO &)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
    {
        arg<GLuint> program;

        fifoIn >> program;

#ifdef BUILD_SERVER
        glUseProgram(program.val);
#endif
    }

    static bool returns() { return false; }

}
#endif // CMD_GLUSEPROGRAM_HPP
