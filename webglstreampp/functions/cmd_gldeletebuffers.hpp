//
// Created by Tonu Jaansoo on 19/08/16.
//

#ifndef BUILD_ALL_CMD_GLDELETEBUFFERS_HPP
#define BUILD_ALL_CMD_GLDELETEBUFFERS_HPP

//GL_APICALL void GL_APIENTRY glDeleteBuffers (GLsizei n, const GLuint *buffers);

#include "cmd_includes.hpp"

namespace cmd_glDeleteBuffers
{
    static void client_out(FIFO &fifo, GLsizei n, const GLuint *buffers)
{
    DBG_TRAFFIC("(n=" << n << ", buffers=...");

    fifo << arg<GLsizei>(n);
    fifo << arg<buffer<GLuint>>(buffer<GLuint>(buffers, n));
}

static void client_in(FIFO &fifo)
{

}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
{
    arg<GLsizei> n;
    arg<buffer<GLuint>> buffers;

    fifoIn >> n >> buffers;

#ifdef BUILD_SERVER
    glDeleteBuffers(n.val, buffers.val._ptr.get());
#endif
}

static bool returns() { return false; }
}



#endif //BUILD_ALL_CMD_GLDELETEBUFFERS_HPP
