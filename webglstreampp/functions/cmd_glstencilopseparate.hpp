//
// Created by Tonu Jaansoo on 26/08/16.
//

#ifndef BUILD_ALL_CMD_GLSTENCILOPSEPARATE_HPP
#define BUILD_ALL_CMD_GLSTENCILOPSEPARATE_HPP
//    GL_APICALL void GL_APIENTRY glStencilOpSeparate (GLenum face, GLenum sfail, GLenum dpfail, GLenum dppass);

// XXXX

#include "cmd_includes.hpp"

namespace cmd_glStencilOpSeparate
{
    static void client_out(FIFO &fifo, GLenum face, GLenum sfail, GLenum dpfail, GLenum dppass)
    {
        DBG_TRAFFIC("(face=" << glesType(face) << ", sfail=" << glesType(sfail) << ", dpfail=" <<glesType(dpfail) << ", dppass=" << glesType(dppass) << ")");

        fifo << arg<GLenum>(face) << arg<GLenum>(sfail) << arg<GLenum>(dpfail) << arg<GLenum>(dppass);

    }

    static void client_in(FIFO &)
    {

    }

// what to execute on server
    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLenum> face;
        arg<GLenum> sfail;
        arg<GLenum> dpfail;
        arg<GLenum> dppass;
        fifoIn >> face >> sfail >> dpfail >> dppass;

#ifdef BUILD_SERVER
        glStencilOpSeparate(face.val, sfail.val, dpfail.val, dppass.val);
#endif
    }

    static bool returns() { return false; }

}
#endif //BUILD_ALL_CMD_GLSTENCILOPSEPARATE_HPP
