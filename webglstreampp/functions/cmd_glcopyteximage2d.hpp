#ifndef CMD_GLCOPYTEXIMAGE2D_HPP
#define CMD_GLCOPYTEXIMAGE2D_HPP
//typedef void          (*glCopyTexImage2D_ptr) (GLenum target, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLsizei height, GLint border);

#include "cmd_includes.hpp"

namespace cmd_glCopyTexImage2D
{
static void client_out(FIFO &fifo, GLenum target, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLsizei height, GLint border)
{
    DBG_TRAFFIC("(target=" << glesType(target) << ", level=" << level << ", internalFormat=" << internalformat << ", x=" << x << ", y=" << y << ", width=" << width << ", height=" << height << ", border=" << border << ")" );
    fifo << arg<GLenum>(target);
    fifo << arg<GLint>(level);
    fifo << arg<GLenum>(internalformat);
    fifo << arg<GLint>(x);
    fifo << arg<GLint>(y);
    fifo << arg<GLsizei>(width);
    fifo << arg<GLsizei>(height);
    fifo << arg<GLint>(border);
}

static void client_in(FIFO &fifo)
{

}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
{
    arg<GLenum> target;
    arg<GLint> level;
    arg<GLenum> internalformat;
    arg<GLint> x;
    arg<GLint> y;
    arg<GLsizei> width;
    arg<GLsizei> height;
    arg<GLint> border;

    fifoIn >> target >> level >> internalformat >> x >> y >> width >> height >> border;
#ifdef BUILD_SERVER
    glCopyTexImage2D(target.val, level.val, internalformat.val, x.val, y.val, width.val, height.val, border.val);
#endif
}

static bool returns() { return false; }
}
#endif // CMD_GLCOPYTEXIMAGE2D_HPP
