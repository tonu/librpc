//
// Created by Tonu Jaansoo on 23/08/16.
//

#ifndef BUILD_ALL_CMD_GLPOLYGONOFFSET_HPP
#define BUILD_ALL_CMD_GLPOLYGONOFFSET_HPP

//     GL_APICALL void GL_APIENTRY glPolygonOffset (GLfloat factor, GLfloat units);


#include "cmd_includes.hpp"

namespace cmd_glPolygonOffset
{
    static void client_out(FIFO &fifo, GLfloat factor, GLfloat units)
    {
        DBG_TRAFFIC("(factor=" << factor << ", units=" << units <<")");

        fifo << arg<GLfloat>(factor);
        fifo << arg<GLfloat>(units);
    }

    static void client_in(FIFO &)
    {

    }

// what to execute on server
    static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
    {
        arg<GLfloat> factor;
        arg<GLfloat> units;

        fifoIn >> factor >> units;

#ifdef BUILD_SERVER
        glPolygonOffset(factor.val, units.val);
#endif
    }

    static bool returns() { return false; }

}
#endif //BUILD_ALL_CMD_GLPOLYGONOFFSET_HPP
