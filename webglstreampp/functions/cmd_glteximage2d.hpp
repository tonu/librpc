#ifndef CMD_GLTEXIMAGE2D_HPP
#define CMD_GLTEXIMAGE2D_HPP
//typedef void          (*glTexImage2D_ptr) (GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const GLvoid* pixels);


#include "cmd_includes.hpp"

namespace cmd_glTexImage2D
{
static void client_out(FIFO &fifo, GLenum target, GLint level, GLint internalformat,
                       GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const GLvoid* pixels)
{
    DBG_TRAFFIC("(target=" << glesType(target) << ", level=" << level << ", internalformat=" << glesType(internalformat) \
    << ", width=" << width << ", height=" << height << ", border=" << border << ", format=" << glesType(format) << ", type=" << glesType(type) << ", pixels=" << uint64_t(pixels) << ")");

    //// fixme how to calc size of buffer?

    int perCanBits = typeBits(type);

    switch(format)
    {
    case  0x1903: // GL_RED_EXT FIXME
        internalformat = format = 0x1903;
        break;
    case GL_ALPHA:
    case GL_LUMINANCE:
    case GL_LUMINANCE_ALPHA:
    case GL_DEPTH_COMPONENT:
        break;
    case GL_RGB:
        perCanBits *= 3;
        break;
    case GL_RGBA:
        perCanBits *= 4;
        break;
    default:
    DBG_TRAFFIC(std::hex << " fmt:" << format << "\n");
        throw "unknown fmt";
    }


//FIXME... check if binding to gpu address not client side

    fifo << arg<GLenum>(target);
    fifo << arg<GLint>(level);
    fifo << arg<GLint>(internalformat);
    fifo << arg<GLsizei>(width);
    fifo << arg<GLsizei>(height);
    fifo << arg<GLint>(border);
    fifo << arg<GLenum>(format);
    fifo << arg<GLenum>(type);
    fifo << arg<buffer<char>>( buffer<char>(pixels, width * height * (perCanBits >> 3))) ;
}

static void client_in(FIFO &fifo)
{

}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
{
    arg<GLenum> target;
    arg<GLint> level;
    arg<GLint> internalformat;
    arg<GLsizei> width;
    arg<GLsizei> height;
    arg<GLint> border;
    arg<GLenum> format;
    arg<GLenum> type;
    arg<buffer<char>> pixels;

    fifoIn >> target >> level >> internalformat >> width >> height >> border >> format >> type >> pixels;


#ifdef BUILD_SERVER
    glTexImage2D(target.val, level.val, internalformat.val, width.val, height.val, border.val, format.val, type.val, pixels.val._ptr.get());
#endif
}

static bool returns() { return false; }
}

#endif // CMD_GLTEXIMAGE2D_HPP
