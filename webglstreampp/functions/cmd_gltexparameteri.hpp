#ifndef CMD_GLTEXPARAMETERI_HPP
#define CMD_GLTEXPARAMETERI_HPP
//typedef void          (*glTexParameteri_ptr) (GLenum target, GLenum pname, GLint param);

#include "cmd_includes.hpp"

namespace cmd_glTexParameteri
{
    static void client_out(FIFO &fifo, GLenum target, GLenum pname, GLint param)
    {
        if (!glesHasType(pname))
        {
            DBG_TRAFFIC("(wont do: unknown enum:" << pname << ")");
            return;
        }

        DBG_TRAFFIC("(target=" << glesType(target) << ", pname=" << glesType(pname) << ", param=" << glesType(param) << ")");

        fifo << arg<GLenum>(target);
        fifo << arg<GLenum>(pname);
        fifo << arg<GLenum>(param);
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
    {
        arg<GLenum> target;
        arg<GLenum> pname;
        arg<GLenum> param;

        fifoIn >> target >> pname >> param;

#ifdef BUILD_SERVER
//        if (pname == GL_TEXTURE_MAG_FILTER)
//
//        if (pname == GL_TEXTURE_MIN_FILTER)
//
        glTexParameteri(target.val, pname.val, param.val);
#endif
    }

    static bool returns() { return false; }
}

#endif // CMD_GLTEXPARAMETERI_HPP
