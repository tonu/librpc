#ifndef CMD_GLDELETEPROGRAM_HPP
#define CMD_GLDELETEPROGRAM_HPP
//typedef void          (*glDeleteProgram_ptr) (GLuint program);


#include "cmd_includes.hpp"

namespace cmd_glDeleteProgram
{
    static void client_out(FIFO &fifo, GLuint program)
    {
        fifo << arg<GLuint>(program);
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLuint> program;
        fifoIn >> program;

#ifdef BUILD_SERVER
        glDeleteProgram(program.val);
#endif
    }

    static bool returns() { return false; }
}


#endif // CMD_GLDELETEPROGRAM_HPP
