//
// Created by Tonu Jaansoo on 18/08/16.
//

#ifndef BUILD_ALL_CMD_GLFRAMEBUFFERTEXTURE2D_HPP
#define BUILD_ALL_CMD_GLFRAMEBUFFERTEXTURE2D_HPP
//    GL_APICALL void GL_APIENTRY glFramebufferTexture2D (GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level);


#include "cmd_includes.hpp"

namespace cmd_glFramebufferTexture2D
{
    static void client_out(FIFO &fifo, GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level)
{
    DBG_TRAFFIC("(target=" << glesType(target) << ", attachment=" << glesType(attachment) << ", textarget=" << glesType(textarget) << ", texure=" << texture << ", level=" << level << ")");

    fifo << arg<GLenum>(target);
    fifo << arg<GLenum>(attachment);
    fifo << arg<GLenum>(textarget);
    fifo << arg<GLuint>(texture);
    fifo << arg<GLint>(level);
}

static void client_in(FIFO &fifo)
{

}

static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
{
    arg<GLenum> target;
    arg<GLenum> attachment;
    arg<GLenum> textarget;
    arg<GLuint> texture;
    arg<GLint> level;

    fifoIn >> target >> attachment >> textarget >> texture >> level;

#ifdef BUILD_SERVER
    glFramebufferTexture2D(target.val, attachment.val, textarget.val, texture.val, level.val);
#endif
}

static bool returns() { return false; }
}
#endif //BUILD_ALL_CMD_GLFRAMEBUFFERTEXTURE2D_HPP
