#ifndef CMD_GLCLEARSTENCIL_HPP
#define CMD_GLCLEARSTENCIL_HPP
//typedef void          (*glClearStencil_ptr) (GLint s);
#include "cmd_includes.hpp"

namespace cmd_glClearStencil
{
    static void client_out(FIFO &fifo, GLint s)
    {
        fifo << arg<GLint>(s);
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLint> s;
        fifoIn >> s;

#ifdef BUILD_SERVER
        glClearStencil(s.val);
#endif
    }

    static bool returns() { return false; }
}
#endif // CMD_GLCLEARSTENCIL_HPP
