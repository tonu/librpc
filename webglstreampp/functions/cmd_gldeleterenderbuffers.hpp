//
// Created by Tonu Jaansoo on 20/08/16.
//

#ifndef BUILD_ALL_CMD_GLDELETERENDERBUFFERS_HPP
#define BUILD_ALL_CMD_GLDELETERENDERBUFFERS_HPP
//    GL_APICALL void GL_APIENTRY glDeleteRenderbuffers (GLsizei n, const GLuint *renderbuffers);

#include "cmd_includes.hpp"

namespace cmd_glDeleteRenderbuffers
{
    static void client_out(FIFO &fifo, GLsizei n, const GLuint *buffers)
    {
        DBG_TRAFFIC("(n=" << n << ", renderbuffers=...");

        fifo << arg<GLsizei>(n);
        fifo << arg<buffer<GLuint>>(buffer<GLuint>(buffers, n));
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
    {
        arg<GLsizei>        n;
        arg<buffer<GLuint>> buffers;

        fifoIn >> n >> buffers;

#ifdef BUILD_SERVER
        glDeleteRenderbuffers(n.val, buffers.val._ptr.get());
#endif
    }

    static bool returns() { return false; }
};

#endif //BUILD_ALL_CMD_GLDELETERENDERBUFFERS_HPP
