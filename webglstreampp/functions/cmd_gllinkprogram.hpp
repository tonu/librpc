#ifndef CMD_GLLINKPROGRAM_H
#define CMD_GLLINKPROGRAM_H

//typedef void          (*glLinkProgram_ptr) (GLuint program);

#include "cmd_includes.hpp"

namespace cmd_glLinkProgram
{
    static void client_out(FIFO &fifo, GLuint program)
    {
        fifo << arg<GLuint>(program);

        DBG_TRAFFIC("(program=" << program << ")");
    }

    static void client_in(FIFO &)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
    {
        arg<GLuint> program;

        fifoIn >> program;

 //        glFlush();
#ifdef BUILD_SERVER
        glLinkProgram(program.val);
#endif
    }

    static bool returns() { return false; }

}

#endif // CMD_GLLINKPROGRAM_H
