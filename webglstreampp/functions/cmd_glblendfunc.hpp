#ifndef CMD_GLBLENDFUNC_HPP
#define CMD_GLBLENDFUNC_HPP
//typedef void          (*glBlendFunc_ptr) (GLenum sfactor, GLenum dfactor);

#include "cmd_includes.hpp"

namespace cmd_glBlendFunc
{
    static void client_out(FIFO &fifo, GLenum sfactor, GLenum dfactor)
    {
        DBG_TRAFFIC("(sfactor=" << glesType(sfactor) << ", dfactor=" << glesType(dfactor) << ")");

        fifo << arg<GLenum>(sfactor) << arg<GLenum>(dfactor);
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
    {
        arg<GLenum> sfactor;
        arg<GLenum> dfactor;

        fifoIn >> sfactor >> dfactor;

#ifdef BUILD_SERVER
        glBlendFunc(sfactor.val, dfactor.val);
#endif
    }

    static bool returns() { return false; }
}
#endif // CMD_GLBLENDFUNC_HPP
