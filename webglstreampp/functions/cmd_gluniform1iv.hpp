//
// Created by Tonu Jaansoo on 20/08/16.
//

#ifndef BUILD_ALL_CMD_GLUNIFORM1IV_HPP
#define BUILD_ALL_CMD_GLUNIFORM1IV_HPP
//     GL_APICALL void GL_APIENTRY glUniform1iv (GLint location, GLsizei count, const GLint *value);


#include "cmd_includes.hpp"

namespace cmd_glUniform1iv
{
    static void client_out(FIFO &fifo, GLint location, GLsizei count, const GLint *value)
    {
        DBG_TRAFFIC("(location=" << location << ", count=" << count << ", value=...) v[0] =" << value[0]);

        fifo << arg<GLint>(location);
        fifo << arg<GLsizei>(count);
        fifo << arg<buffer<GLint>>(buffer<GLint>(value, 1 * count));
    }

    static void client_in(FIFO &)
    {

    }

// what to execute on server
    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLint> location;
        arg<GLsizei> count;
        arg<buffer<GLint>> v;

        fifoIn >> location >> count >> v;

#ifdef BUILD_SERVER
        glUniform1iv(location.val, count.val, v.val._ptr.get());
#endif
    }

    static bool returns() { return false; }

}
#endif //BUILD_ALL_CMD_GLUNIFORM1IV_HPP
