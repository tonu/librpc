//
// Created by Tonu Jaansoo on 23/08/16.
//

#ifndef BUILD_ALL_CMD_GLUNIFORM1F_HPP
#define BUILD_ALL_CMD_GLUNIFORM1F_HPP
//     GL_APICALL void GL_APIENTRY glUniform1f (GLint location, GLfloat v0);


#include "cmd_includes.hpp"

namespace cmd_glUniform1f
{
    static void client_out(FIFO &fifo, GLint location, GLfloat v0)
    {
        DBG_TRAFFIC("(location=" << location << ", v0=" << v0 << ")");

        fifo << arg<GLint>(location);
        fifo << arg<GLfloat>(v0);
    }

    static void client_in(FIFO &)
    {

    }

// what to execute on server
    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLint> location;
        arg<GLfloat> v0;

        fifoIn >> location >> v0;

#ifdef BUILD_SERVER
        glUniform1f(location.val, v0.val);
#endif
    }

    static bool returns() { return false; }

}

#endif //BUILD_ALL_CMD_GLUNIFORM1F_HPP
