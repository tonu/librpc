//
// Created by Tonu Jaansoo on 20/08/16.
//

#ifndef BUILD_ALL_CMD_GLGETBOOLEANV_HPP
#define BUILD_ALL_CMD_GLGETBOOLEANV_HPP
//     GL_APICALL void GL_APIENTRY glGetBooleanv (GLenum pname, GLboolean *data);

#include "cmd_includes.hpp"

namespace cmd_glGetBooleanv
{
    static void client_out(FIFO &fifo, GLenum pname)
    {
        if (!glesHasType(pname))
        {
            DBG_TRAFFIC("(wont do: unknown enum:" << pname << ")");
            return;
        }

        DBG_TRAFFIC("(pname=" << glesType(pname) <<  ")");

        fifo << arg<GLenum>(pname);
    }

    static void client_in(FIFO &fifo, GLboolean *data)
    {
        arg<GLboolean> value;
        fifo >> value;

        *data = value.val;

        DBG_TRAFFIC(" returned=" << int(value.val));
    }

// what to execute on server
    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLenum> pname;
        fifoIn >> pname;

        arg<GLboolean> value;

#ifdef BUILD_SERVER
        glGetBooleanv(pname.val, &value.val);
#endif

        fifoOut << value;
    }


    static bool returns() { return true; }

}
#endif //BUILD_ALL_CMD_GLGETBOOLEANV_HPP
