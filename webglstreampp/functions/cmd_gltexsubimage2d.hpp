#ifndef CMD_GLTEXSUBIMAGE2D_HPP
#define CMD_GLTEXSUBIMAGE2D_HPP
//    GL_APICALL void GL_APIENTRY glTexSubImage2D (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const void *pixels);

#include "cmd_includes.hpp"

namespace cmd_glTexSubImage2D
{
    static void client_out(FIFO &fifo, GLenum target, GLint level, GLint xoffset, GLint yoffset,
                           GLsizei width, GLsizei height, GLenum format, GLenum type, const void *pixels)
    {
        DBG_TRAFFIC("(target=" << glesType(target) << ", level=" << level \
 << ", xoffset=" << xoffset << ", yoffset=" << yoffset << ", width=" << width << ", height=" << height << ", type="
                               << glesType(type) << ", pixels=" << uint64_t(pixels) << ")");


        //// fixme !! how to calc size of buffer????? this is hack
        int perCanBits = typeBits(type);

        switch (format)
        {
        case 0x1903: // GL_RED_EXT FIXME
            format = 0x1903;
            break;
        case GL_ALPHA:
        case GL_LUMINANCE:
        case GL_LUMINANCE_ALPHA:
        case GL_DEPTH_COMPONENT:break;
        case GL_RGB:perCanBits *= 3;
            break;
        case GL_RGBA:perCanBits *= 4;
            break;
        default:
        DBG_TRAFFIC(std::hex << " fmt:" << format << "\n");
            throw "unknown fmt";
        }

        fifo << arg<GLenum>(target);
        fifo << arg<GLint>(level);
        fifo << arg<GLint>(xoffset);
        fifo << arg<GLint>(yoffset);
        fifo << arg<GLsizei>(width);
        fifo << arg<GLsizei>(height);
        fifo << arg<GLenum>(format);
        fifo << arg<GLenum>(type);
        fifo << arg<buffer<char>>(buffer<char>(pixels, width * height * (perCanBits >> 3)));
    }

    static void client_in(FIFO &fifo)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &fifoOut)
    {
        arg<GLenum>       target;
        arg<GLint>        level;
        arg<GLint>        xoffset;
        arg<GLint>        yoffset;
        arg<GLsizei>      width;
        arg<GLsizei>      height;
        arg<GLenum>       format;
        arg<GLenum>       type;
        arg<buffer<char>> pixels;

        fifoIn >> target >> level >> xoffset >> yoffset >> width >> height >> format >> type >> pixels;

#ifdef BUILD_SERVER
        glTexSubImage2D(target.val, level.val, xoffset.val, yoffset.val, width.val, height.val, format.val, type.val, pixels.val._ptr.get());
#endif
    }

    static bool returns() { return false; }
}


#endif // CMD_GLTEXSUBIMAGE2D_HPP
