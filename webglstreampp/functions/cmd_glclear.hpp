#ifndef CMD_GLCLEAR_HPP
#define CMD_GLCLEAR_HPP

#include "cmd_includes.hpp"

namespace cmd_glClear
{
    static void client_out(FIFO &fifo, GLbitfield mask)
    {
        DBG_TRAFFIC("(mask=");
        if (mask & GL_COLOR_BUFFER_BIT) DBG_TRAFFIC("GL_COLOR_BUFFER_BIT ");
        if (mask & GL_DEPTH_BUFFER_BIT) DBG_TRAFFIC("GL_DEPTH_BUFFER_BIT ");
        if (mask & GL_COLOR_BUFFER_BIT) DBG_TRAFFIC("GL_STENCIL_BUFFER_BIT ");
        DBG_TRAFFIC(")");

        fifo << arg<uint32_t>(mask);
    }

    static void client_in(FIFO &)
    {

    }

    static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
    {
        arg<uint32_t> mask;

        fifoIn >> mask;

#ifdef BUILD_SERVER
         //mask.val &= ~GL_COLOR_BUFFER_BIT;

        glClear(mask.val); // fixme
#endif
    }

    static bool returns() { return false; }

}


#endif // CMD_GLCLEAR_HPP
