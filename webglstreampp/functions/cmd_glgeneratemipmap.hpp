//
// Created by Tonu Jaansoo on 20/08/16.
//

#ifndef BUILD_ALL_CMD_GLGENERATEMIPMAP_HPP
#define BUILD_ALL_CMD_GLGENERATEMIPMAP_HPP

//     GL_APICALL void GL_APIENTRY glGenerateMipmap (GLenum target);

#include "cmd_includes.hpp"

namespace cmd_glGenerateMipmap
{
    static void client_out(FIFO &fifo, GLenum target)
    {
        DBG_TRAFFIC("(target=" << glesType(target) << ")");

        fifo << arg<GLenum>(target);

    }

    static void client_in(FIFO &)
    {

    }

// what to execute on server
    static void serv_in(Server *const, FIFO &fifoIn, FIFO &)
    {
        arg<GLenum> target;

        fifoIn >> target;

#ifdef BUILD_SERVER
        glGenerateMipmap(target.val);
#endif
    }

    static bool returns() { return false; }

}
#endif //BUILD_ALL_CMD_GLGENERATEMIPMAP_HPP
