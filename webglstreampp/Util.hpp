//
// Created by Tonu Jaansoo on 24/08/16.
//

#ifndef BUILD_ALL_UTIL_HPP
#define BUILD_ALL_UTIL_HPP

#define GL_GLEXT_PROTOTYPES
#include <GLES2/gl2.h>

#include "functions/gles_names.hpp"

#include <iostream>
class Util
{

};
#include <mutex>

extern std::mutex gPm;// move all below somewhere in main dir

//#define DEBUG_ALL_IO

#define  DBG_TRAFFIC(n) {}
//#define DBG_TRAFFIC(n) {    std::lock_guard<std::mutex> g(gPm); std::cerr << n; }

//#define DBG_TRAFFIC_DATA(n) DBG_TRAFFIC(n)
#define DBG_TRAFFIC_DATA(n)

//move from here
//#ifndef NDEBUG
#   define ASSERT(condition, message) \
    do { \
        if (! (condition)) { \
            std::cerr << "Assertion `" #condition "` failed in " << __FILE__ \
                      << " line " << __LINE__ << ": " << message << std::endl; \
            std::terminate(); \
        } \
    } while (false)
//#else
//#   define ASSERT(condition, message) do { } while (false)
//#endif

//fixme ... replace some places in code with this
inline int typeBits(GLenum type)
{
    switch(type)
    {
    case GL_FLOAT:
    case GL_INT:
    case GL_UNSIGNED_INT:
        return 32;
    case GL_SHORT:
    case GL_UNSIGNED_SHORT:
        return 16;;
    case GL_BYTE:
    case GL_UNSIGNED_BYTE:
        return 8;
    default:
        throw "unknown type";
    }
    return -1;
}

// error checking
#define enum_case(str,c)\
    case str:\
    DBG_TRAFFIC( "------ ERROR cmd:" << gles_names[c] << file << ", " << line << ": " #str  "\n")\
    break;


static void gl_error_check(std::string file, int line, int c)
{
    GLenum err = glGetError();

    switch (err) {
    enum_case(GL_INVALID_ENUM,c)
    enum_case(GL_INVALID_VALUE,c)
    enum_case(GL_INVALID_OPERATION,c)
    enum_case(GL_INVALID_FRAMEBUFFER_OPERATION,c)
    enum_case(GL_OUT_OF_MEMORY,c)
    default:
//        std::cerr << "ok";
        break;
    }
}

#define GL_ERR_CHECK(c) \
    do {\
    gl_error_check(__FILE__, __LINE__, c); \
    } while(0)



#endif //BUILD_ALL_UTIL_HPP
