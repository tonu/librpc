#include "client.hpp"
#include "functions/gles_access.hpp"
#include <iostream>

Client staticInstance;

Client::Client()
{
}

Client *Client::get()
{
    return &staticInstance;
}


void Client::init(std::shared_ptr<FIFO> fifoOut, std::shared_ptr<FIFO> fifoIn)
{
    _fifoOut = fifoOut;
    _fifoIn = fifoIn;
    _run = true;

std::cerr << "Client init------------------------------------\n";
    _procs["glActiveTexture"]                       = (void *) glActiveTexture;
    _procs["glAttachShader"]                        = (void *) glAttachShader;
    _procs["glBindAttribLocation"]                  = (void *) glBindAttribLocation;
    _procs["glBindBuffer"]                          = (void *) glBindBuffer;
    _procs["glBindFramebuffer"]                     = (void *) glBindFramebuffer;
    _procs["glBindRenderbuffer"]                    = (void *) glBindRenderbuffer;
    _procs["glBindTexture"]                         = (void *) glBindTexture;
    _procs["glBlendColor"]                          = (void *) glBlendColor;
    _procs["glBlendEquation"]                       = (void *) glBlendEquation;
    _procs["glBlendEquationSeparate"]               = (void *) glBlendEquationSeparate;
    _procs["glBlendFunc"]                           = (void *) glBlendFunc;
    _procs["glBlendFuncSeparate"]                   = (void *) glBlendFuncSeparate;
    _procs["glBufferData"]                          = (void *) glBufferData;
    _procs["glBufferSubData"]                       = (void *) glBufferSubData;
    _procs["glCheckFramebufferStatus"]              = (void *) glCheckFramebufferStatus;
    _procs["glClear"]                               = (void *) glClear;
    _procs["glClearColor"]                          = (void *) glClearColor;
    _procs["glClearDepthf"]                         = (void *) glClearDepthf;
    _procs["glClearStencil"]                        = (void *) glClearStencil;
    _procs["glColorMask"]                           = (void *) glColorMask;
    _procs["glCompileShader"]                       = (void *) glCompileShader;
    _procs["glCompressedTexImage2D"]                = (void *) glCompressedTexImage2D;
    _procs["glCompressedTexSubImage2D"]             = (void *) glCompressedTexSubImage2D;
    _procs["glCopyTexImage2D"]                      = (void *) glCopyTexImage2D;
    _procs["glCopyTexSubImage2D"]                   = (void *) glCopyTexSubImage2D;
    _procs["glCreateProgram"]                       = (void *) glCreateProgram;
    _procs["glCreateShader"]                        = (void *) glCreateShader;
    _procs["glCullFace"]                            = (void *) glCullFace;
    _procs["glDeleteBuffers"]                       = (void *) glDeleteBuffers;
    _procs["glDeleteFramebuffers"]                  = (void *) glDeleteFramebuffers;
    _procs["glDeleteProgram"]                       = (void *) glDeleteProgram;
    _procs["glDeleteRenderbuffers"]                 = (void *) glDeleteRenderbuffers;
    _procs["glDeleteShader"]                        = (void *) glDeleteShader;
    _procs["glDeleteTextures"]                      = (void *) glDeleteTextures;
    _procs["glDepthFunc"]                           = (void *) glDepthFunc;
    _procs["glDepthMask"]                           = (void *) glDepthMask;
    _procs["glDepthRangef"]                         = (void *) glDepthRangef;
    _procs["glDetachShader"]                        = (void *) glDetachShader;
    _procs["glDisable"]                             = (void *) glDisable;
    _procs["glDisableVertexAttribArray"]            = (void *) glDisableVertexAttribArray;
    _procs["glDrawArrays"]                          = (void *) glDrawArrays;
    _procs["glDrawElements"]                        = (void *) glDrawElements;
    _procs["glEnable"]                              = (void *) glEnable;
    _procs["glEnableVertexAttribArray"]             = (void *) glEnableVertexAttribArray;
    _procs["glFinish"]                              = (void *) glFinish;
    _procs["glFlush"]                               = (void *) glFlush;
    _procs["glFramebufferRenderbuffer"]             = (void *) glFramebufferRenderbuffer;
    _procs["glFramebufferTexture2D"]                = (void *) glFramebufferTexture2D;
    _procs["glFrontFace"]                           = (void *) glFrontFace;
    _procs["glGenBuffers"]                          = (void *) glGenBuffers;
    _procs["glGenerateMipmap"]                      = (void *) glGenerateMipmap;
    _procs["glGenFramebuffers"]                     = (void *) glGenFramebuffers;
    _procs["glGenRenderbuffers"]                    = (void *) glGenRenderbuffers;
    _procs["glGenTextures"]                         = (void *) glGenTextures;
    _procs["glGetActiveAttrib"]                     = (void *) glGetActiveAttrib;
    _procs["glGetActiveUniform"]                    = (void *) glGetActiveUniform;
    _procs["glGetAttachedShaders"]                  = (void *) glGetAttachedShaders;
    _procs["glGetAttribLocation"]                   = (void *) glGetAttribLocation;
    _procs["glGetBooleanv"]                         = (void *) glGetBooleanv;
    _procs["glGetBufferParameteriv"]                = (void *) glGetBufferParameteriv;
    _procs["glGetError"]                            = (void *) glGetError;
    _procs["glGetFloatv"]                           = (void *) glGetFloatv;
    _procs["glGetFramebufferAttachmentParameteriv"] = (void *) glGetFramebufferAttachmentParameteriv;
    _procs["glGetIntegerv"]                         = (void *) glGetIntegerv;
    _procs["glGetProgramiv"]                        = (void *) glGetProgramiv;
    _procs["glGetProgramInfoLog"]                   = (void *) glGetProgramInfoLog;
    _procs["glGetRenderbufferParameteriv"]          = (void *) glGetRenderbufferParameteriv;
    _procs["glGetShaderiv"]                         = (void *) glGetShaderiv;
    _procs["glGetShaderInfoLog"]                    = (void *) glGetShaderInfoLog;
    _procs["glGetShaderPrecisionFormat"]            = (void *) glGetShaderPrecisionFormat;
    _procs["glGetShaderSource"]                     = (void *) glGetShaderSource;
    _procs["glGetString"]                           = (void *) glGetString;
    _procs["glGetTexParameterfv"]                   = (void *) glGetTexParameterfv;
    _procs["glGetTexParameteriv"]                   = (void *) glGetTexParameteriv;
    _procs["glGetUniformfv"]                        = (void *) glGetUniformfv;
    _procs["glGetUniformiv"]                        = (void *) glGetUniformiv;
    _procs["glGetUniformLocation"]                  = (void *) glGetUniformLocation;
    _procs["glGetVertexAttribfv"]                   = (void *) glGetVertexAttribfv;
    _procs["glGetVertexAttribiv"]                   = (void *) glGetVertexAttribiv;
    _procs["glGetVertexAttribPointerv"]             = (void *) glGetVertexAttribPointerv;
    _procs["glHint"]                                = (void *) glHint;
    _procs["glIsBuffer"]                            = (void *) glIsBuffer;
    _procs["glIsEnabled"]                           = (void *) glIsEnabled;
    _procs["glIsFramebuffer"]                       = (void *) glIsFramebuffer;
    _procs["glIsProgram"]                           = (void *) glIsProgram;
    _procs["glIsRenderbuffer"]                      = (void *) glIsRenderbuffer;
    _procs["glIsShader"]                            = (void *) glIsShader;
    _procs["glIsTexture"]                           = (void *) glIsTexture;
    _procs["glLineWidth"]                           = (void *) glLineWidth;
    _procs["glLinkProgram"]                         = (void *) glLinkProgram;
    _procs["glPixelStorei"]                         = (void *) glPixelStorei;
    _procs["glPolygonOffset"]                       = (void *) glPolygonOffset;
    _procs["glReadPixels"]                          = (void *) glReadPixels;
    _procs["glReleaseShaderCompiler"]               = (void *) glReleaseShaderCompiler;
    _procs["glRenderbufferStorage"]                 = (void *) glRenderbufferStorage;
    _procs["glSampleCoverage"]                      = (void *) glSampleCoverage;
    _procs["glScissor"]                             = (void *) glScissor;
    _procs["glShaderBinary"]                        = (void *) glShaderBinary;
    _procs["glShaderSource"]                        = (void *) glShaderSource;
    _procs["glStencilFunc"]                         = (void *) glStencilFunc;
    _procs["glStencilFuncSeparate"]                 = (void *) glStencilFuncSeparate;
    _procs["glStencilMask"]                         = (void *) glStencilMask;
    _procs["glStencilMaskSeparate"]                 = (void *) glStencilMaskSeparate;
    _procs["glStencilOp"]                           = (void *) glStencilOp;
    _procs["glStencilOpSeparate"]                   = (void *) glStencilOpSeparate;
    _procs["glTexImage2D"]                          = (void *) glTexImage2D;
    _procs["glTexParameterf"]                       = (void *) glTexParameterf;
    _procs["glTexParameterfv"]                      = (void *) glTexParameterfv;
    _procs["glTexParameteri"]                       = (void *) glTexParameteri;
    _procs["glTexParameteriv"]                      = (void *) glTexParameteriv;
    _procs["glTexSubImage2D"]                       = (void *) glTexSubImage2D;
    _procs["glUniform1f"]                           = (void *) glUniform1f;
    _procs["glUniform1fv"]                          = (void *) glUniform1fv;
    _procs["glUniform1i"]                           = (void *) glUniform1i;
    _procs["glUniform1iv"]                          = (void *) glUniform1iv;
    _procs["glUniform2f"]                           = (void *) glUniform2f;
    _procs["glUniform2fv"]                          = (void *) glUniform2fv;
    _procs["glUniform2i"]                           = (void *) glUniform2i;
    _procs["glUniform2iv"]                          = (void *) glUniform2iv;
    _procs["glUniform3f"]                           = (void *) glUniform3f;
    _procs["glUniform3fv"]                          = (void *) glUniform3fv;
    _procs["glUniform3i"]                           = (void *) glUniform3i;
    _procs["glUniform3iv"]                          = (void *) glUniform3iv;
    _procs["glUniform4f"]                           = (void *) glUniform4f;
    _procs["glUniform4fv"]                          = (void *) glUniform4fv;
    _procs["glUniform4i"]                           = (void *) glUniform4i;
    _procs["glUniform4iv"]                          = (void *) glUniform4iv;
    _procs["glUniformMatrix2fv"]                    = (void *) glUniformMatrix2fv;
    _procs["glUniformMatrix3fv"]                    = (void *) glUniformMatrix3fv;
    _procs["glUniformMatrix4fv"]                    = (void *) glUniformMatrix4fv;
    _procs["glUseProgram"]                          = (void *) glUseProgram;
    _procs["glValidateProgram"]                     = (void *) glValidateProgram;
    _procs["glVertexAttrib1f"]                      = (void *) glVertexAttrib1f;
    _procs["glVertexAttrib1fv"]                     = (void *) glVertexAttrib1fv;
    _procs["glVertexAttrib2f"]                      = (void *) glVertexAttrib2f;
    _procs["glVertexAttrib2fv"]                     = (void *) glVertexAttrib2fv;
    _procs["glVertexAttrib3f"]                      = (void *) glVertexAttrib3f;
    _procs["glVertexAttrib3fv"]                     = (void *) glVertexAttrib3fv;
    _procs["glVertexAttrib4f"]                      = (void *) glVertexAttrib4f;
    _procs["glVertexAttrib4fv"]                     = (void *) glVertexAttrib4fv;
    _procs["glVertexAttribPointer"]                 = (void *) glVertexAttribPointer;
    _procs["glViewport"]                            = (void *) glViewport;

    _procs["glGenVertexArraysOES"]    = (void *) glGenVertexArraysOES;
    _procs["glDeleteVertexArraysOES"] = (void *) glDeleteVertexArraysOES;
    _procs["glBindVertexArrayOES"]    = (void *) glBindVertexArrayOES;

    // emscripten SDL does not have this symbol this
    //    _procs["glIsVertexArrayOES"]                    = (void *) glIsVertexArrayOES;

}

void *Client::getProcAddress(const char *name) const
{
    auto res = _procs.find(name);
    if (res != _procs.end())
    {
        return res->second;
    }

    return NULL;
}
