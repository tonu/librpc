#ifndef FIFO_H
#define FIFO_H

#include <stdint.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include "queue.hpp"
#include "Huffy.hpp"

#define MAX_PACKET_SIZE 10000

/// fifo queue for buffering frames and queueing
// works like this:
// - networking thread/function does read n chars from socket (or lets just say gets n chars)
// - immediately writes these bytes using writeFrames.
// - FIFO recognises frame lengths from stream and if it has full frame, it automatically pushes frame to queue
// - all time user thread can try to pop() full frame. If pop returns true, contents of buffer is full frame
//
// same goes opposite direction (from user to network)
//
/// limitations : read() / write() to buf must be sequential
// - write whole buf + pushbuf() must be done in single thread
// - popBuf + read whole buf must be done in single thread
// actually everything must be in single thread except popFrame()
#include "Util.hpp"
#include <iomanip>

class FIFO
{
public:
    FIFO()
    {
        _bufWrite = std::make_shared<std::vector<uint8_t>>();
        _bufRead = std::make_shared<std::vector<uint8_t>>();
        _bufFrame = std::make_shared<std::vector<uint8_t>>();

        _haveFrameSize = false;
        _frameBitSizeBuf = 0;
        _nCarry = 0;
        _sizeBytesRead = 0;

        _bail = false;
    }

    void reset();

    //  first do bunch of writeBuf()'s (FROM SINGLE THREAD!), then pushBuf(). This makes new frame that you can popFrame() and send to network
    void write(const void *ptr, size_t sz);
    void pushBuf(bool flag);
    std::shared_ptr<std::vector<uint8_t>> popFrame(); // getting ready frame out to send out to network

    void writeFrames(const void *p, uint32_t sz); // push data in from network, then if some frames become available, you can pop, then do reads until data is exhausted.
    bool popBuf(bool notReturnFlag); // this locks read buf
    size_t read(void *ptr, size_t sz);// do reads if read reaches 0 it automatically unlocks

    // not totally useless, but these functions might return "old" info in multithreaded environment. for statistics
    int readBufSize() const { return _bufRead->size(); };
    int writeBufSize() const { return _bufWrite->size(); };

    class ResetException: public std::exception
    {
    public:
        virtual const char* what() const throw()
        {
            return "fifo was reset";
        }
    };

private:


    moodycamel::ConcurrentQueue< std::shared_ptr<std::vector<uint8_t>>> _queue;

    std::shared_ptr<std::vector<uint8_t>> _bufFrame;
    std::shared_ptr<std::vector<uint8_t>> _bufWrite;
    std::shared_ptr<std::vector<uint8_t>> _bufRead;

    bool     _haveFrameSize = false;
    uint32_t _frameBitSizeBuf;
    uint32_t _nCarry;
    uint32_t  _sizeBytesRead;

    bool _bail;

    std::mutex _readLock;

    Huffy hf;
};


#endif // FIFO_H
