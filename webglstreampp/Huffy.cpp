//
// Created by Tonu Jaansoo on 29/08/16.
//

#include "Huffy.hpp"
#include <vector>
#include <iostream>

// this is generated with tool:

class Node
{
public:
    Node()
    {
        next[0] = NULL;
        next[1] = NULL;
    }

    Node *next[2]; // left-0 and right-1
    uint8_t val;
};

Huffy::Huffy()
{
    root = new Node();


    auto xxx  = new std::vector<std::pair< uint8_t , std::vector<bool>>> {
            std::pair< uint8_t , std::vector<bool>>(0x00,std::vector<bool>{0 }),
            std::pair< uint8_t , std::vector<bool>>(0x01,std::vector<bool>{1, 0, 0, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x02,std::vector<bool>{1, 1, 0, 0, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x03,std::vector<bool>{1, 1, 1, 1, 1, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x04,std::vector<bool>{1, 0, 1, 1, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x05,std::vector<bool>{1, 0, 1, 1, 0, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x06,std::vector<bool>{1, 1, 1, 1, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x07,std::vector<bool>{1, 0, 0, 0, 1, 0, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x08,std::vector<bool>{1, 1, 0, 1, 0, 0, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x09,std::vector<bool>{1, 0, 0, 1, 1, 1, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x0a,std::vector<bool>{1, 0, 0, 1, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x0b,std::vector<bool>{1, 0, 1, 1, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x0c,std::vector<bool>{1, 0, 0, 1, 0, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x0d,std::vector<bool>{1, 0, 1, 0, 1, 1, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x0e,std::vector<bool>{1, 1, 0, 1, 0, 1, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x0f,std::vector<bool>{1, 0, 1, 1, 0, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x10,std::vector<bool>{1, 1, 1, 0, 1, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x11,std::vector<bool>{1, 0, 1, 1, 0, 1, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x12,std::vector<bool>{1, 1, 1, 0, 1, 1, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x13,std::vector<bool>{1, 0, 1, 0, 0, 0, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x14,std::vector<bool>{1, 0, 1, 0, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x15,std::vector<bool>{1, 1, 1, 1, 1, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x16,std::vector<bool>{1, 0, 1, 1, 0, 1, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x17,std::vector<bool>{1, 1, 1, 1, 0, 1, 0, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x18,std::vector<bool>{1, 1, 0, 0, 0, 0, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x19,std::vector<bool>{1, 1, 0, 1, 0, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x1a,std::vector<bool>{1, 1, 0, 0, 1, 1, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x1b,std::vector<bool>{1, 1, 0, 0, 1, 1, 1, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x1c,std::vector<bool>{1, 0, 1, 1, 0, 0, 0, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x1d,std::vector<bool>{1, 1, 1, 1, 1, 1, 1, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x1e,std::vector<bool>{1, 1, 1, 1, 1, 1, 1, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x1f,std::vector<bool>{1, 1, 1, 1, 1, 1, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x20,std::vector<bool>{1, 1, 1, 0, 1, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x21,std::vector<bool>{1, 0, 0, 1, 1, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x22,std::vector<bool>{1, 1, 0, 1, 1, 1, 0, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x23,std::vector<bool>{1, 1, 0, 0, 1, 0, 0, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x24,std::vector<bool>{1, 0, 1, 0, 1, 0, 1, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x25,std::vector<bool>{1, 1, 0, 1, 1, 1, 0, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x26,std::vector<bool>{1, 1, 1, 0, 1, 1, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x27,std::vector<bool>{1, 0, 0, 1, 1, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x28,std::vector<bool>{1, 1, 1, 0, 0, 0, 1, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x29,std::vector<bool>{1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x2a,std::vector<bool>{1, 0, 0, 1, 0, 0, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x2b,std::vector<bool>{1, 1, 1, 0, 0, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x2c,std::vector<bool>{1, 1, 1, 0, 0, 0, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x2d,std::vector<bool>{1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x2e,std::vector<bool>{1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x2f,std::vector<bool>{1, 1, 0, 1, 1, 1, 1, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x30,std::vector<bool>{1, 0, 1, 0, 1, 1, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x31,std::vector<bool>{1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x32,std::vector<bool>{1, 0, 0, 1, 1, 1, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x33,std::vector<bool>{1, 0, 1, 1, 1, 1, 0, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x34,std::vector<bool>{1, 1, 0, 1, 1, 1, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x35,std::vector<bool>{1, 0, 1, 1, 0, 1, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x36,std::vector<bool>{1, 1, 1, 1, 0, 1, 0, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x37,std::vector<bool>{1, 1, 0, 0, 0, 0, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x38,std::vector<bool>{1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x39,std::vector<bool>{1, 1, 0, 1, 0, 0, 1, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x3a,std::vector<bool>{1, 1, 0, 1, 0, 0, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x3b,std::vector<bool>{1, 1, 1, 1, 1, 1, 1, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x3c,std::vector<bool>{1, 1, 0, 1, 0, 1, 0, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x3d,std::vector<bool>{1, 1, 0, 0, 0, 0, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x3e,std::vector<bool>{1, 1, 1, 1, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x3f,std::vector<bool>{1, 1, 0, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x40,std::vector<bool>{1, 1, 1, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x41,std::vector<bool>{1, 0, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x42,std::vector<bool>{1, 1, 1, 1, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x43,std::vector<bool>{1, 1, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x44,std::vector<bool>{1, 0, 1, 0, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x45,std::vector<bool>{1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x46,std::vector<bool>{1, 0, 1, 0, 1, 1, 0, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x47,std::vector<bool>{1, 1, 1, 0, 0, 0, 0, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x48,std::vector<bool>{1, 1, 1, 0, 0, 0, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x49,std::vector<bool>{1, 1, 0, 0, 0, 0, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x4a,std::vector<bool>{1, 0, 0, 1, 0, 1, 0, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x4b,std::vector<bool>{1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x4c,std::vector<bool>{1, 1, 0, 1, 0, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x4d,std::vector<bool>{1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x4e,std::vector<bool>{1, 0, 1, 0, 1, 0, 1, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x4f,std::vector<bool>{1, 0, 1, 1, 0, 1, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x50,std::vector<bool>{1, 0, 1, 1, 0, 0, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x51,std::vector<bool>{1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x52,std::vector<bool>{1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x53,std::vector<bool>{1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x54,std::vector<bool>{1, 0, 0, 1, 0, 1, 0, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x55,std::vector<bool>{1, 0, 0, 1, 1, 0, 0, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x56,std::vector<bool>{1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x57,std::vector<bool>{1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x58,std::vector<bool>{1, 1, 0, 0, 0, 0, 0, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x59,std::vector<bool>{1, 0, 1, 1, 1, 1, 0, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x5a,std::vector<bool>{1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x5b,std::vector<bool>{1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x5c,std::vector<bool>{1, 0, 1, 1, 0, 1, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x5d,std::vector<bool>{1, 1, 0, 1, 0, 1, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x5e,std::vector<bool>{1, 0, 0, 1, 0, 1, 1, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x5f,std::vector<bool>{1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x60,std::vector<bool>{1, 0, 0, 1, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x61,std::vector<bool>{1, 1, 1, 1, 0, 0, 1, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x62,std::vector<bool>{1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x63,std::vector<bool>{1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x64,std::vector<bool>{1, 0, 0, 1, 1, 0, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x65,std::vector<bool>{1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x66,std::vector<bool>{1, 0, 0, 1, 0, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x67,std::vector<bool>{1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x68,std::vector<bool>{1, 0, 1, 1, 0, 1, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x69,std::vector<bool>{1, 0, 0, 1, 0, 1, 0, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x6a,std::vector<bool>{1, 1, 1, 0, 0, 0, 1, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x6b,std::vector<bool>{1, 1, 1, 0, 0, 0, 0, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x6c,std::vector<bool>{1, 1, 0, 1, 0, 1, 0, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x6d,std::vector<bool>{1, 1, 1, 1, 1, 1, 1, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x6e,std::vector<bool>{1, 1, 0, 1, 0, 0, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x6f,std::vector<bool>{1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x70,std::vector<bool>{1, 1, 0, 0, 0, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x71,std::vector<bool>{1, 0, 0, 1, 1, 1, 0, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x72,std::vector<bool>{1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x73,std::vector<bool>{1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x74,std::vector<bool>{1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x75,std::vector<bool>{1, 0, 0, 0, 1, 0, 1, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x76,std::vector<bool>{1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x77,std::vector<bool>{1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x78,std::vector<bool>{1, 1, 0, 0, 1, 1, 1, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x79,std::vector<bool>{1, 1, 1, 0, 1, 1, 0, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x7a,std::vector<bool>{1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x7b,std::vector<bool>{1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x7c,std::vector<bool>{1, 0, 1, 0, 1, 1, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x7d,std::vector<bool>{1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x7e,std::vector<bool>{1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x7f,std::vector<bool>{1, 1, 0, 1, 0, 0, 1, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x80,std::vector<bool>{1, 0, 1, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x81,std::vector<bool>{1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x82,std::vector<bool>{1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x83,std::vector<bool>{1, 0, 0, 1, 0, 1, 1, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x84,std::vector<bool>{1, 0, 1, 0, 0, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x85,std::vector<bool>{1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x86,std::vector<bool>{1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x87,std::vector<bool>{1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x88,std::vector<bool>{1, 0, 0, 0, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x89,std::vector<bool>{1, 0, 0, 0, 1, 0, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x8a,std::vector<bool>{1, 1, 0, 0, 0, 0, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x8b,std::vector<bool>{1, 1, 1, 1, 0, 0, 1, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x8c,std::vector<bool>{1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x8d,std::vector<bool>{1, 1, 1, 1, 0, 1, 0, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x8e,std::vector<bool>{1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x8f,std::vector<bool>{1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x90,std::vector<bool>{1, 0, 1, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x91,std::vector<bool>{1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x92,std::vector<bool>{1, 0, 1, 0, 1, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x93,std::vector<bool>{1, 1, 0, 1, 0, 0, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x94,std::vector<bool>{1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x95,std::vector<bool>{1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x96,std::vector<bool>{1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x97,std::vector<bool>{1, 0, 0, 1, 0, 1, 1, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x98,std::vector<bool>{1, 0, 1, 1, 0, 1, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0x99,std::vector<bool>{1, 0, 0, 1, 0, 0, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x9a,std::vector<bool>{1, 0, 1, 0, 0, 0, 1, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x9b,std::vector<bool>{1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x9c,std::vector<bool>{1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x9d,std::vector<bool>{1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x9e,std::vector<bool>{1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0x9f,std::vector<bool>{1, 0, 0, 1, 1, 0, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xa0,std::vector<bool>{1, 1, 1, 1, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xa1,std::vector<bool>{1, 0, 0, 1, 1, 0, 0, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xa2,std::vector<bool>{1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xa3,std::vector<bool>{1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xa4,std::vector<bool>{1, 1, 1, 1, 0, 1, 0, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xa5,std::vector<bool>{1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xa6,std::vector<bool>{1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xa7,std::vector<bool>{1, 0, 0, 1, 0, 1, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xa8,std::vector<bool>{1, 1, 0, 1, 1, 1, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xa9,std::vector<bool>{1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xaa,std::vector<bool>{1, 0, 1, 0, 0, 0, 1, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xab,std::vector<bool>{1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xac,std::vector<bool>{1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xad,std::vector<bool>{1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xae,std::vector<bool>{1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xaf,std::vector<bool>{1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xb0,std::vector<bool>{1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xb1,std::vector<bool>{1, 0, 1, 0, 1, 0, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xb2,std::vector<bool>{1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xb3,std::vector<bool>{1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xb4,std::vector<bool>{1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xb5,std::vector<bool>{1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xb6,std::vector<bool>{1, 1, 0, 1, 1, 1, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xb7,std::vector<bool>{1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xb8,std::vector<bool>{1, 0, 0, 1, 0, 1, 1, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xb9,std::vector<bool>{1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xba,std::vector<bool>{1, 0, 1, 0, 0, 0, 1, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xbb,std::vector<bool>{1, 0, 1, 0, 1, 1, 0, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xbc,std::vector<bool>{1, 0, 1, 0, 1, 0, 1, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xbd,std::vector<bool>{1, 1, 1, 0, 0, 0, 0, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xbe,std::vector<bool>{1, 0, 0, 1, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xbf,std::vector<bool>{1, 1, 0, 0, 1, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xc0,std::vector<bool>{1, 1, 0, 0, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xc1,std::vector<bool>{1, 0, 1, 0, 1, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xc2,std::vector<bool>{1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xc3,std::vector<bool>{1, 1, 0, 0, 1, 1, 1, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xc4,std::vector<bool>{1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xc5,std::vector<bool>{1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xc6,std::vector<bool>{1, 0, 1, 0, 0, 0, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xc7,std::vector<bool>{1, 1, 0, 0, 1, 1, 1, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xc8,std::vector<bool>{1, 1, 1, 1, 0, 1, 0, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xc9,std::vector<bool>{1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xca,std::vector<bool>{1, 0, 1, 1, 0, 0, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xcb,std::vector<bool>{1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xcc,std::vector<bool>{1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xcd,std::vector<bool>{1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xce,std::vector<bool>{1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xcf,std::vector<bool>{1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xd0,std::vector<bool>{1, 1, 1, 1, 0, 0, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xd1,std::vector<bool>{1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xd2,std::vector<bool>{1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xd3,std::vector<bool>{1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xd4,std::vector<bool>{1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xd5,std::vector<bool>{1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xd6,std::vector<bool>{1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xd7,std::vector<bool>{1, 1, 0, 1, 0, 1, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xd8,std::vector<bool>{1, 1, 0, 1, 1, 1, 1, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xd9,std::vector<bool>{1, 1, 0, 1, 0, 0, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xda,std::vector<bool>{1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xdb,std::vector<bool>{1, 1, 0, 1, 1, 1, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xdc,std::vector<bool>{1, 1, 1, 0, 0, 0, 1, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xdd,std::vector<bool>{1, 0, 1, 1, 1, 1, 0, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xde,std::vector<bool>{1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xdf,std::vector<bool>{1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xe0,std::vector<bool>{1, 1, 0, 0, 0, 0, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xe1,std::vector<bool>{1, 0, 1, 1, 1, 1, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xe2,std::vector<bool>{1, 1, 1, 1, 0, 0, 1, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xe3,std::vector<bool>{1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xe4,std::vector<bool>{1, 0, 1, 1, 0, 1, 1, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xe5,std::vector<bool>{1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xe6,std::vector<bool>{1, 0, 0, 1, 0, 1, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xe7,std::vector<bool>{1, 0, 1, 1, 1, 1, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xe8,std::vector<bool>{1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xe9,std::vector<bool>{1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xea,std::vector<bool>{1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xeb,std::vector<bool>{1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xec,std::vector<bool>{1, 1, 0, 1, 1, 1, 0, 0, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xed,std::vector<bool>{1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xee,std::vector<bool>{1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xef,std::vector<bool>{1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xf0,std::vector<bool>{1, 1, 1, 0, 0, 0, 1, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xf1,std::vector<bool>{1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xf2,std::vector<bool>{1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xf3,std::vector<bool>{1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xf4,std::vector<bool>{1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xf5,std::vector<bool>{1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xf6,std::vector<bool>{1, 1, 1, 0, 0, 0, 0, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xf7,std::vector<bool>{1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xf8,std::vector<bool>{1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xf9,std::vector<bool>{1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xfa,std::vector<bool>{1, 0, 1, 0, 1, 0, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xfb,std::vector<bool>{1, 0, 0, 1, 0, 1, 1, 1, 0, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xfc,std::vector<bool>{1, 1, 0, 1, 1, 1, 0, 1, 1, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xfd,std::vector<bool>{1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1 }),
            std::pair< uint8_t , std::vector<bool>>(0xfe,std::vector<bool>{1, 1, 0, 1, 0, 1, 1, 0, 0 }),
            std::pair< uint8_t , std::vector<bool>>(0xff,std::vector<bool>{1, 1, 1, 0, 1, 0 }),
    };

codes.reset(xxx);


    for(auto code: *codes)
    {
        Node *current = root;

        for(auto bit: code.second)
        {
            if (current->next[bit] == NULL)
                current->next[bit] = new Node();

            current = current->next[bit];
        }
        current->val = code.first;
    }
}

void Huffy::encode(std::shared_ptr<std::vector<uint8_t>>& vec, uint32_t & bits)
{
    std::vector<bool> out;

    for(auto it = vec->begin(); it != vec->end(); it++)
    {
        auto & ref = codes->at(*it).second;
        out.insert(out.end(), ref.begin(), ref.end());
    }

    bits = out.size();

    // copy out to vec
    vec->clear();
    uint8_t ch = 0;
    uint8_t n = 0;
    for(auto it = out.begin(); it != out.end(); it++)
    {
        // set bit if true
        if (*it)
            ch |= 1 << (n);

        n++;
        if (n == 8)
        {
            vec->push_back(ch);
            n = ch = 0;
        }
    }

    // push last char (that is not full)
    if (n > 0)
        vec->push_back(ch);
}

void Huffy::decode(std::shared_ptr<std::vector<uint8_t>> &vec, uint32_t bits)
{
    std::vector<uint8_t> out;

    // read bit by bit
    Node *current = root;
    for (uint32_t n=0;n<bits;n++)
    {
        bool bit(vec->at(n/8) & (1 << (n % 8)));

        //find
        if (current->next[uint8_t(bit)])
        {
            current = current->next[uint8_t(bit)];
        }

        if (current->next[0] == NULL && current->next[1] == NULL)
        {
            out.push_back(current->val);
            current = root;
        }
    }

    *vec = out;
}
