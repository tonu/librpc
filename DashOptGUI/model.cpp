#include "model.h"
#include <QDebug>
#include <sstream>
#include <fstream>

Model::Model(QObject *parent) : QObject(parent)
{
    init_properties();

    readCSV("out.csv", _allData, _allAnomalies);

    _last = 160000;

    connect(&_timer, &QTimer::timeout, [&]()
    {
        set_min(qMax(_last - _count, 0));
        set_max(_last);
static int i=0; i++;
if (i%10 ==0)
{
        set_data(filter(_allData.mid(_last - _count, _count), _visibleCount));
        set_anom(_allAnomalies);
}
        //animate with 10 new points
        _last += 10;
    });

    _timer.setSingleShot(false);
    _timer.setInterval(30);
    _timer.start();
}

Model::~Model()
{
    save();
}

void Model::submit()
{
    qDebug() << "Submit: " <<
                "task_type=" << _task_type <<
                "max_anoms=" << _max_anoms <<
                "direction=" << _direction <<
                "alpha=" << _alpha <<
                "period=" << _period <<
                "only_last=" << _only_last <<
                "threshold=" << _threshold <<
                "e_value=" << _e_value;

//    _allData = ;
//    _allAnomalies = ;
}

// reduces data while trying to keep all info. this is quick and dirty. TODO better algo
QVector<QPointF> Model::filter(const QVector<QPointF> & input, int outCount)
{
    int inCount = input.size();

    // do not interpolate
    if (outCount >= inCount * 2)
        return input;

    //reset min/max
    qreal min =  std::numeric_limits<qreal>::infinity();
    qreal max = -std::numeric_limits<qreal>::infinity();

    qreal ratio = (double)outCount/inCount;
    QVector<QPointF> out;
    int outCounter = 0;

    for (int i=0; i<inCount; i++)
    {
        min = qMin(input[i].y(), min);
        max = qMax(input[i].y(), max);

        //push new value
        if (outCounter != (int)(i * ratio) || i == inCount-1)
        {
            out.push_back(QPointF(input[i].x(), min));

            //push another
            if (min != max)
                out.push_back(QPointF(input[i].x(), max));

            //reset min/max
            min = std::numeric_limits<qreal>::infinity();
            max = -std::numeric_limits<qreal>::infinity();
            outCounter = (int)(i * ratio);
        }
    }

    return out;
}


std::vector<std::string> Model::split(std::istream& str)
{
    std::vector<std::string>   result;
    std::string                line;

    std::getline(str,line);

    std::stringstream          lineStream(line);
    std::string                cell;

    while(std::getline(lineStream,cell, ' '))
    {
        result.push_back(cell);
    }
    return result;
}

void Model::readCSV(const char *filename, QVector<QPointF> & data, QVector<QPointF> & anomalies)
{

    std::ifstream file (filename);
    std::string line;

    int count = 0;
    while ( file.good() )
    {
        std::getline (file, line);

        std::stringstream ss(line);
        auto tokens = split(ss);

        if (tokens.size() == 2)
        {
            if (std::stod(tokens[1]) == 1)
                anomalies.push_back(QPointF(count, std::stod(tokens[0])));

            data.push_back(QPointF(count, std::stod(tokens[0])));
        }

        count++;
    }
}
