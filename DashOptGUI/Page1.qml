import QtQuick 2.0
import QuickPlot 1.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0

Item {
    RowLayout {
        anchors.fill: parent
        anchors.margins: 15
        GroupBox
        {

            Layout.minimumWidth: 400
            Layout.fillHeight: true
            title: "Choices:"

            GridLayout {
                anchors.fill: parent
                anchors.margins: 20
                columns: 2
                rowSpacing: 20


                Text {
                    text: "task_type:"
                }
                ComboBox {
                    Layout.fillWidth: true

                    textRole: "key"
                    model: ListModel {
                        ListElement { key: "Anomaly"; }
                    }

                }

                Text { text: "max_anoms:"}
                DoubleSpinBox {
                    realFrom: 0
                    realTo: 1
                    realStepSize: 0.001
                    decimals: 3

                    realValue: cpp.max_anoms
                    onRealValueChanged: cpp.max_anoms = realValue;
                }

                Text {
                    text: "direction:"
                }
                ComboBox {
                    Layout.fillWidth: true

                    textRole: "key"
                    model: ListModel {
                        ListElement { key: "Both";  }
                    }
                }

                Text { text: "alpha:"}
                DoubleSpinBox {
                    realFrom: 0
                    realTo: 1
                    realStepSize: 0.01
                    decimals: 3

                    realValue: cpp.alpha
                    onRealValueChanged: cpp.alpha = realValue;
                }


                Text { text: "period:"}
                SpinBox {
                    from: 0
                    to:1000000000
                    stepSize: 1

                    value: cpp.period
                    onValueChanged: cpp.period = value;
                }

                Text { text: "only_last:"}
                CheckBox {
                    checked: cpp.only_last
                    onCheckedChanged: cpp.only_last = checked
                }

                Text { text: "threshold:" }
                ComboBox {
                    Layout.fillWidth: true

                    textRole: "key"
                    model: ListModel {
                        ListElement { key: "None";  }
                    }
                }

                Text { text: "e_value:"}
                CheckBox {
                    checked: cpp.e_value
                    onCheckedChanged: cpp.e_value = checked
                }



                Text {

                }
                Item {
                    Layout.fillHeight: true
                }

                Button
                {
                    text: "Submit";
                    onClicked: cpp.submit();
                }

            }
        }

        ColumnLayout {
            Layout.fillHeight: true
            Layout.fillWidth: true



            PlotArea {
                id: plotArea

                Layout.fillHeight: true
                Layout.fillWidth: true


                xScaleEngine: FixedScaleEngine {
                    id:xscale
                    min: cpp.min
                    max: cpp.max
                }

                yScaleEngine: TightScaleEngine {
                    margin: 0.2;
                }


                items: [
                    Curve {
                        id: meter;
                        color: "gray"
                    }
                    ,
                    ScrollingScatter {
                        id: meter2;

                        color: "red"
                    }
                ]

                property var plot1Data: cpp.data
                onPlot1DataChanged: {
                    meter.setData(cpp.data);
                }
                property var plot2Data: cpp.anom
                onPlot2DataChanged: {
                    meter2.setData(cpp.anom);
                }
                Component.onCompleted: {
                    meter.setData(cpp.data);
                    meter2.setData(cpp.anom);
                }
            }
            Slider {
                Layout.fillWidth: true

                value: 0.5

                onValueChanged: cpp.count = Math.pow(2, position * 10) * 100;
            }

        }
    }


    //    // workaround for bug - https://gitlab.com/qtquickplot/qtquickplot/issues/4
    //    Timer {
    //        id: timer2
    //        interval: 2500;
    //        onTriggered: plotArea.update()
    //        repeat: false
    //        running: true
    //    }
}
