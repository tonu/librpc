#ifndef MODEL_H
#define MODEL_H

#include <QObject>
#include <QSettings>
#include <QVector>
#include <QPointF>
#include <QTimer>
#include <math.h>

class Model : public QObject
{
    Q_OBJECT

public:
    explicit Model(QObject *parent = 0);
    ~Model();

#define Q_DEF_PROPERTIES \
    Q_DEF_PROPERTY_RWS(QString,     task_type, "Anomaly") \
    Q_DEF_PROPERTY_RWS(double,      max_anoms,  0.001)\
    Q_DEF_PROPERTY_RWS(QString,     direction, "Both")\
    Q_DEF_PROPERTY_RWS(double,       alpha,      0.05)\
    Q_DEF_PROPERTY_RWS(double,       period,     86400)\
    Q_DEF_PROPERTY_RWS(bool,         only_last,  false)\
    Q_DEF_PROPERTY_RWS(QString,     threshold, "None")\
    Q_DEF_PROPERTY_RWS(bool,         e_value,    true)\
    \
    Q_DEF_PROPERTY_RW(int,          min,    0)\
    Q_DEF_PROPERTY_RW(int,          max,    0)\
    Q_DEF_PROPERTY_RW(int,          count,  pow(2,5) * 100)\
    Q_DEF_PROPERTY_RW(int,          visibleCount,  800)\
    Q_DEF_PROPERTY_RW(QVector<QPointF>,  data,    QVector<QPointF>())\
    Q_DEF_PROPERTY_RW(QVector<QPointF>,  anom,    QVector<QPointF>())\

#include "generate_properties.h"


    QVector<QPointF> filter(const QVector<QPointF> & input, int outCount);

signals:

public slots:
    void submit();



private slots:

private:

    static std::vector<std::string> split(std::istream& str);
    static void readCSV(const char *filename, QVector<QPointF> & data, QVector<QPointF> & anomalies);

    QTimer _timer;

    int _last;
    QVector<QPointF> _allData;
    QVector<QPointF> _allAnomalies;
  };

#endif // MODEL_H
