#include <QApplication>
#include <QQmlApplicationEngine>

#include <memory>
#include <QFontDatabase>
#include <QDebug>
#include <QQmlContext>

#include "model.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QQmlApplicationEngine engine;

    QFontDatabase fontDatabase;
    qDebug() << fontDatabase.families();

    qRegisterMetaType<QVector<float>>();
    qRegisterMetaType<QVector<QPointF>>();

    std::shared_ptr<Model> obj(new Model);
    engine.rootContext()->setContextProperty("cpp", obj.get());
    engine.setObjectOwnership(obj.get(), QQmlEngine::CppOwnership);

    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    return app.exec();
}
