import QtQuick 2.6
import QtQuick.Controls 2.0

//! [1]
SpinBox {
    id: spinbox

    property int decimals: 2
    property real realValue:0.0
    property real realFrom:0.0
    property real realTo:1.0
    property real realStepSize: 0.01



    property double divider: Math.pow(10, decimals)

    value: realValue * divider;
    from: realFrom * divider;
    to: realTo * divider;

    stepSize: realStepSize * divider

    onValueChanged: realValue = value / divider

    validator: DoubleValidator {
        bottom: Math.min(spinbox.from, spinbox.to)
        top:  Math.max(spinbox.from, spinbox.to)
    }

    textFromValue: function(value, locale) {
        return Number(value / divider).toLocaleString(locale, 'f', spinbox.decimals)
    }

    valueFromText: function(text, locale) {
        return Number.fromLocaleString(locale, text) * divider
    }
}
