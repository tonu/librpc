/// generate_properties.cpp is preprocessor code-generator.
/// It is meant to generate QT properties inside class definition.

#ifndef Q_DEF_PROPERTIES
#error "did you forget to define PROPS? yes!"
#endif
// XXX: we should not include any headers inside class definition!!!
// #include <qobjectdefs.h>

#define Q_DEF_PROPERTY_RWS(T, name, initVal) \
    public: \
    Q_PROPERTY(T name READ name WRITE set_##name NOTIFY name##_changed); \
    T name() const \
{ \
    return _##name; \
    } \
    void set_##name(T val) \
{ \
    if (_##name == val) \
    return;\
    _##name = val; \
    emit name##_changed(); \
    } \
    Q_SIGNALS: \
    void name##_changed(); \
    private: \
    T _##name;

#define Q_DEF_PROPERTY_RW(T, name, initVal) Q_DEF_PROPERTY_RWS(T, name, initVal)

#define Q_DEF_PROPERTY_RWP(T, name, initVal) \
    public: \
    Q_PROPERTY(T* name READ name CONSTANT); \
    T* name() const \
{ \
    return _##name.get(); \
    } \
    private: \
    std::shared_ptr< T > _##name;




Q_DEF_PROPERTIES

#undef Q_DEF_PROPERTY_RWS
#undef Q_DEF_PROPERTY_RWP
#undef Q_DEF_PROPERTY_RW
#define Q_DEF_PROPERTY_RWS(T, name, initVal) _##name = s.value("settings/" #name, initVal).value<T>();
#define Q_DEF_PROPERTY_RWP(T, name, initVal) _##name = std::shared_ptr<T>(initVal);
#define Q_DEF_PROPERTY_RW(T, name, initVal) _##name = initVal;

public:
void init_properties()
{
    QSettings s;
    // load from file
    Q_DEF_PROPERTIES
}



#undef Q_DEF_PROPERTY_RWS
#undef Q_DEF_PROPERTY_RWP
#undef Q_DEF_PROPERTY_RW
#define Q_DEF_PROPERTY_RWS(T, name, initVal) s.setValue("settings/" #name, _##name);
#define Q_DEF_PROPERTY_RWP(T, name, initVal)
#define Q_DEF_PROPERTY_RW(T, name, initVal)
void save()
{
    QSettings s;
    Q_DEF_PROPERTIES
}
#undef Q_DEF_PROPERTY_RWS
#undef Q_DEF_PROPERTY_RWP
#undef Q_DEF_PROPERTY_RW

#undef Q_DEF_PROPERTIES
