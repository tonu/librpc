import QtQuick 2.7
import QtQuick.Controls 1.4



ApplicationWindow {
    visible: true
    width: 640
    height: 480

    color: "#00000000"

    MouseArea {
        anchors.fill: parent
    }

    Rectangle {
        layer.enabled: true

        color:"white"
        anchors.fill: parent

        radius: 25
        border.color: "gray"
    }

    Page1 {
        layer.enabled: true
        anchors.fill: parent
        anchors.margins: 60
    }


    Component.onCompleted: showMaximized()
}
