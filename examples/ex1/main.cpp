#include <webglstreampp/client.hpp>
#include <webglstreampp/wsserver.hpp>
#include "game.hpp"


bool run = false;

void output_gl_diagnostics()
{
    std::cout << "GL version:" << glGetString(GL_VERSION) << std::endl;
    std::cout << "GL renderer:" << glGetString(GL_RENDERER) << std::endl;
    std::cout << "GL vendor:" << glGetString(GL_VENDOR) << std::endl;
    std::cout << "GLSL version:" << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
    std::cout << "GL extensions:" << glGetString(GL_EXTENSIONS) << std::endl;
}

void run_gltest(Client *client)
{
    run = true;

    std::cerr << "starting GL test\n";

    // catch connection closed
    try {
        output_gl_diagnostics();

        std::unique_ptr<game> g(new game);
        while (run)
        {
            g->draw();
            usleep(16000);
            std::cerr << ".\n";
        }
    }
    catch (FIFO::ResetException &e)
    {
        std::cerr << "err:" << e.what() << "\n";
    }

    std::cerr << "thread end\n";
}

int main(int argc, char *argv[])
{
    std::shared_ptr<FIFO> fifo_cli_to_srv(new FIFO);
    std::shared_ptr<FIFO> fifo_srv_to_cli(new FIFO);

    Client client(fifo_cli_to_srv, fifo_srv_to_cli);

    WSServer ws(12345, fifo_cli_to_srv, fifo_srv_to_cli);

    std::cerr << "starting server\n";

    std::shared_ptr<std::thread> thread;

    ws.onConnect = [&] ()
    {
        if (thread)
        {
            fifo_srv_to_cli->reset();

            run = false;
            thread->join();
        }

        thread = std::make_shared<std::thread>(run_gltest, &client);
    };

    ws.onClose = [&] ()
    {
        std::cerr << "close\n";
    };

    ws.loop();

    return 0;
}
