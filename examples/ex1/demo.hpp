#ifndef DEMO_H
#define DEMO_H


class demo
{
public:
    demo();

    bool init();
    void draw();

private:
    GLuint LoadShader(GLenum type, const char *shaderSrc);
};

#endif // DEMO_H
