#include <webglstreampp/client.hpp>
#include <webglstreampp/wsserver.hpp>
#include <webglstreampp/server.hpp>

#include <thread>


static std::shared_ptr<FIFO> fifo_cli_to_srv(new FIFO);
static std::shared_ptr<FIFO> fifo_srv_to_cli(new FIFO);
static std::shared_ptr<Client> client;
static std::shared_ptr<Server> server;
static std::shared_ptr<WSServer> ws;

static std::condition_variable cv;
static bool ready = false;
static std::mutex m;

std::thread serverThread;

void init()
{
    client = std::make_shared<Client>(fifo_cli_to_srv, fifo_srv_to_cli);
    server = std::make_shared<Server>(fifo_srv_to_cli, fifo_cli_to_srv);

    ws = std::make_shared<WSServer>(12345, fifo_cli_to_srv, fifo_srv_to_cli);

    std::cerr << "starting server ...\n";
    ws->start();

    ws->onConnect = [&] ()
    {
        std::cout << "Connection\n";

        std::lock_guard<std::mutex> lk(m);
        ready = true;
        cv.notify_one();
    };

    {
        std::unique_lock<std::mutex> lk(m);
        cv.wait(lk, []{return ready;});
    }

    serverThread = std::thread([&]{ while(1) { server->step(); usleep(1000); } });//fixme
}

void deinit()
{
    ws->stop();
}