cmake_minimum_required(VERSION 3.2)
project(examples)

add_subdirectory(ex1)
add_subdirectory(ex2)
