#!/bin/bash

SILENT=2 # 0 not silenced, 1 silence stdout, 2 silence stdout & stderr
MAKE_ARGS="-j12"

STARTTIME=$(date +%s)

GREEN='\033[0;32m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

set -e
BUILD_MAIN_PATH=$(pwd)

export LC_ALL="C"


function cmd {
    echo -e "$GREEN [$(($(date +%s) - $STARTTIME))s] $BLUE    $@ $NC"

    if [ "$SILENT" -eq "1" ]; then
        "$@" > /dev/null

    elif [ "$SILENT" -eq "2" ]; then
        "$@" 2> /dev/null > /dev/null
    else
        "$@"
    fi
}

function prn {
    echo -e "$GREEN [$(($(date +%s) - $STARTTIME))s] $@ $NC"
}

# ----------------------------------------------

prn "Getting emscripten SDK..."
if [ ! -d emsdk ]; then
   cmd git clone --progress https://github.com/juj/emsdk.git
fi

prn "Compiling emscripten SDK..."
cmd cd emsdk
cmd ./emsdk install sdk-incoming-64bit
prn "Activating emscripten SDK..."
cmd ./emsdk activate sdk-incoming-64bit
cmd cd $BUILD_MAIN_PATH
cmd source emsdk/emsdk_env.sh

#prn "Cleaning builds..."
#cmd rm -rf build*

prn "Building WebGL..."
cmd mkdir -p build-webglstreampp
cmd cd build-webglstreampp
cmd export CC=/usr/bin/clang
cmd export CXX=/usr/bin/clang++
cmd cmake ../../
cmd make $MAKE_ARGS
cmd cd $BUILD_MAIN_PATH


prn "Building Qt..."
cmd mkdir -p build-qt
cmd cd build-qt
cmd ../../qt-everywhere-opensource-src-5.7.0/configure -release -no-xcb -DQSG_SEPARATE_INDEX_BUFFER=1 -opengl es2 -opensource -confirm-license -c++std c++11 -qt-freetype -nomake examples -nomake tests -prefix ../QtHack -platform linux-clang-libc++-webgl -DGL_GLEXT_PROTOTYPES -no-egl -no-pkg-config
cmd make $MAKE_ARGS module-qtbase
cmd make $MAKE_ARGS module-qtdeclarative
cmd make $MAKE_ARGS module-qtquickcontrols
cmd make $MAKE_ARGS module-qtquickcontrols2

prn "Copying Qt to dst..."
cmd cd $BUILD_MAIN_PATH/build-qt/qtbase
cmd make install
cmd cd $BUILD_MAIN_PATH/build-qt/qtdeclarative
cmd make install
cmd cd $BUILD_MAIN_PATH/build-qt/qtquickcontrols
cmd make install
cmd cd $BUILD_MAIN_PATH/build-qt/qtquickcontrols2
cmd make install
cmd cd $BUILD_MAIN_PATH

prn "Building webgl platform plugin ..."
cmd mkdir -p build-qtwebgl
cmd cd build-qtwebgl
cmd $BUILD_MAIN_PATH/build-qt/QtHack/bin/qmake -spec linux-clang-libc++-webgl ../../webgl/webgl.pro
cmd make $MAKE_ARGS
cmd make install
cmd cd $BUILD_MAIN_PATH

prn "Building qtquickplot..."
cmd mkdir -p build-qtquickplot
cmd cd build-qtquickplot
cmd $BUILD_MAIN_PATH/build-qt/QtHack/bin/qmake -spec linux-clang-libc++-webgl ../../qtquickplot/quickplot.pro
cmd make
cmd make install
cmd cd $BUILD_MAIN_PATH

prn "Copying libwebglstreampp.so =>  $BUILD_MAIN_PATH/build-qt/QtHack/lib"
cmd cp $BUILD_MAIN_PATH/build-webglstreampp/webglstreampp/libwebglstreampp.so $BUILD_MAIN_PATH/build-qt/QtHack/lib

prn "Building DashOpt..."
cmd mkdir -p build-dashopt
cmd cd build-dashopt
cmd $BUILD_MAIN_PATH/build-qt/QtHack/bin/qmake -spec linux-clang-libc++-webgl ../../DashOptGUI/dashoptgui.pro
cmd make $MAKE_ARGS
cmd cd $BUILD_MAIN_PATH

prn "Copying webclient to ./webclient"
cmd mkdir -p ./webclient
cmd cp $BUILD_MAIN_PATH/build-webglstreampp/build-web/webglrenderer/webglrenderer.js.mem ./webclient/
cmd cp $BUILD_MAIN_PATH/build-webglstreampp/build-web/webglrenderer/webglrenderer.js     ./webclient/
cmd cp $BUILD_MAIN_PATH/../webglstreampp/webglrenderer/html/* ./webclient/

prn "ALL DONE!"
cmd cp $BUILD_MAIN_PATH/build-dashopt/dashoptgui .
echo "----------------------------------------------------"
echo "WebClient is located at ./webclient/ You can move it to /var/www/html/"
echo "Application executable at current dir"
echo "try running './dashoptgui -platform webgl'"
echo "----------------------------------------------------"

