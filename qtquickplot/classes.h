#ifndef CLASSES_H
#define CLASSES_H

// Some OpenGL knowledge may be required to understand this example.

#include <QSGSimpleMaterialShader>
#include <QOpenGLTexture>
#include <QSGGeometryNode>
#include <QColor>
#include <QSGDynamicTexture>

class QSGTexture;

// This class allows a single texture to be updated while still using QSGDynamicTexture,
// which does not support updating the texture.  Notice it derives from QSGDynamicTexture.
// Essentially it is a wrapper around a QOpenGLTexture, but it gets the added
// Scene Graph benefits that come with QSGDynamicTexture.
class DynamicTexture : public QSGDynamicTexture
{
public:
    DynamicTexture();
    ~DynamicTexture();
    void bind() Q_DECL_OVERRIDE { m_texture->bind(); }
    bool hasAlphaChannel() const Q_DECL_OVERRIDE { return true; }
    bool hasMipmaps() const Q_DECL_OVERRIDE { return m_texture->mipLevels(); }
    int textureId() const Q_DECL_OVERRIDE { return m_texture ? m_texture->textureId() : 0; }
    QSize textureSize() const Q_DECL_OVERRIDE { return m_texture ? QSize(m_texture->width(), m_texture->height()) : QSize(); }

    QOpenGLTexture* texture() { return m_texture; }

    void setTextureInfo(QOpenGLTexture::TextureFormat format, QString url);
    bool updateTexture() Q_DECL_OVERRIDE;

private:
    QOpenGLTexture *m_texture;
    QOpenGLTexture::TextureFormat m_format;
    QSize m_size;
    QImage m_image;
    QString m_oldUrl;
    QString m_url;
};

// This simple class is used in conjunction with YUVMaterial to help generate boilerplate code.
struct Textures
{
    DynamicTexture *myTexture;
};

// This is a Material class. Notice it inherits from QSGSimpleMaterialShader.
// A Material is kind of self explanatory, it can be thought of as how your scene object appears visually.
// It can be simple colors, to textures, or can even by more dynamic in how it looks.
class YUVMaterial : public QSGSimpleMaterialShader<Textures>
{
    // Don't forget this macro, this is what creates some boilerplate code so you don't have to.
    QSG_DECLARE_SIMPLE_SHADER(YUVMaterial, Textures)

public:

    YUVMaterial();
    ~YUVMaterial();

    // Vertex shader, if you don't know what this means you should probably read more up on modern OpenGL.
    // This is a virtual function that Qt internals will call when needed.
    const char *vertexShader() const {
        return
                "attribute highp vec4 aVertex;                              \n"
                "attribute highp vec2 aTexCoord;                            \n"
                "uniform highp mat4 qt_Matrix;                              \n"
                "varying highp vec2 texCoord;                               \n"
                "void main() {                                              \n"
                "    gl_Position = qt_Matrix * aVertex;                     \n"
                "    texCoord = aTexCoord;                                  \n"
                "}";
    }

    // Fragment shader, if you don't know what this means you should probably read more up on modern OpenGL.
    // This is a virtual function that Qt internals will call when needed.
    const char *fragmentShader() const {
        return
                "uniform lowp sampler2D myTexture;                          \n"
                "uniform lowp vec4 color;                                   \n"
                "uniform lowp float qt_Opacity;                             \n"
                "varying highp vec2 texCoord;                               \n"
                "void main() {                                              \n"
                  "gl_FragColor = color;                                    \n"
                  "gl_FragColor.a = texture2D(myTexture,texCoord).a * qt_Opacity; \n"
                "}";
    }

    // This is what you use to configure your vertex shader attributes.
    // This is a virtual function that Qt internals will call when needed.
    QList<QByteArray> attributes() const {
        return QList<QByteArray>() << "aVertex" << "aTexCoord";
    }

    // This updates the state of classes used by the Material, in this case the "Textures" struct.
    // This is a virtual function that Qt internals will call when needed.
    void updateState(const Textures *state, const Textures *);

    // Any OpenGL uniforms need to be set here.
    // This is a virtual function that Qt internals will call when needed.
    void resolveUniforms();

    QColor _color;
};
#endif // CLASSES_H

