#include "qpscrollingscatter.h"

#include <QSGGeometry>
#include <QSGGeometryNode>
#include <QSGFlatColorMaterial>

#include "classes.h"

QPScrollingScatter::QPScrollingScatter(QQuickItem *parent)
    : QPCurve(parent)
{

    connect(this, &QPCurve::colorChanged, [&]()
    {

    });

}

QSGNode *QPScrollingScatter::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *)
{
    QSGGeometryNode *node = 0;
    QSGGeometry *geometry = 0;

    if (!oldNode) {
        node = new QSGGeometryNode;
        geometry = new QSGGeometry(QSGGeometry::defaultAttributes_TexturedPoint2D(), m_data.size() * 6);
        geometry->setDrawingMode(GL_TRIANGLES);

        node->setGeometry(geometry);
        node->setFlag(QSGNode::OwnsGeometry);

        QString m_fileName("x.png");

        // Set the texture data and update the texture.
        QSGSimpleMaterial<Textures> *material = YUVMaterial::createMaterial();

        material->state()->myTexture = new DynamicTexture();
        material->state()->myTexture->setTextureInfo(QOpenGLTexture::RGBA8_UNorm, m_fileName);
        material->setFlag(QSGMaterial::Blending);

        node->setMaterial(material);

        node->setFlag(QSGNode::OwnsMaterial);
        node->markDirty(QSGNode::DirtyMaterial);
    } else {
        node = static_cast<QSGGeometryNode *>(oldNode);
        geometry = node->geometry();
        geometry->allocate(m_data.size() * 6);
    }

    QSGGeometry::TexturedPoint2D *vertices = geometry->vertexDataAsTexturedPoint2D();

    for (int i = 0; i < m_data.size(); i ++)
    {
        QPointF pixCoords = mapToScene(m_data[i]);

        qreal w = 6;

        // 6 verts per rectangle tl,bl,br
        QPointF tl = mapFromScene(pixCoords + QPointF(-w,-w));
        QPointF bl = mapFromScene(pixCoords + QPointF(-w, w));
        QPointF br = mapFromScene(pixCoords + QPointF( w, w));
        QPointF tr = mapFromScene(pixCoords + QPointF( w,-w));

        vertices[i*6 + 0].set(tl.x(), tl.y(), 0, 0);
        vertices[i*6 + 1].set(bl.x(), bl.y(), 0, 1);
        vertices[i*6 + 2].set(br.x(), br.y(), 1, 1);

        vertices[i*6 + 3].set(br.x(), br.y(), 1, 1);
        vertices[i*6 + 4].set(tr.x(), tr.y(), 1, 0);
        vertices[i*6 + 5].set(tl.x(), tl.y(), 0, 0);
    }

    node->markDirty(QSGNode::DirtyGeometry);

    return node;
}
