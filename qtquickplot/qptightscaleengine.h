#ifndef QPTIGHTSCALEENGINE_H
#define QPTIGHTSCALEENGINE_H

#include "qpscaleengine.h"

class QPTightScaleEngine : public QPScaleEngine
{
    Q_OBJECT
public:
    Q_PROPERTY(qreal margin READ margin WRITE setMargin NOTIFY marginChanged)

    explicit QPTightScaleEngine(QQuickItem *parent = 0);

    qreal margin() const;
    void setMargin(qreal margin);

signals:
    void marginChanged();

protected:
    void calculateMinMax(qreal &min, qreal &max);

private:
    qreal _margin;
};

#endif // QPTIGHTSCALEENGINE_H
