#include "classes.h"

// Some OpenGL knowledge may be required to understand this example.

#include <QOpenGLFunctions>

DynamicTexture::DynamicTexture()
    : m_texture(new QOpenGLTexture(QOpenGLTexture::Target2D)), m_url("") { }

DynamicTexture::~DynamicTexture() {
    delete m_texture;
}

// Set some basic information about the texture.
void DynamicTexture::setTextureInfo(QOpenGLTexture::TextureFormat format, QString url)
{
    // Load image file
    QImage image(url);
    if (image.isNull()) {
        qDebug() << url << "Unable to load image";
    }

    m_image = image;
    m_format = format;
    m_size = QSize(image.size());
    m_oldUrl = m_url;
    m_url = url;
    updateTexture();
}

bool DynamicTexture::updateTexture()
{
    // If format is different or texture not created, create it here.
    if (!m_texture->isCreated() ||
            m_texture->width() != m_image.width() || m_texture->height() != m_image.height() ||
            m_texture->format() != m_format ||
            m_oldUrl != m_url)
    {
        if (m_texture->isCreated()) {
            m_texture->destroy();
        }

        // Configure texture object.
        m_texture->create();
        m_texture->setFormat(m_format);
        m_texture->setMinMagFilters(QOpenGLTexture::Linear, QOpenGLTexture::Linear);
        m_texture->setWrapMode(QOpenGLTexture::ClampToEdge);
        //m_texture->setSize(m_image.width(), m_image.height());
        m_texture->setMipLevels(0);
        //m_texture->allocateStorage();
    }

    // setData actually allocates the memory for the image and fills the texture with the image data.
    // There are also ways to pass raw data (or YUV and many other formats which can be useful).
    if (m_texture->isCreated()) {
        m_texture->setData(m_image);
    }
    else {
        qDebug() << "Texture not created, cannot set data to it.";
        return false;
    }

    return true;
}

YUVMaterial::YUVMaterial()
{
    _color = QColor(Qt::red);
}

YUVMaterial::~YUVMaterial()
{

}

void YUVMaterial::updateState(const Textures *state, const Textures *)
{
    if (!program()->bind())
        qDebug() << "Error binding shader program in" << Q_FUNC_INFO;

    QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();

    f->glActiveTexture(GL_TEXTURE0);
    state->myTexture->bind();
}

void YUVMaterial::resolveUniforms()
{
    if (!program()->bind())
        qDebug() << "Error binding shader program in" << Q_FUNC_INFO;

    program()->setUniformValue("myTexture", 0); // GL_TEXTURE0
    program()->setUniformValue("color", _color);
}
