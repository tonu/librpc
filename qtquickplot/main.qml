import QtQuick 2.1
import QuickPlot 1.0

Rectangle {
    visible: true
    width: 640
    height: 480

    MouseArea {
        anchors.fill: parent
        onClicked: {
            Qt.quit();
        }
    }

    PlotArea {
        id: plotArea
        anchors.fill: parent

        xScaleEngine: TightScaleEngine {
            margin: 0;
        }
        yScaleEngine: TightScaleEngine {
            margin: 0.2;
        }

        items: [
            ScrollingScatter {
                id: meter;
                numPoints: 300
            }
        ]
    }

    Timer {
        id: timer;
        interval: 20;
        repeat: true;
        running: true;

        property real pos: 0

        onTriggered: {
            meter.appendDataPoint(Math.sin(pos));
            pos += 0.05;
        }
    }

    // workaround for bug - https://gitlab.com/qtquickplot/qtquickplot/issues/4
    Timer {
        id: timer2
        interval: 500;
        onTriggered: parent.width = parent.width + 1
        repeat: false
        running: true
    }
}
