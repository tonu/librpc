#ifndef QPSCROLLINGSCATTER_H
#define QPSCROLLINGSCATTER_H

#include <QQuickItem>
#include <boost/circular_buffer.hpp>
#include <set>

#include "qpcurve.h"

class QPScrollingScatter : public QPCurve
{
    Q_OBJECT

public:
    QPScrollingScatter(QQuickItem *parent = 0);


protected:
    QSGNode *updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *);

};

#endif // QPSCROLLINGSCATTER_H
