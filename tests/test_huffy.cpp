//
// Created by Tonu Jaansoo on 14/08/16.
//
#include "../webglstreampp/Huffy.hpp"

#include <iostream>
#include <cassert>

std::vector<char> vec = {0x0, 0x0, 0x0, 0x0, 0x03, 0x07, 0x0f, 0x00, 0x03, 0x07, 0x0f, 0x00, 0x03, 0x07, 0x0f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

void test1()
{
    std::shared_ptr<std::vector<char>> ptr(new std::vector<char>(vec));
    Huffy hf;

    uint32_t bits;
    hf.encode(ptr, bits);

    std::cerr << "old size:" << vec.size() << " new size:" << (bits / 8) << "\n";

    hf.decode(ptr, bits);

    assert(vec.size() == ptr->size());

    for (int i=0; i< vec.size(); i++)
    {
        std::cerr << int(vec.at(i)) << "==" << int(ptr->at(i)) << "\n";
        assert(vec.at(i) == ptr->at(i));
    }

    std::cerr << "seems all ok!\n";
}


int main()
{
    test1();

    return 0;
}
