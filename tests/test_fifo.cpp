//
// Created by Tonu Jaansoo on 14/08/16.
//
#include "../webglstreampp/fifo.hpp"
#include "../webglstreampp/arg.hpp"
#include <iostream>

uint8_t buf[] = {0x0, 0x1, 0x2, 0x3};

void test1()
{
    FIFO f;

    std::cerr << "TEST1--------- \n";

    //writing
    f << arg<uint8_t>(7);
    f << arg<uint16_t>(15);

    //pushing buf to queue
    f.pushBuf(false);

    //pop frame
    auto frame = f.popFrame();

    if (!frame)
    {
        throw "error";
    }

    for (uint8_t n : *frame)
        printf(" %02x", n);
    printf("\n");

    for (int i = 0; i < frame->size(); i++)
        assert(frame->operator[](i) == buf[i]);
}

void test2()
{
    FIFO f;

    std::cerr << "\nTEST2--------- \n";

    uint8_t *ptr = buf;

    f.writeFrames(ptr, 3);
    ptr += 3;
    f.writeFrames(ptr, 3);
    ptr += 3;
    f.writeFrames(ptr, 3);
    ptr += 3;
    f.writeFrames(ptr, 3);
    ptr += 3;

    for (int i = 0; i < 3; i++)
    {
        auto frame = f.popFrame();

        if (!frame)
        {
            throw "error";
        }

        for (uint8_t n : *frame)
            printf(" %02x", n);
        printf("\n");

        for (int i = 0; i < frame->size(); i++)
            assert(frame->operator[](i) == buf[i]);
    }
}

void test3()
{
    FIFO f;
    uint8_t *ptr = buf;

    std::cerr << "\nTEST3--------- \n";


    f.writeFrames(ptr, 3);
    ptr += 3; //3
    f.writeFrames(ptr, 1);
    ptr += 1; //4
    f.writeFrames(ptr, 2);
    ptr += 2; //6
    f.writeFrames(ptr, 4);
    ptr += 4; //10
    f.writeFrames(ptr, 1);
    ptr += 1; //11
    f.writeFrames(ptr, 1);
    ptr += 1; //12

    uint8_t buf[100];
    int  n;

    assert(f.popBuf(true));
    n = f.read(buf, 2);
    assert(n == 2);
    assert(buf[0] == 0x07);
    assert(buf[1] == 0x0f);
    n = f.read(buf, 2);
    assert(n == 1);
    assert(buf[0] == 0x00);
    n = f.read(buf, 2);
    assert(n == 0);


    assert(f.popBuf(true));
    n = f.read(buf, 4);
    assert(n == 3);
    assert(buf[0] == 0x07);
    assert(buf[1] == 0x0f);
    assert(buf[2] == 0x00);
    n = f.read(buf, 2);
    assert(n == 0);

    assert(f.popBuf(true));
    n = f.read(buf, 2);
    assert(n == 2);
    assert(buf[0] == 0x07);
    assert(buf[1] == 0x0f);
    n = f.read(buf, 2);
    assert(n == 1);
    assert(buf[0] == 0x00);
    n = f.read(buf, 2);
    assert(n == 0);

    assert(f.popBuf(true) == false);
}


void test4()
{
    FIFO f;

    uint8_t tmp[sizeof(buf)];

    f.write(buf, sizeof(buf));
    f.pushBuf(false);
    f.popBuf(true);
    int n = f.read(tmp, sizeof(tmp));

    std::cerr << "----\n";
    for (int i=0;i<n; i++)
    {
        assert(buf[i] == tmp[i]);
        std::cerr << i << ":" << int(tmp[i]) << "\n";
    }
}

void test5()
{
    uint8_t b[]  = { 0x36, 0x5f, 0xa6, 0xff, 0x01 };

    uint8_t tmp2[1000];
    FIFO f1;
    FIFO f2;

//    f1.write(b, sizeof(b));
//    f1.pushBuf(true);
//    auto fr = f1.popFrame();

    f2.writeFrames(b, sizeof(b));
    f2.popBuf(true);

    int n = f2.read(tmp2, 1000);
    std::cerr << "read back:" << n << "\n";
    for (int i=0;i<n; i++)
    {
        std::cerr << i << ": " <<  int(b[i]) << "==" << int(tmp2[i]) << "\n";
       // assert(tmp2[i] == b[i]);
    }
}

int main(int argc, char *argv[])
{
//    test1();
//    test2();
//    test3();
//    test4();
    test5();

    std::cerr << "\n-------\nAll tests passed\n";

    return 0;
}

