#include "tests.h"

#include <webglstreampp/fifo.hpp>
#include <webglstreampp/buffer.hpp>
#include <webglstreampp/arg.hpp>
#include <webglstreampp/gles_includes.hpp>
#include <webglstreampp/client.hpp>
#include <webglstreampp/server.hpp>
#include <webglstreampp/wsserver.hpp>

#include <thread>

bool test1()
{
    float buf [] = {1.1,2.2,3.3};

    FIFO f;
    buffer<float> b = buffer<float>(buf, 3);

    arg<uint16_t>      ar11(55);
    arg<buffer<float>> ar12(b);
    arg<uint16_t>      ar21;
    arg<buffer<float>> ar22;

    f << ar11;
    f << ar12;

    f >> ar21;
    f >> ar22;

    if(ar21.val != 55)
        return false;

    for (int i=0;i<3;i++)
        if (buf[0] != ar22.val._ptr.get()[0])
            return false;

    std::cerr << "test1 OK\n\n";
    return true;
}


bool test2()
{
    std::shared_ptr<FIFO> fifo_cli_to_srv(new FIFO);
    std::shared_ptr<FIFO> fifo_srv_to_cli(new FIFO);

    Client client(fifo_cli_to_srv, fifo_srv_to_cli);
    Server server(fifo_cli_to_srv, fifo_srv_to_cli);

    std::thread cthr([&]
    {
        glAttachShader_ptr proc = (glAttachShader_ptr)client.getProcAddress("glAttachShader");
        proc(444,555);
    });

    std::thread sthr([&]
    {
        server.run();
    });

    sthr.join();
    cthr.join();

    std::cerr << "test2 OK\n\n";
    return true;
}


bool test3()
{
    std::shared_ptr<FIFO> fifo_cli_to_srv(new FIFO);
    std::shared_ptr<FIFO> fifo_srv_to_cli(new FIFO);

    Client client(fifo_cli_to_srv, fifo_srv_to_cli);
    Server server(fifo_cli_to_srv, fifo_srv_to_cli);

    std::thread server_thr([&]
    {
        server.run();
    });

    std::thread client_thr([&]
    {
        GLuint program = 555;
        GLsizei bufsize = 666;
        GLsizei length[1]; *length = 777;// get actual size
        GLchar infolog[] = {"tere"};

        glGetProgramInfoLog_ptr proc = (glGetProgramInfoLog_ptr)client.getProcAddress("glGetProgramInfoLog");
        proc(program,bufsize,length,infolog);


        std::cerr << " return vals:\n";
        std::cerr << length[0] << "\n";
        std::cerr << infolog << "\n";

    });

    server_thr.join();
    client_thr.join();

    std::cerr << "test3 OK\n\n";
    return true;
}



bool test4()
{
    std::shared_ptr<FIFO> fifo_cli_to_srv(new FIFO);
    std::shared_ptr<FIFO> fifo_srv_to_cli(new FIFO);

    Client client(fifo_cli_to_srv, fifo_srv_to_cli);
    Server server(fifo_cli_to_srv, fifo_srv_to_cli);

    std::thread server_thr([&]
    {
        server.run();
    });

    std::thread client_thr([&]
    {
        glViewport_ptr pViewport= (glViewport_ptr)client.getProcAddress("glViewport");
        glClearColor_ptr pClearColor= (glClearColor_ptr)client.getProcAddress("glClearColor");
        glClear_ptr pClear= (glClear_ptr)client.getProcAddress("glClear");
        glGetString_ptr pGetString = (glGetString_ptr)client.getProcAddress("glGetString");

        std::cerr << "GL_VENDOR: " << pGetString(GL_VENDOR) << "\n";
        std::cerr << "GL_RENDERER: " << pGetString(GL_RENDERER) << "\n";
        std::cerr << "GL_VERSION: " << pGetString(GL_VERSION) << "\n";
        std::cerr << "GL_EXTENSIONS: " << pGetString(GL_EXTENSIONS) << "\n";

        pViewport(0,0, 800, 600);

        for (int i=0;i<2;i++)
        {
            pClearColor(1,0,0,0);
            pClear(GL_COLOR_BUFFER_BIT);
            std::this_thread::sleep_for(std::chrono::milliseconds(500));

            pClearColor(0,1,0,0);
            pClear(GL_COLOR_BUFFER_BIT);
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
        }

        server.stop();
    });

    server_thr.join();
    client_thr.join();

    std::cerr << "test4 OK\n\n";
    return true;
}

void run_gltest(Client *client)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(10));

    // catch connection closed
    try {

        glViewport_ptr pViewport= (glViewport_ptr)client->getProcAddress("glViewport");
        glClearColor_ptr pClearColor= (glClearColor_ptr)client->getProcAddress("glClearColor");
        glClear_ptr pClear= (glClear_ptr)client->getProcAddress("glClear");
        glGetString_ptr pGetString = (glGetString_ptr)client->getProcAddress("glGetString");

        std::cerr << "GL_VENDOR: " << pGetString(GL_VENDOR) << "\n";
        std::cerr << "GL_RENDERER: " << pGetString(GL_RENDERER) << "\n";
        std::cerr << "GL_VERSION: " << pGetString(GL_VERSION) << "\n";
        std::cerr << "GL_EXTENSIONS: " << pGetString(GL_EXTENSIONS) << "\n";

        pViewport(0,0, 800, 600);

        for (int i=0;i<2;i++)
        {
            pClearColor(1,0,0,0);
            pClear(GL_COLOR_BUFFER_BIT);
            std::this_thread::sleep_for(std::chrono::milliseconds(500));

            pClearColor(0,1,0,0);
            pClear(GL_COLOR_BUFFER_BIT);
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
        }
    }
    catch (const char * err)
    {
        std::cerr << "GL test terminated\n";
    }
};


bool test5()
{
    std::shared_ptr<FIFO> fifo_cli_to_srv(new FIFO);
    std::shared_ptr<FIFO> fifo_srv_to_cli(new FIFO);

    Client client(fifo_cli_to_srv, fifo_srv_to_cli);

    WSServer ws(12345, fifo_cli_to_srv, fifo_srv_to_cli);

    std::shared_ptr<std::thread> thread;
    ws.onConnect = [&] ()
    {
        if (thread)
        {
            fifo_cli_to_srv->cancelWrite();
            fifo_srv_to_cli->cancelRead();

            thread->join();
        }

        thread = std::make_shared<std::thread>(run_gltest, &client);
    };

    ws.onClose = [&] ()
    {
        std::cerr << "close\n";
    };

    ws.start();

    std::cerr << "test5 OK\n\n";
    return true;
}

bool test6()
{
    std::cerr << "asd\n";

    FIFO fifo1;


    return true;
}